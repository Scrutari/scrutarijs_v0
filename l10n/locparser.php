<?php

function parseLocArray($lines) {
    $locArray = array();
    $count = count($lines);
    for($i = 0; $i < $count; $i++) {
        $line = trim($lines[$i]);
        if (strlen($line) == 0) {
            continue;
        }
        $firstChar = substr($line, 0, 1);
        switch($firstChar) {
            case ';':
            case '#':
            case '!';
            case '[';
                continue;
        }
        $idx = strpos($line, '=');
        if (!$idx) {
            continue;
        }
        $key = trim(substr($line, 0, $idx));
        $value = trim(substr($line, $idx + 1));
        $locArray[$key] = $value;
    }
    return $locArray;
}


?>