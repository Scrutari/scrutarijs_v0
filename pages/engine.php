<?php
/***************************************************************
 * ScutariJs
 * Copyright (c) 2014-2017 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */

function getPermalinkPattern() {
    // Get HTTP/HTTPS (the possible values for this vary from server to server)
    $permalink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] && !in_array(strtolower($_SERVER['HTTPS']),array('off','no'))) ? 'https' : 'http';
    // Get domain portion
    $permalink .= '://'.$_SERVER['HTTP_HOST'];
    // Get path to script
    $permalink .= strtok($_SERVER['REQUEST_URI'],'?');
    // Add path info, if any
    if (!empty($_SERVER['PATH_INFO'])) $permalink .= $_SERVER['PATH_INFO'];

    $get = $_GET; // Create a copy of $_GET
    unset($get['q']);
    unset($get['qid']);
    unset($get['langui']);
    unset($get['lang']);
    unset($get['target']);
    $get['engine'] = $GLOBALS['scrutari']['engine']['name'];
    if (isset($get['mode'])) {
        $mode = $get['mode'];
        if ($mode == 'frame') {
            $get['page'] = 'frame';
        }
        unset($get['mode']);
    }
    $permalink .= '?'.http_build_query($get);
    $permalink .= '&qid=$QID&lang=$LANG';
    return $permalink;
}
 
 function getEngineHtml($dirPath) {
    $list = file($dirPath."list.txt");
    $count = count($list);
    $jsString = "Engine.html = {\nlist:[";
    $next = false;
    for($i = 0; $i < $count; $i++) {
        $key = trim($list[$i]);
        if (strlen($key) > 0) {
            if ($next) {
                $jsString .= ',';
            } else {
                $next = true;
            }
            $jsString .= '"'.$key.'"';
        }
    }
    $jsString .= "]";
    for($i = 0; $i < $count; $i++) {
        $key = trim($list[$i]);
        if (strlen($key) > 0) {
            $html = file_get_contents($dirPath.$key.".html");
            $html = preg_replace("/\s+/", " ", $html);
            $jsString .=  ",\n".$key.":\"";
            $jsString .= addslashes($html);
            $jsString .= "\"";
        }
    }
    $jsString .= "\n};\n";
    return $jsString;
 } 
?>
<!DOCTYPE html>
<html lang="<?php echo $GLOBALS['scrutari']['lang'];?>">
<head>
<title>ScrutariJs</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="static/icon.png" type="image/png" rel="icon">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="scrutarijs/jquery/1.11.2/jquery.min.js"></script>
<script src="scrutarijs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="scrutarijs/bootstrap/3.3.7/css/bootstrap.min.css">
<?php
if ($GLOBALS['scrutari']['dev']) {
    $list = file("scrutarijs/js/lib/scrutari/list.txt");
    $count = count($list);
    for($i = 0; $i < $count; $i++) {
        $jsfile = trim($list[$i]);
        if ((strlen($jsfile) > 0) && (strpos($jsfile, '#') !== 0)) {
            echo '<script src="scrutarijs/js/lib/scrutari/'.$jsfile.'"></script>'."\n";
        }
    }
    $list = file("scrutarijs/js/lib/engine/list.txt");
    $count = count($list);
    for($i = 0; $i < $count; $i++) {
        $jsfile = trim($list[$i]);
        if ((strlen($jsfile) > 0) && (strpos($jsfile, '#') !== 0)) {
            echo '<script src="scrutarijs/js/lib/engine/'.$jsfile.'"></script>'."\n";
        }
    }
} else {
    echo '<script src="scrutarijs/js/scrutarijs.js"></script>'."\n";
}
?>
<script src="scrutarijs/js/l10n/<?php echo $GLOBALS['scrutari']['l10n']; ?>.js"></script>
<script>
    <?php if ($GLOBALS['scrutari']['dev']) {
        echo getEngineHtml("scrutarijs/js/lib/engine/html/bootstrap3/");
    }?>
    $(function () {
        var scrutariConfig = new Scrutari.Config("<?php echo $GLOBALS['scrutari']['engine']['name'];?>","<?php echo $GLOBALS['scrutari']['engine']['url'];?>", "<?php echo $GLOBALS['scrutari']['lang'];?>", "<?php echo $GLOBALS['scrutari']['origin'];?>");
        var engine = new Engine(scrutariConfig, Engine.l10n);
        engine.setWithCorpus(<?php booleanToString($GLOBALS['scrutari']['engine']['with-corpus']); ?>);
        engine.setBaseSort("<?php echo addslashes($GLOBALS['scrutari']['engine']['base-sort']);?>");
        engine.setCorpusSort("<?php echo addslashes($GLOBALS['scrutari']['engine']['corpus-sort']);?>");
        engine.setMode("<?php echo addslashes($GLOBALS['scrutari']['mode']);?>");
        engine.setTarget("<?php echo addslashes($GLOBALS['scrutari']['target']);?>");
        engine.setInitialQuery("<?php echo addslashes($GLOBALS['scrutari']['query']);?>");
        if (engine.setInitialQId) {
            engine.setInitialQId("<?php echo addslashes($GLOBALS['scrutari']['qid']);?>");
        }
        if (engine.setHiddenList) {
            engine.setHiddenList("<?php echo addslashes($GLOBALS['scrutari']['hide']);?>");
        }
        if (engine.setPermalinkPattern) {
            engine.setPermalinkPattern("<?php echo addslashes(getPermalinkPattern());?>");
        }
        Engine.Init.html(engine, "#mainContainer");
        Engine.Init.mainTitle(engine);
        Engine.Init.custom(engine);
        var _scrutariMetaCallback = function (scrutariMeta) {
            Engine.Init.scrutariMeta(engine, scrutariMeta);
        };
        if (Engine.FrameMode.is(engine)) {
            engine.setChangePlageCallbackFunction(Engine.FrameMode.changePlageCallback);
        }
        Scrutari.Meta.load(_scrutariMetaCallback, scrutariConfig);
    });
</script>
<?php
$count = count($GLOBALS['scrutari']['engine']['js-links']);
for($i = 0; $i < $count; $i++) {
    echo '<script src="'.$GLOBALS['scrutari']['engine']['js-links'][$i].'"></script>'."\n";
}
$count = count($GLOBALS['scrutari']['engine']['css-links']);
for($i = 0; $i < $count; $i++) {
    echo '<link rel="stylesheet" href="'.$GLOBALS['scrutari']['engine']['css-links'][$i].'">'."\n";
}
?>
</head>
<body>
<div class="container" id="mainContainer">
</div>
</body>
</html>