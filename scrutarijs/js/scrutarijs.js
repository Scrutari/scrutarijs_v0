/*** r1612 ***/
var Scrutari = {};
Scrutari.log = function (msg) {
    if ((console) && (console.log)) {
        console.log(msg);
    }
};
Scrutari.logError = function (error) {
    var errorMessage = "Scrutari Request Error [key = " + error.key + " | parameter = " + error.parameter;
    if (error.hasOwnProperty("value")) {
        errorMessage += " | value = " + error.value;
    }
    if (error.hasOwnProperty("array")) {
        errorMessage += " | array = (";
        for(var i = 0; i < error.array.length; i++) {
            if (i > 0) {
                errorMessage += ";";
            }
            var obj = error.array[i];
            errorMessage += obj.key;
            if (obj.hasOwnProperty("value")) {
                errorMessage += "=" + obj.value;
            }
        }
        errorMessage += ")";
    }
    errorMessage += "}";
    Scrutari.log(errorMessage);
};
Scrutari.convert = function (jqArgument) {
    if (jqArgument.jquery) {
        return jqArgument;
    } else {
        return $(jqArgument);
    }
};
Scrutari.exists = function (jqArgument) {
    return Scrutari.convert(jqArgument).length > 0;
};
 Scrutari.escape = function (text) {
     var result = "";
    for(var i = 0; i < text.length; i++) {
        carac = text.charAt(i);
        switch (carac) {
            case '&':
                result += "&amp;";
                break;
            case '"':
                result += "&quot;";
                break;
            case '<':
                result += "&lt;";
                break;
            case '>':
                result += "&gt;";
                break;
            case '\'':
                result += "&#x27;";
                break;
            default:
                result += carac;
        }
    }
    return result;
 };
Scrutari.Loc = function (map) {
    if (map) {
        this.map = map;
    } else {
        this.map = new Object();
    }
};
Scrutari.Loc.prototype.putAll = function (map) {
    for(var key in map) {
        this.map[key] = map[key];
    }
};
Scrutari.Loc.prototype.putLoc = function (locKey, locText) {
    this.map[locKey] = locText;
};
Scrutari.Loc.prototype.loc = function (locKey) {
    if (!this.map.hasOwnProperty(locKey)) {
        return locKey;
    }
    var text = this.map[locKey];
    var argLength = arguments.length;
    if (argLength > 1) {
        for(var i = 0; i < argLength; i++) {
            var p = i -1;
            var mark = "{" + p + "}";
            text = text.replace(mark, arguments[i]);
        }
    }
    return text;
};
Scrutari.Loc.prototype.escape = function (locKey) {
    return Scrutari.escape(this.loc.apply(this, arguments));
};
Scrutari.Config = function (name, engineUrl, langUi, origin) {
    this.name = name;
    this.engineUrl = engineUrl;
    this.langUi = langUi;
    this.origin = origin;
    this.ficheFields = "";
    this.plageLength = 50;
    this.groupSortFunction = Scrutari.Utils.sortGroupByFicheCount;
};
Scrutari.Config.prototype.getName = function () {
    return this.name;
};
Scrutari.Config.prototype.getJsonUrl = function () {
    return this.engineUrl + "json";
};
Scrutari.Config.prototype.getLangUi = function () {
    return this.langUi;
};
Scrutari.Config.prototype.getOrigin = function () {
    return this.origin;
};
Scrutari.Config.prototype.getPlageLength = function () {
    return this.plageLength;
};
Scrutari.Config.prototype.setPlageLength = function (plageLength) {
    this.plageLength = plageLength;
};
Scrutari.Config.prototype.getOdsUrl = function (qId) {
    return this.engineUrl + "export/" +  "result_" + qId + "_" + this.langUi + ".ods";
};
Scrutari.Config.prototype.getAtomUrl = function (qId) {
    var date = new Date();
    var dateString = date.getFullYear() + "-";
    var mois = date.getMonth() + 1;
    if (mois < 10) {
        dateString += "0";
    }
    dateString += mois;
    dateString += "-";
    var jour = date.getDate();
    if (jour < 10) {
        dateString += "0";
    }
    dateString += jour;
    return this.engineUrl + "feed/" + "fiches_" + this.langUi + ".atom?qid=" + qId + "&all=" + dateString;
};
Scrutari.Config.prototype.getPermalinkUrl = function (qId, permalinkPattern) {
    var permalink = permalinkPattern.replace("$LANG", this.langUi);
    permalink = permalink.replace("$QID", qId);
    return permalink;
};
Scrutari.Config.prototype.getFicheFields = function () {
    return this.ficheFields;
};
Scrutari.Config.prototype.setFicheFields = function (ficheFields) {
    this.ficheFields = ficheFields;
};
Scrutari.Config.prototype.setGroupSortFunction = function (groupSortFunction) {
    this.groupSortFunction = groupSortFunction;
};
Scrutari.Config.prototype.getGroupSortFunction = function () {
    return this.groupSortFunction;
};
Scrutari.Ajax = {};
Scrutari.Ajax.loadBaseArray = function (baseArrayCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "base";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "baseArray", baseArrayCallback);
        }
    });
};
Scrutari.Ajax.loadCorpusArray = function (corpusArrayCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "corpus";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "corpusArray", corpusArrayCallback);
        }
    });
};
Scrutari.Ajax.loadThesaurusArray = function (thesaurusArrayCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "thesaurus";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "thesaurusArray", thesaurusArrayCallback);
        }
    });
};
Scrutari.Ajax.loadMotcleArray = function (motcleArrayCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "motcle";
    Scrutari.Ajax.check(requestParameters, "motclefields", "motcleid,libelles");
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "motcleArray", motcleArrayCallback);
        }
    });
};
Scrutari.Ajax.loadFicheArray = function (ficheArrayCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "fiche";
    Scrutari.Ajax.check(requestParameters, "fichefields", "titre");
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "ficheArray", ficheArrayCallback);
        }
    });
};
Scrutari.Ajax.loadCategoryArray = function (categoryArrayCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "category";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "categoryArray", categoryArrayCallback);
        }
    });
};
Scrutari.Ajax.loadEngineInfo = function (engineInfoCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "engine";
    Scrutari.Ajax.check(requestParameters, "data", "all");
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 1);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "engineInfo", engineInfoCallback);
        }
    });
};
Scrutari.Ajax.loadFicheSearchResult = function (ficheSearchResultCallback, scrutariConfig, requestParameters, scrutariErrorCallback) {
    var defaultFicheFields = scrutariConfig.getFicheFields();
    if (!defaultFicheFields) {
        defaultFicheFields = "codecorpus,mtitre,msoustitre,mattrs_all,mcomplements,annee,href,icon";
    }
    requestParameters.type = "q-fiche";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "fichefields", defaultFicheFields);
    Scrutari.Ajax.check(requestParameters, "motclefields", "mlibelles");
    Scrutari.Ajax.check(requestParameters, "q-mode", "intersection");
    Scrutari.Ajax.check(requestParameters, "origin", scrutariConfig.getOrigin());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    Scrutari.Ajax.check(requestParameters, "start", 1);
    Scrutari.Ajax.check(requestParameters, "limit", scrutariConfig.getPlageLength() * 2);
    Scrutari.Ajax.check(requestParameters, "starttype", "in_all");
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "ficheSearchResult", ficheSearchResultCallback, scrutariErrorCallback);
        }
    });
};
Scrutari.Ajax.loadExistingFicheSearchResult = function (existingFicheSearchResultCallback, scrutariConfig, requestParameters) {
    var defaultFicheFields = scrutariConfig.getFicheFields();
    if (!defaultFicheFields) {
        defaultFicheFields = "codecorpus,mtitre,msoustitre,mattrs_all,mcomplements,annee,href,icon";
    }
    requestParameters.type = "q-fiche";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "fichefields", defaultFicheFields);
    Scrutari.Ajax.check(requestParameters, "motclefields", "");
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    Scrutari.Ajax.check(requestParameters, "start", 1);
    Scrutari.Ajax.check(requestParameters, "limit", scrutariConfig.getPlageLength() * 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "ficheSearchResult", existingFicheSearchResultCallback);
        }
    });
};
Scrutari.Ajax.loadGeoJson = function (geojsonCallback, scrutariConfig, requestParameters, scrutariErrorCallback) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "geojson";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "fichefields", "titre");
    Scrutari.Ajax.check(requestParameters, "motclefields", "");
    Scrutari.Ajax.check(requestParameters, "origin", scrutariConfig.getOrigin());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 1);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            if (data.hasOwnProperty("error")) {
                if (scrutariErrorCallback) {
                    scrutariErrorCallback(data.error);
                } else {
                    Scrutari.logError(data.error);
                }
            } else {
                Scrutari.Ajax.logWarnings(data);
                geojsonCallback(data);
            }
        }
    });
};
Scrutari.Ajax.success = function(ajaxResult, objectName, objectCallback, scrutariErrorCallback) {
    if (ajaxResult.hasOwnProperty("error")) {
        if (scrutariErrorCallback) {
            scrutariErrorCallback(ajaxResult.error);
        } else {
            Scrutari.logError(ajaxResult.error);
        }
    } else {
        Scrutari.Ajax.logWarnings(ajaxResult);
        if (!ajaxResult.hasOwnProperty(objectName)) {
            $.error(objectName + " object is missing in json response");
        } else {
            objectCallback(ajaxResult[objectName]);
        }
    }
};
Scrutari.Ajax.logWarnings = function (ajaxResult) {
    if (ajaxResult.hasOwnProperty("warnings")) {
        warningsMessage = "Scrutari Request Warnings [";
        for(var i = 0; i < ajaxResult.warnings.length; i++) {
            if (i > 0) {
                warningsMessage += ";";
            }
            var warning = ajaxResult.warnings[i];
            warningsMessage += "key = ";
            warningsMessage += warning.key;
            warningsMessage += " | parameter = ";
            warningsMessage += warning.parameter;
            if (warning.hasOwnProperty("value")) {
                warningsMessage += " | value = ";
                warningsMessage += warning.value;
            }
        }
        warningsMessage += "]";
        Scrutari.log(warningsMessage);
    }
};
Scrutari.Ajax.check = function (obj, name, defaultValue) {
    if (!obj.hasOwnProperty(name)) {
        obj[name] = defaultValue;
    }
};
Scrutari.Meta = function (engineInfo) {
    this.engineInfo = engineInfo;
};
Scrutari.Meta.load = function(callback, scrutariConfig) {
    var buffer = new Object();
    var _ajaxEnd = function() {
        var scrutariMeta = new Scrutari.Meta(buffer.engineInfo);
        callback(scrutariMeta);
    };
    var _engineInfoCallback = function (engineInfo) {
        buffer.engineInfo = engineInfo;
        _ajaxEnd();
    };
    Scrutari.Ajax.loadEngineInfo(_engineInfoCallback, scrutariConfig);
};
Scrutari.Meta.prototype.getEngineInfo = function () {
    return this.engineInfo;
};
Scrutari.Meta.prototype.getGlobalFicheCount = function () {
    return this.engineInfo.stats.fiche;
};
Scrutari.Meta.prototype.getGlobalLangFicheCount = function (langArray) {
    var ficheCount = 0;
    var length = this.engineInfo.stats.langArray.length;
    for(var i = 0; i < length; i++) {
        var langObj = this.engineInfo.stats.langArray[i];
        if ($.inArray(langObj.lang, langArray) !== -1) {
            ficheCount += langObj.fiche;
        }
    }
    return ficheCount;
};
Scrutari.Meta.prototype.getCorpusFicheCount = function (code) {
    var corpus = this.getCorpus(code);
    if (!corpus) {
        return 0;
    }
    return corpus.stats.fiche;
};
Scrutari.Meta.prototype.getCorpusLangFicheCount = function (code, langArray) {
    var corpus = this.getCorpus(code);
    if (!corpus) {
        return 0;
    }
    var ficheCount = 0;
    var length = corpus.stats.langArray.length;
    for(var i = 0; i < length; i++) {
        var langObj = corpus.stats.langArray[i];
        if ($.inArray(langObj.lang, langArray) !== -1) {
            ficheCount += langObj.fiche;
        }
    }
    return ficheCount;
};
Scrutari.Meta.prototype.getBaseArray = function (sortFunction) {
    var array = new Array();
    var baseMap = this.engineInfo.baseMap;
    var p=0;
    for(var prop in baseMap) {
        array[p] = baseMap[prop];
        p++;
    }
    if (sortFunction) {
        array = array.sort(sortFunction);
    }
    return array;
};
Scrutari.Meta.prototype.getLangArray = function (sortFunction) {
    var array = new Array();
    var length = this.engineInfo.stats.langArray.length;
    for(var i = 0; i < length; i++) {
        array[i] = this.engineInfo.stats.langArray[i];
    }
    if (sortFunction) {
        array = array.sort(sortFunction);
    }
    return array;
};
Scrutari.Meta.prototype.getCategoryArray = function (sortFunction) {
    var array = new Array();
    if (!this.engineInfo.hasOwnProperty("categoryMap")) {
        return array;
    }
    var categoryMap = this.engineInfo.categoryMap;
    var p=0;
    for(var prop in categoryMap) {
        array[p] = categoryMap[prop];
        p++;
    }
    if (sortFunction) {
        array = array.sort(sortFunction);
    }
    return array;
};
Scrutari.Meta.prototype.getCorpusArray = function (sortFunction) {
    var array = new Array();
    var corpusMap = this.engineInfo.corpusMap;
    var p=0;
    for(var prop in corpusMap) {
        array[p] = corpusMap[prop];
        p++;
    }
    if (sortFunction) {
        array = array.sort(sortFunction);
    }
    return array;
};
Scrutari.Meta.prototype.getAttributeArray = function (type) {
    if (!this.engineInfo.hasOwnProperty("attributes")) {
        return new Array();
    }
    if (!this.engineInfo.attributes.hasOwnProperty(type)) {
        return new Array();
    }
    return this.engineInfo.attributes[type];
};
Scrutari.Meta.prototype.getBase = function (code) {
    var key = "code_" + code;
    if (this.engineInfo.baseMap.hasOwnProperty(key)) {
        return this.engineInfo.baseMap[key];
    } else {
        return null;
    }
};
Scrutari.Meta.prototype.getCorpus = function (code) {
    var key = "code_" + code;
    if (this.engineInfo.corpusMap.hasOwnProperty(key)) {
        return this.engineInfo.corpusMap[key];
    } else {
        return null;
    }
};
Scrutari.Meta.prototype.getThesaurus = function (code) {
    var key = "code_" + code;
    if (this.engineInfo.thesaurusMap.hasOwnProperty(key)) {
        return this.engineInfo.thesaurusMap[key];
    } else {
        return null;
    }
};
Scrutari.Meta.prototype.getLangLabel = function (iso) {
    if (this.engineInfo.langMap.hasOwnProperty(iso)) {
        return this.engineInfo.langMap[iso];
    } else {
        return iso;
    }
};
Scrutari.Meta.prototype.withCategory = function () {
    return this.engineInfo.hasOwnProperty("categoryMap");
};
Scrutari.Meta.prototype.getCorpusArrayForCategories = function (categoryArray) {
    var result = new Array();
    if (!this.engineInfo.hasOwnProperty("categoryMap")) {
        return result;
    }
    for(var i = 0; i < categoryArray.length; i++) {
        var categoryName = categoryArray[i];
        if (this.engineInfo.categoryMap.hasOwnProperty(categoryName)) {
            result = result.concat(this.engineInfo.categoryMap[categoryName].codecorpusArray);
        }
    }
    return result;
};
Scrutari.Meta.prototype.getCorpusArrayForBases = function (baseArray) {
    var result = new Array();
    for(var i = 0; i < baseArray.length; i++) {
        var key = "code_" + baseArray[i];
        if (this.engineInfo.baseMap.hasOwnProperty(key)) {
            result = result.concat(this.engineInfo.baseMap[key].codecorpusArray);
        }
    }
    return result;
};
Scrutari.Meta.prototype.getComplementIntitule = function(code, complementNumber) {
    var corpus = this.getCorpus(code);
    if (!corpus) {
        return "";
    }
    var key = "complement_" + complementNumber;
    if (corpus.intitules.hasOwnProperty(key)) {
        return corpus.intitules[key];
    } else {
        return key;
    }
};
Scrutari.Utils = {};
Scrutari.Utils.divideIntoColumns = function (objectArray, jqArgument, objectTemplate) {
    var objectCount = objectArray.length;
    if (objectCount === 0) {
        return;
    }
    var $elements = Scrutari.convert(jqArgument);
    var elementCount = $elements.length;
    if (elementCount === 0) {
        Scrutari.log("HtmlElement selection with jqArgument is empty ");
        return;
    }
    var objectCount = objectArray.length;
    if (objectCount <= elementCount) {
        for(var i = 0; i < objectCount; i++) {
            $($elements[i]).append(objectTemplate(objectArray[i]));
        }
        return;
    }
    var modulo = objectCount % elementCount;
    var columnLength = (objectCount - modulo) / elementCount;
    var start = 0;
    var stop = 0;
    for(var i = 0; i< elementCount; i++) {
        var $element = $($elements[i]);
        stop += columnLength;
        if (i < modulo) {
            stop++;
        }
        for(var j = start; j < stop; j++) {
            $element.append(objectTemplate(objectArray[j]));
        }
        start = stop;
    }
};
Scrutari.Utils.checkPagination = function (ficheCount, plageLength, currentPlageNumber, jqArgument, paginationTemplate) {
    var $elements = Scrutari.convert(jqArgument);
    $elements.empty();
    var plageCount;
    if (ficheCount <= plageLength) {
        plageCount = 1;
        return;
    } else {
        var modulo = ficheCount % plageLength;
        plageCount = (ficheCount - modulo) / plageLength;
        if (modulo > 0) {
            plageCount ++;
        }
    }
    if (currentPlageNumber > plageCount) {
        currentPlageNumber = plageCount;
    }
    var plageNumberStart = 1;
    var plageNumberEnd = 9;
    if (currentPlageNumber > 6) {
        plageNumberStart = currentPlageNumber - 3;
        plageNumberEnd = currentPlageNumber + 3;
    }
    if (plageNumberEnd > plageCount) {
        plageNumberEnd = plageCount;
    }
    if (plageNumberStart > 1) {
        $elements.append(paginationTemplate({
                number: 1,
                title: "1",
                state: 'enabled'
        }));
        $elements.append(paginationTemplate({
                number: 0,
                title: "…",
                state: 'disabled'
        }));
    }
    for(var i = plageNumberStart; i <= plageNumberEnd; i++) {
        var state = 'enabled';
        if (i == currentPlageNumber) {
            state = 'active';
        }
        var html = paginationTemplate({
                number: i,
                title: i.toString(),
                state: state
        });
        $elements.append(html);
    }
    if (plageNumberEnd < plageCount) {
        $elements.append(paginationTemplate({
                number: 0,
                title: "…",
                state: 'disabled'
        }));
    }
};
Scrutari.Utils.disable = function (jqArgument) {
    var $elements = Scrutari.convert(jqArgument);
    $elements.prop('disabled', true);
    return $elements;
};
Scrutari.Utils.enable = function (jqArgument) {
    var $elements = Scrutari.convert(jqArgument);
    $elements.prop('disabled', false);
    return $elements;
};
Scrutari.Utils.uncheck = function (jqArgument) {
    var $elements = Scrutari.convert(jqArgument);
    $elements.prop('checked', false);
    return $elements;
};
Scrutari.Utils.check = function (jqArgument) {
    var $elements = Scrutari.convert(jqArgument);
    $elements.prop('checked', true);
    return $elements;
};
Scrutari.Utils.toggle = function (jqElement, stateDataKey) {
    var state = jqElement.data(stateDataKey);
    if (state === 'off') {
        state = 'on';
    } else {
        state = 'off';
    }
    jqElement.data(stateDataKey, state);
    return state;
};
Scrutari.Utils.toggle.getState = function (jqArgument, stateDataKey) {
    var $elements = Scrutari.convert(jqArgument);
    return $elements.data(stateDataKey);
};
Scrutari.Utils.toggle.disabled = function (jqArgument, state) {
    var $elements = Scrutari.convert(jqArgument);
    if (state === 'off') {
        $elements.prop('disabled', true);
    } else {
        $elements.prop('disabled', false);
    }
    return $elements;
};
Scrutari.Utils.toggle.text = function (jqArgument, alterDataKey) {
    var $elements = Scrutari.convert(jqArgument);
    var length = $elements.length;
    for(var i = 0; i < length; i++) {
        var jqEl = $($elements[i]);
        var currentText =jqEl.text();
        var alterText = jqEl.data(alterDataKey);
        jqEl.text(alterText);
        jqEl.data(alterDataKey, currentText);
    }
    return $elements;
};
Scrutari.Utils.toggle.classes = function (jqArgument, state, onClass, offClass) {
    var $elements = Scrutari.convert(jqArgument);
    if (state === 'off') {
        $elements.addClass(offClass).removeClass(onClass);
    } else {
        $elements.removeClass(offClass).addClass(onClass);
    }
    return $elements;
};
Scrutari.Utils.sortBaseByFicheCountDesc = function (base1, base2) {
    var count1 = base1.stats.fiche;
    var count2 = base2.stats.fiche;
    if (count1 > count2) {
        return -1;
    } else if (count1 < count2) {
        return 1;
    } else {
        var code1 = base1.codebase;
        var code2 = base2.codebase;
        if (code1 < code2) {
            return -1;
        } else if (code1 > code2) {
            return 1;
        } else {
            return 0;
        }
    }
};
Scrutari.Utils.sortLangByFicheCountDesc = function (lang1, lang2) {
    var count1 = lang1.fiche;
    var count2 = lang2.fiche;
    if (count1 > count2) {
        return -1;
    } else if (count1 < count2) {
        return 1;
    } else {
        var code1 = lang1.lang;
        var code2 = lang2.lang;
        if (code1 < code2) {
            return -1;
        } else if (code1 > code2) {
            return 1;
        } else {
            return 0;
        }
    }
};
Scrutari.Utils.sortCategoryByRankDesc = function (category1, category2) {
    var count1 = category1.rank;
    var count2 = category2.rank;
    if (count1 > count2) {
        return -1;
    } else if (count1 < count2) {
        return 1;
    } else {
        var code1 = category1.name;
        var code2 = category2.name;
        if (code1 < code2) {
            return -1;
        } else if (code1 > code2) {
            return 1;
        } else {
            return 0;
        }
    }
};
Scrutari.Utils.sortCorpusByFicheCountDesc = function (corpus1, corpus2) {
    var count1 = corpus1.stats.fiche;
    var count2 = corpus2.stats.fiche;
    if (count1 > count2) {
        return -1;
    } else if (count1 < count2) {
        return 1;
    } else {
        var code1 = corpus1.codecorpus;
        var code2 = corpus2.codecorpus;
        if (code1 < code2) {
            return -1;
        } else if (code1 > code2) {
            return 1;
        } else {
            return 0;
        }
    }
};
Scrutari.Utils.sortGroupByFicheCount = function (group1, group2) {
    var count1 = group1.ficheCount;
    var count2 = group2.ficheCount;
    if (count1 > count2) {
        return -1;
    } else if (count1 < count2) {
        return 1;
    } else {
        var rank1 = group1.category.rank;
        var rank2 = group1.category.rank;
        if (rank1 < rank2) {
            return -1;
        } else if (rank1 > rank2) {
            return 1;
        } else {
            return 0;
        }
    }
};
Scrutari.Utils.mark = function (markArray, classAttribute) {
    if (!classAttribute) {
        classAttribute = "scrutari-Mark";
    }
    var html = "";
    var length = markArray.length;
    for (var i = 0; i < length; i++) {
        var obj = markArray[i];
        if (typeof obj === 'string') {
            html += obj;
        } else if (obj.s) {
            html += "<span class='" + classAttribute + "'>";
            html += Scrutari.escape(obj.s);
            html += "</span>";
        }
    }
    return html;
};
Scrutari.Utils.buildPaginationTemplate = function (paginationIdPrefix) {
    var _paginationTemplate = function (obj) {
        var html = "";
        if (obj.state === 'active') {
            html += "<li class='active'>";
            html += "<span>";
            html += Scrutari.escape(obj.title);
            html += "</span>";
            html += "</li>";
        } else if (obj.state === 'disabled') {
            html += "<li class='disabled'>";
            html += "<span>";
            html += Scrutari.escape(obj.title);
            html += "</span>";
            html += "</li>";
        } else {
            html += "<li>";
            html += "<a href='#" + paginationIdPrefix + obj.number + "'>";
            html += Scrutari.escape(obj.title);
            html += "</a>";
            html += "</li>";
        }
        return html;   
    };
    return _paginationTemplate;
};
 Scrutari.Utils.buildFicheTemplate = function (scrutariMeta, scrutariLoc, scrutariResult, target) {
     var _attrToString = function (mattrMap, attributeArray) {
         var html = "";
         var length = attributeArray.length;
         for(var i = 0; i < length; i++) {
            var attribute = attributeArray[i];
            if (mattrMap.hasOwnProperty(attribute.name)) {
                 var valueArray = mattrMap[attribute.name];
                 var valLength = valueArray.length;
                 html += "<p class='scrutari-fiche-Attribute'>";
                 html += '<span class="scrutari-label-Attribute">';
                 html += attribute.title;
                 html += scrutariLoc.loc("_ scrutari-colon");
                 html += '</span>';
                 if (attribute.type === "block") {
                     html += '</p><p class="scrutari-fiche-AttrBlocks">…<br/>';
                     for(var j = 0; j< valLength; j++) {
                        if (j > 0) {
                            html += "<br/>…<br/>";
                        }
                        html +=Scrutari.Utils.mark(valueArray[j]);
                     }
                     html += '<br/>…</p>';
                 } else {
                     html += ' ';
                     for(var j = 0; j< valLength; j++) {
                        if (j > 0) {
                            html += ", ";
                        }
                        html +=Scrutari.Utils.mark(valueArray[j]);
                     }
                     html += "</p>";
                 }
            }
         }
         return html;
     };
     var _ficheTemplate = function (fiche) {
        var html = "";
        html += "<div class='scrutari-fiche-Block' id='ficheBlock_" + fiche.codefiche + "'>";
        if (fiche.hasOwnProperty("icon")) {
            html += "<div class='scrutari-fiche-Icon'><img alt='' src='" + fiche.icon + "'/></div>";
        }
        html += "<p class='scrutari-fiche-Titre'>";
        html += "<a href='" + fiche.href + "'";
        html += " class='scrutari-fiche-Link'";
        html += " id='ficheLink_" + fiche.codefiche + "'";
        if (target.length > 0) {
            html += " target='" + target + "'";
        }
        html += ">";
        if (fiche.hasOwnProperty("mtitre")) {
            html +=Scrutari.Utils.mark(fiche.mtitre);
        } else {
            html += fiche.href;
        }
        html += "</a>";
        html += "</p>";
        if (fiche.hasOwnProperty("msoustitre")) {
            html += "<p class='scrutari-fiche-Soustitre'>";
            html +=Scrutari.Utils.mark(fiche.msoustitre);
            html += "</p>";
        }
        if (fiche.hasOwnProperty("annee")) {
            html += "<p class='scrutari-fiche-Annee'>";
            html += fiche.annee;
            html += "</p>";
        }
        if (fiche.hasOwnProperty("mattrMap")) {
            html += _attrToString(fiche.mattrMap, scrutariMeta.getAttributeArray("primary"));
        }
        if (fiche.hasOwnProperty("mcomplementArray")) {
            var complementLength = fiche.mcomplementArray.length;
            for(var i = 0; i < complementLength; i++) {
                var complement = fiche.mcomplementArray[i];
                html += "<p class='scrutari-fiche-Complement'>";
                html += '<span class="scrutari-label-Complement">';
                html += scrutariMeta.getComplementIntitule(fiche.codecorpus, complement.num);
                html += scrutariLoc.loc("_ scrutari-colon");
                html += '</span> ';
                html +=Scrutari.Utils.mark(complement.mcomp);
                html += "</p>";
            }
        }
        if (fiche.hasOwnProperty("mattrMap")) {
            html += _attrToString(fiche.mattrMap, scrutariMeta.getAttributeArray("secondary"));
        }
        if (fiche.hasOwnProperty('codemotcleArray')) {
            var length = fiche.codemotcleArray.length;
            html += '<p class="scrutari-fiche-Motcle">';
            html += '<span class="scrutari-label-Motcle">';
            if (length === 1) {
                html += scrutariLoc.loc("_ scrutari-motscles_un");
            } else {
                html += scrutariLoc.loc("_ scrutari-motscles_plusieurs");
            }
            html += '</span> ';
            for(var i = 0; i < length; i++) {
                if (i > 0) {
                    html += ", ";
                }
                var code = fiche.codemotcleArray[i];
                var motcle = scrutariResult.getMotcle(code);
                var libelleLength = motcle.mlibelleArray.length;
                for(var j = 0; j < libelleLength; j++) {
                    if (j > 0) {
                        html += "/";
                    }
                    html +=Scrutari.Utils.mark(motcle.mlibelleArray[j].mlib);
                }
            }
            html += "</p>";
        }
        html += '</div>';
        return html;
    };
    return _ficheTemplate;
 };
Scrutari.Filter = function () {
    this.withBase = false;
    this.withCategory = false;
    this.withBase = false;
    this.withCorpus = false;
    this.baseArray = new Array();
    this.langArray = new Array();
    this.categoryArray = new Array();
    this.corpusArray = new Array();
};
Scrutari.Filter.prototype.clearArrays = function () {
    while(this.baseArray.length > 0) {
        this.baseArray.pop();
    }
    while(this.langArray.length > 0) {
        this.langArray.pop();
    }
    while(this.categoryArray.length > 0) {
        this.categoryArray.pop();
    }
    while(this.corpusArray.length > 0) {
        this.corpusArray.pop();
    }
};
Scrutari.Filter.prototype.setWithBase = function (bool) {
    this.withBase = bool;
};
Scrutari.Filter.prototype.setWithLang = function (bool) {
    this.withLang = bool;
};
Scrutari.Filter.prototype.setWithCategory = function (bool) {
    this.withCategory = bool;
};
Scrutari.Filter.prototype.setWithCorpus = function (bool) {
    this.withCorpus = bool;
};
Scrutari.Filter.prototype.addBase = function (base) {
    this.baseArray.push(base);
};
Scrutari.Filter.prototype.addLang = function (lang) {
    this.langArray.push(lang);
};
Scrutari.Filter.prototype.addCategory = function (category) {
    this.categoryArray.push(category);
};
Scrutari.Filter.prototype.addCorpus = function (corpus) {
    this.corpusArray.push(corpus);
};
Scrutari.Filter.prototype.checkRequestParameters = function (scrutariMeta, requestParameters) {
    if (this.hasLang()) {
        requestParameters.langlist = this.langArray.join(",");
    }
    if (this.hasBase()) {
        requestParameters.baselist = this.baseArray.join(",");
    }
    if (this.hasCategory()) {
        requestParameters.categorylist = this.categoryArray.join(",");
    }
    if (this.hasCorpus()) {
        requestParameters.corpuslist = this.corpusArray.join(",");
    }
};
Scrutari.Filter.prototype.getFilterFicheCount = function (scrutariMeta) {
    if (!this.hasFilter()) {
        return scrutariMeta.getGlobalFicheCount();
    }
    var filterFicheCount = 0;
    if (this.hasCorpusFilter()) {
        var corpusMap = this.checkCorpusMap(scrutariMeta);
        var completeValue = corpusMap.completeValue;
        for(var key in corpusMap) {
            if (key === 'completeValue') {
                continue;
            }
            if (corpusMap[key] === completeValue) {
                var codecorpus = parseInt(key.substring(5));
                if (this.hasLang()) {
                    filterFicheCount += scrutariMeta.getCorpusLangFicheCount(codecorpus, this.langArray);
                } else {
                    filterFicheCount += scrutariMeta.getCorpusFicheCount(codecorpus);
                }
            }
        }
    } else {
        filterFicheCount += scrutariMeta.getGlobalLangFicheCount(this.langArray);
    }
    return filterFicheCount;
};
Scrutari.Filter.prototype.hasFilter = function () {
    if (this.hasLang()) {
        return true;
    }
    if (this.hasBase()) {
        return true;
    }
    if (this.hasCategory()) {
        return true;
    }
    if (this.hasCorpus()) {
        return true;
    }
    return false;
};
Scrutari.Filter.prototype.hasCorpusFilter = function () {
    if (this.hasBase()) {
        return true;
    }
    if (this.hasCategory()) {
        return true;
    }
    if (this.hasCorpus()) {
        return true;
    }
    return false;
};
Scrutari.Filter.prototype.checkCorpusMap = function (scrutariMeta) {
    var corpusMap = new Object();
    var finalCount = 0;
    if (this.hasCategory())  {
        finalCount++;
        var arrayForCategories = scrutariMeta.getCorpusArrayForCategories(this.categoryArray);
        for(var i = 0; i < arrayForCategories.length; i++) {
            var key = "code_" + arrayForCategories[i];
            if (corpusMap.hasOwnProperty(key)) {
                corpusMap[key] = corpusMap[key] + 1;
            } else {
                corpusMap[key] = 1;
            }
        }
    }
    if (this.hasBase())  {
        finalCount++;
        var arrayForBases = scrutariMeta.getCorpusArrayForBases(this.baseArray);
        for(var i = 0; i < arrayForBases.length; i++) {
            var key = "code_" + arrayForBases[i];
            if (corpusMap.hasOwnProperty(key)) {
                corpusMap[key] = corpusMap[key] + 1;
            } else {
                corpusMap[key] = 1;
            }
        }
    }
    if (this.hasCorpus())  {
        finalCount++;
        for(var i = 0; i < this.corpusArray.length; i++) {
            var key = "code_" + this.corpusArray[i];
            if (corpusMap.hasOwnProperty(key)) {
                corpusMap[key] = corpusMap[key] + 1;
            } else {
                corpusMap[key] = 1;
            }
        }
    }
    corpusMap.completeValue = finalCount;
    return corpusMap;
};
Scrutari.Filter.prototype.hasLang = function () {
    return ((this.withLang) && (this.langArray.length > 0));
};
Scrutari.Filter.prototype.hasBase = function () {
    return ((this.withBase) && (this.baseArray.length > 0));
};
Scrutari.Filter.prototype.hasCategory = function () {
    return ((this.withCategory) && (this.categoryArray.length > 0));
};
Scrutari.Filter.prototype.hasCorpus = function () {
    return ((this.withCorpus) && (this.corpusArray.length > 0));
};
Scrutari.Result = function (ficheSearchResult, groupSortFunction) {
    this.ficheSearchResult = ficheSearchResult;
    this.ficheGroupArray = ficheSearchResult.ficheGroupArray;
    if ((groupSortFunction) && (this.ficheGroupArray.length > 1)) {
        this.ficheGroupArray = this.ficheGroupArray.sort(groupSortFunction);
    }
    this.motcleMap = new Object();
    if (ficheSearchResult.hasOwnProperty("motcleArray")) {
        var length = ficheSearchResult.motcleArray.length;
        for(var i = 0; i < length; i++) {
            var motcle = ficheSearchResult.motcleArray[i];
            this.motcleMap["code_" + motcle.codemotcle] = motcle;
        }
    }
};
Scrutari.Result.newSearch = function (callback, scrutariConfig, requestParameters, scrutariErrorCallback) {
    var _ficheSearchResultCallback = function (ficheSearchResult) {
        callback(new Scrutari.Result(ficheSearchResult, scrutariConfig.getGroupSortFunction()));
    };
    Scrutari.Ajax.loadFicheSearchResult(_ficheSearchResultCallback, scrutariConfig, requestParameters, scrutariErrorCallback);
};
Scrutari.Result.prototype.getQId = function () {
    return this.ficheSearchResult.qId;
};
Scrutari.Result.prototype.getFormatedSearchSequence = function (scrutariLoc) {
    var q = this.ficheSearchResult.q;
    q = q.replace(/\&\&/g, scrutariLoc.loc('_ scrutari-and'));
    q = q.replace(/\|\|/g, scrutariLoc.loc('_ scrutari-or'));
    return q;
};
Scrutari.Result.prototype.getFicheCount = function () {
    return this.ficheSearchResult.ficheCount;
};
Scrutari.Result.prototype.getFicheGroupType = function () {
    return this.ficheSearchResult.ficheGroupType;
};
Scrutari.Result.prototype.getNoneFicheArray = function () {
    if (this.ficheGroupArray.length === 0) {
        return new Array();
    }
    return this.ficheGroupArray[0].ficheArray;
};
Scrutari.Result.prototype.selectNoneFicheArray = function (plageLength, plageNumber) {
    var selectionArray = new Array();
    if (this.ficheGroupArray.length === 0) {
        return selectionArray;
    }
    var ficheArray = this.ficheGroupArray[0].ficheArray;
    var startIndex = plageLength * (plageNumber - 1);
    var length = ficheArray.length;
    if (startIndex >= length) {
        return selectionArray;
    }
    var min = Math.min(ficheArray.length, startIndex + plageLength);
    for(var i = startIndex; i < min; i++) {
        selectionArray.push(ficheArray[i]);
    }
    return selectionArray;
};
Scrutari.Result.prototype.isNonePlageLoaded = function (plageLength, plageNumber) {
    if (this.ficheGroupArray.length === 0) {
        return true;
    }
    var ficheCount = this.ficheSearchResult.ficheCount;
    var ficheArray = this.ficheGroupArray[0].ficheArray;
    var length = ficheArray.length;
    if (length === ficheCount) {
        return true;
    }
    var endIndex = (plageLength * plageNumber) - 1;
    if (endIndex < length) {
        return true;
    }
    return false;
};
Scrutari.Result.prototype.loadNonePlage = function (callback, scrutariConfig, plageLength, plageNumber) {
    if (this.ficheGroupArray.length === 0) {
        return true;
    }
    var group = this.ficheGroupArray[0];
    if (!group) {
        return;
    }
    var ficheCount = this.ficheSearchResult.ficheCount;
    var ficheArray = group.ficheArray;
    var length = ficheArray.length;
    if (length === ficheCount) {
        return;
    }
    var _existingFicheSearchResultCallback = function (ficheSearchResult) {
        var newCount = ficheSearchResult.ficheGroupArray.length;
        if (newCount > 0) {
            group.ficheArray = group.ficheArray.concat(ficheSearchResult.ficheGroupArray[0].ficheArray);
        }
        callback();
    };
    var requestParameters = {
        qid: this.ficheSearchResult.qId,
        start: length +1,
        limit: (plageLength * (plageNumber + 2)) - length
    };
    Scrutari.Ajax.loadExistingFicheSearchResult(_existingFicheSearchResultCallback, scrutariConfig, requestParameters); 
};
Scrutari.Result.prototype.isCategoryPlageLoaded = function (categoryName, plageLength, plageNumber) {
    var group = this.getFicheGroupByCategoryName(categoryName);
    if (!group) {
        return true;
    }
    var categoryFicheCount = group.ficheCount;
    var ficheArray = group.ficheArray;
    var length = ficheArray.length;
    if (length === categoryFicheCount) {
        return true;
    }
    var endIndex = (plageLength * plageNumber) - 1;
    if (endIndex < length) {
        return true;
    }
    return false;
};
Scrutari.Result.prototype.loadCategoryPlage = function (callback, scrutariConfig, categoryName, plageLength, plageNumber) {
    var group = this.getFicheGroupByCategoryName(categoryName);
    if (!group) {
        return;
    }
    var categoryFicheCount = group.ficheCount;
    var ficheArray = group.ficheArray;
    var length = ficheArray.length;
    if (length === categoryFicheCount) {
        return;
    }
    var _existingFicheSearchResultCallback = function (ficheSearchResult) {
        var newCount = ficheSearchResult.ficheGroupArray.length;
        for(var i = 0; i < newCount; i++) {
            var newGroup = ficheSearchResult.ficheGroupArray[i];
            if (newGroup.category.name === group.category.name) {
                group.ficheArray = group.ficheArray.concat(newGroup.ficheArray);
            }
        }
        callback();
    };
    var requestParameters = {
        qid: this.ficheSearchResult.qId,
        start: length +1,
        limit: (plageLength * (plageNumber + 2)) - length,
        starttype: "in:" + categoryName
    };
    Scrutari.Ajax.loadExistingFicheSearchResult(_existingFicheSearchResultCallback, scrutariConfig, requestParameters); 
};
Scrutari.Result.prototype.selectCategoryFicheArray = function (categoryName, plageLength, plageNumber) {
    var selectionArray = new Array();
    var ficheArray = this.getCategoryFicheArrayByName(categoryName);
    var startIndex = plageLength * (plageNumber - 1);
    var length = ficheArray.length;
    if (startIndex >= length) {
        return selectionArray;
    }
    var min = Math.min(ficheArray.length, startIndex + plageLength);
    for(var i = startIndex; i < min; i++) {
        selectionArray.push(ficheArray[i]);
    }
    return selectionArray;
};
Scrutari.Result.prototype.getCategoryCount = function () {
    return this.ficheGroupArray.length;
};
Scrutari.Result.prototype.getCategory = function (index) {
    return this.ficheGroupArray[index].category;
};
Scrutari.Result.prototype.getFicheGroupByCategoryName = function (categoryName) {
    var groupCount = this.ficheGroupArray.length;
    for(var i = 0; i < groupCount; i++) {
        var group = this.ficheGroupArray[i];
        if ((group.category) && (group.category.name === categoryName)) {
            return group;
        }
    }
    return null;
};
Scrutari.Result.prototype.getCategoryFicheCount = function (index) {
    return this.ficheGroupArray[index].ficheCount;
};
Scrutari.Result.prototype.getCategoryFicheCountbyName = function (categoryName) {
    var groupCount = this.ficheGroupArray.length;
    for(var i = 0; i < groupCount; i++) {
        var group = this.ficheGroupArray[i];
        if ((group.category) && (group.category.name === categoryName)) {
            return group.ficheCount;
        }
    }
    return 0;
};
Scrutari.Result.prototype.getCategoryFicheArray = function (index) {
    return this.ficheGroupArray[index].ficheArray;
};
Scrutari.Result.prototype.getCategoryFicheArrayByName = function (categoryName) {
    var categoryCount = this.getCategoryCount();
    for(var i = 0; i < categoryCount; i++) {
        var category = this.getCategory(i);
        if (category.name === categoryName) {
            return this.getCategoryFicheArray(i);
        }
    }
    return new Array();
};
Scrutari.Result.prototype.getMotcle = function (code) {
    var key = "code_" + code;
    if (this.motcleMap.hasOwnProperty(key)) {
        return this.motcleMap[key];
    } else {
        return null;
    }
};
Engine = function (scrutariConfig, locMap) {
    this.scrutariConfig = scrutariConfig;
    this.scrutariLoc = new Scrutari.Loc(locMap);
    this.withCorpus = false;
    this.scrutariMeta = null;
    this.historyNumber = 0;
    this.waiting = false;
    this.baseSort = "fiche-count";
    this.corpusSort = "fiche-count";
    this.mode = "";
    this.target = "_blank";
    this.initialQuery = "";
    this.initialQId = "";
    this.currentScrutariResult = null;
    this.changePlageCallbackFunction = null;
    this.hiddenList = new Array();
    this.bootstrapVersion = "3";
    this.permalinkPattern = null;
    this.templateFactoryArray = new Array();
};
Engine.prototype.getScrutariConfig = function () {
    return this.scrutariConfig;
};
Engine.prototype.loc = function (locKey) {
    return this.scrutariLoc.loc(locKey);
};
Engine.prototype.getScrutariLoc = function () {
    return this.scrutariLoc;
};
Engine.prototype.isWithCorpus = function () {
    return this.withCorpus;
};
Engine.prototype.setWithCorpus = function (withCorpus) {
    this.withCorpus = withCorpus;
};
Engine.prototype.isWithPermalink = function () {
    return (this.permalinkPattern != null);
};
Engine.prototype.setPermalinkPattern = function (permalinkPattern) {
    this.permalinkPattern = permalinkPattern;
};
Engine.prototype.toPermalink = function (qId) {
    if (this.permalinkPattern) {
        return this.scrutariConfig.getPermalinkUrl(qId, this.permalinkPattern);
    }
    return null;
};
Engine.prototype.getPermalinkPattern = function () {
    return permalinkPattern;
};
Engine.prototype.getScrutariMeta = function () {
    return this.scrutariMeta;
};
Engine.prototype.setScrutariMeta = function (scrutariMeta) {
    this.scrutariMeta = scrutariMeta;
};
Engine.prototype.getScrutariLoc = function () {
    return this.scrutariLoc;
};
Engine.prototype.newHistoryNumber = function () {
    this.historyNumber++;
    return this.historyNumber;
};
Engine.prototype.isWaiting = function () {
    return this.waiting;
};
Engine.prototype.setWaiting = function (bool) {
    this.waiting = bool;
};
Engine.prototype.getBaseSort = function () {
    return this.baseSort;
};
Engine.prototype.setBaseSort = function (baseSort) {
    this.baseSort = baseSort;
};
Engine.prototype.getCorpusSort = function () {
    return this.corpusSort;
};
Engine.prototype.setCorpusSort = function (corpusSort) {
    this.corpusSort = corpusSort;
};
Engine.prototype.getMode = function () {
    return this.mode;
};
Engine.prototype.setMode = function (mode) {
    this.mode = mode;
};
Engine.prototype.getTarget = function () {
    return this.target;
};
Engine.prototype.setTarget = function (target) {
    this.target = target;
};
Engine.prototype.getInitialQuery = function () {
    return this.initialQuery;
};
Engine.prototype.setInitialQuery = function (initialQuery) {
    this.initialQuery = initialQuery;
};
Engine.prototype.getInitialQId = function () {
    return this.initialQId;
};
Engine.prototype.setInitialQId = function (initialQId) {
    this.initialQId = initialQId;
};
Engine.prototype.getBootstrapVersion = function () {
    return this.bootstrapVersion;
};
Engine.prototype.setBootstrapVersion = function (bootstrapVersion) {
    this.bootstrapVersion = bootstrapVersion;
};
Engine.prototype.setTarget = function (target) {
    this.target = target;
};
Engine.prototype.hasCurrentScrutariResult = function () {
    return (this.currentScrutariResult != null);
};
Engine.prototype.getCurrentScrutariResult = function () {
    return this.currentScrutariResult;
};
Engine.prototype.setCurrentScrutariResult = function (scrutariResult) {
    this.currentScrutariResult = scrutariResult;
};
Engine.prototype.setChangePlageCallbackFunction = function (changePlageCallbackFunction) {
    this.changePlageCallbackFunction = changePlageCallbackFunction;
};
Engine.prototype.changePlageCallback = function (plageFicheArray, changePlageArguments) {
    if (this.changePlageCallbackFunction) {
        this.changePlageCallbackFunction(plageFicheArray, changePlageArguments);
    }
};
Engine.prototype.setHiddenList = function (hiddenArg) {
    if ($.type(hiddenArg) === "string") {
        this.hiddenList = hiddenArg.split(",");
    } else {
        this.hiddenList = hiddenArg;
    }
    for(var i = 0, length = this.hiddenList.length; i < length; i++) {
        this.hiddenList[i] = $.trim(this.hiddenList[i]);
    }
};
Engine.prototype.getHiddenList = function () {
    return this.hiddenList;
};
Engine.prototype.isHidden = function (elementName) {
    return $.inArray(elementName, this.hiddenList) > -1;
};
Engine.prototype.addTemplateFactory = function (templateFactory) {
    this.templateFactoryArray.push(templateFactory);
};
Engine.prototype.getTemplate = function (templateName, templateParameter) {
    var factoryLength = this.templateFactoryArray.length;
    if (factoryLength > 0) {
        for(var i = (factoryLength -1); i >= 0; i--) {
            var templateFactory = this.templateFactoryArray[i];
            var template = templateFactory(this, templateName, templateParameter);
            if (template) {
                return template;
            }
        }
    }
    switch(templateName) {
        case "base":
            return Engine.baseTemplate;
        case "category":
            return Engine.categoryTemplate;
        case "corpus":
            return Engine.corpusTemplate;
        case "lang":
            var scrutariMeta = this.scrutariMeta;
            return function (lang) {
                var code = lang.lang;
                var html = "";
                html += "<p>";
                html += "<label class='scrutari-Label'><input type='checkbox' name='lang' value='" + code + "'> ";
                var label = scrutariMeta.getLangLabel(code);
                if (label !== code) {
                    html += label;
                    html += " ";
                }
                html += "[" + code + "]";
                html += " (" + lang.fiche +")";
                html += "</label>";
                html += "</p>";
                return html;
            };
        case "pagination":
            return Scrutari.Utils.buildPaginationTemplate(templateParameter);
        case "fiche":
            return Scrutari.Utils.buildFicheTemplate(this.getScrutariMeta(), this.getScrutariLoc(), templateParameter, this.getTarget());
        default:
            return function () {
                return "Unknwon template: " + templateName;
            };
    }
};
Engine.baseTemplate = function (base) {
    var code = base.codebase;
    var html = "";
    html += "<p>";
    html += "<label class='scrutari-Label'><input type='checkbox' name='base' value='" + code + "'> ";
    if (base.baseicon) {
        html += "<img src='" + base.baseicon + "' alt='' class='scrutari-panel-Icon'> ";
    }
    html += Scrutari.escape(base.intitules.short);
    html += " (" + base.stats.fiche +")";
    html += "</label>";
    html += "</p>";
    return html;
};
Engine.categoryTemplate = function (category) {
    var name = category.name;
    var html = "";
    html += "<p>";
    html += "<label class='scrutari-Label'><input type='checkbox' name='category' value='" + name + "'> ";
    html +=  Scrutari.escape(category.title);
    html += " (" + category.stats.fiche +")";
    html += "</label>";
    html += "</p>";
    return html;
};
Engine.corpusTemplate = function (corpus) {
    var code = corpus.codecorpus;
    var html = "";
    html += "<p>";
    html += "<label class='scrutari-Label'><input type='checkbox' name='corpus' value='" + code + "'> ";
    html += Scrutari.escape(corpus.intitules.corpus);
    html += " (" + corpus.stats.fiche +")";
    html += "</label>";
    html += "</p>";
    return html;
};
Engine.Init = {};
Engine.Init.html = function (engine, jqArgument) {
    if  ((!Engine.html) || (!Engine.html.list) || (!Engine.html.list.length)) {
        Scrutari.log("Engine.html.list is undefined");
        return;
    }
    var _hide = function (key, text) {
        var key_start = "<!-- " + key + " -->";
        var key_end = "<!-- /" + key + " -->";
        var start_index = text.indexOf(key_start);
        var end_index = text.indexOf(key_end);
        if ((start_index > -1) && (end_index > -1) && (start_index < end_index)) {
            text = text.substring(0, start_index) + text.substring(end_index + key_end.length);
        }
        return text;
    }
    var html = "";
    for(var i= 0, len = Engine.html.list.length; i < len; i++) {
        var key = Engine.html.list[i];
        if (Engine.html.hasOwnProperty(key)) {
            html += Engine.html[key];
        }
    }
    var hiddenList = engine.getHiddenList();
    for(var i =0, length = hiddenList.length; i < length ; i++) {
        html = _hide(hiddenList[i], html);
    }
    if (engine.isWithCorpus()) {
         html = _hide("base-panel", html);
    } else {
         html = _hide("corpus-panel", html);
    }
    if (!engine.isWithPermalink()) {
        html = _hide("share", html);
    }
    html = html.replace(/_ [-_\.a-z0-9]+/g, function(match) {
        return engine.loc(match);
    });
    $(jqArgument).html(html);
};
Engine.Init.mainTitle = function (engine) {
    var html = "";
    if ($.isFunction(engine)) {
        html = engine.call(this);
    } else if (typeof engine === "string") {
        html = engine;
    } else {
        html += engine.loc('_ title_main');
        html += " – [";
        html += engine.getScrutariConfig().getName();
        html += "]";
    }
    $("#mainTitle").html(html);
};
Engine.Init.custom = function (engine) {
};
Engine.Init.forms = function (engine) {
    $("#mainsearchForm").submit(function () {
        var q = $(this).find("input[name='q']").val();
        q = $.trim(q);
        if (q.length > 0) {
            var requestParameters = Engine.Ui.checkRequestParameters(engine);
            requestParameters["log"] = "all";
            requestParameters["q"] = q;
            var _mainsearchScrutariResultCallback = function (scrutariResult) {
                Engine.Query.scrutariResult(engine, scrutariResult, "mainsearch");
                $("#loadingModal").modal('hide');
                $parametersDisplayButton = $('#parametersDisplayButton');
                if ($parametersDisplayButton.data("scrutariState") === "on") {
                    $parametersDisplayButton.click();
                }
            };
            var _mainScrutariErrorCallback = function (error) {
                Engine.Query.scrutariError(engine, error, "mainsearch");
                $("#loadingModal").modal('hide');
            };
            $("#loadingModal").modal('show');
            Scrutari.Result.newSearch(_mainsearchScrutariResultCallback, engine.getScrutariConfig(), requestParameters, _mainScrutariErrorCallback);
        }
        return false;
    });
    $("#subsearchForm").submit(function () {
        var q = $(this).find("input[name='q']").val();
        q = $.trim(q);
        if ((q.length > 0) && (engine.hasCurrentScrutariResult())) {
            var requestParameters = Engine.Ui.checkRequestParameters(engine);
            requestParameters["q"] = q;
            requestParameters["flt-qid"] = engine.getCurrentScrutariResult().getQId();
            var _subsearchScrutariResultCallback = function (scrutariResult) {
                Engine.Query.scrutariResult(engine, scrutariResult, "subsearch");
                $("#loadingModal").modal('hide');
            };
            var _subsearchScrutariErrorCallback = function (error) {
                Engine.Query.scrutariError(engine, error, "subsearch");
                $("#loadingModal").modal('hide');
            };
            $("#loadingModal").modal('show');
            Scrutari.Result.newSearch(_subsearchScrutariResultCallback, engine.getScrutariConfig(), requestParameters, _subsearchScrutariErrorCallback);
        }
        return false;
    });
};
Engine.Init.buttons = function (engine) {
    $('#langEnableButton').click(function () {
        var $this = $(this);
        var state = Scrutari.Utils.toggle($this, "scrutariState");
        Scrutari.Utils.toggle.disabled("#langCheckAllButton, #langUncheckAllButton", state);
        Scrutari.Utils.toggle.classes(".scrutari-column-Lang", state, "", "scrutari-Disabled");
        Scrutari.Utils.toggle.text($this.children("span"), "scrutariAlternate");
        var $langFilterLabel = Scrutari.Utils.toggle.text("#langFilterLabel", "scrutariAlternate");
        Scrutari.Utils.toggle.classes($langFilterLabel, state, "scrutari-panel-Active", "scrutari-Disabled");
        Engine.Ui.filterChange(engine);
    });
    $('#langCheckAllButton').click(function () {
        Scrutari.Utils.check("input[name='lang']");
        Engine.Ui.filterChange(engine);
    });
    $('#langUncheckAllButton').click(function () {
        Scrutari.Utils.uncheck("input[name='lang']");
        Engine.Ui.filterChange(engine);
    });
    $('#categoryEnableButton').click(function () {
        var $this = $(this);
        var state = Scrutari.Utils.toggle($this, "scrutariState");
        Scrutari.Utils.toggle.disabled("#categoryCheckAllButton, #categoryUncheckAllButton", state);
        Scrutari.Utils.toggle.classes(".scrutari-column-Category", state, "", "scrutari-Disabled");
        Scrutari.Utils.toggle.text($this.children("span"), "scrutariAlternate");
        var $categoryFilterLabel = Scrutari.Utils.toggle.text("#categoryFilterLabel", "scrutariAlternate");
        Scrutari.Utils.toggle.classes($categoryFilterLabel, state, "scrutari-panel-Active", "scrutari-Disabled");
        Engine.Ui.filterChange(engine);
    });
    $('#categoryCheckAllButton').click(function () {
        Scrutari.Utils.check("input[name='category']");
        Engine.Ui.filterChange(engine);
    });
    $('#categoryUncheckAllButton').click(function () {
        Scrutari.Utils.uncheck("input[name='category']");
        Engine.Ui.filterChange(engine);
    });
    if (engine.isWithCorpus()) {
        $('#corpusEnableButton').click(function () {
            var $this = $(this);
            var state = Scrutari.Utils.toggle($this, "scrutariState");
            Scrutari.Utils.toggle.disabled("#corpusCheckAllButton, #corpusUncheckAllButton", state);
            Scrutari.Utils.toggle.classes(".scrutari-column-Corpus", state, "", "scrutari-Disabled");
            Scrutari.Utils.toggle.text($this.children("span"), "scrutariAlternate");
            var $corpusFilterLabel = Scrutari.Utils.toggle.text("#corpusFilterLabel", "scrutariAlternate");
            Scrutari.Utils.toggle.classes($corpusFilterLabel, state, "scrutari-panel-Active", "scrutari-Disabled");
            Engine.Ui.filterChange(engine);
        });
        $('#corpusCheckAllButton').click(function () {
            Scrutari.Utils.check("input[name='corpus']");
            Engine.Ui.filterChange(engine);
        });
        $('#corpusUncheckAllButton').click(function () {
            Scrutari.Utils.uncheck("input[name='corpus']");
            Engine.Ui.filterChange(engine);
        });
    } else {
        $('#baseEnableButton').click(function () {
            var $this = $(this);
            var state = Scrutari.Utils.toggle($this, "scrutariState");
            Scrutari.Utils.toggle.disabled("#baseCheckAllButton, #baseUncheckAllButton", state);
            Scrutari.Utils.toggle.classes(".scrutari-column-Base", state, "", "scrutari-Disabled");
            Scrutari.Utils.toggle.text($this.children("span"), "scrutariAlternate");
            var $baseFilterLabel = Scrutari.Utils.toggle.text("#baseFilterLabel", "scrutariAlternate");
            Scrutari.Utils.toggle.classes($baseFilterLabel, state, "scrutari-panel-Active", "scrutari-Disabled");
            Engine.Ui.filterChange(engine);
        });
        $('#baseCheckAllButton').click(function () {
            Scrutari.Utils.check("input[name='base']");
            Engine.Ui.filterChange(engine);
        });
        $('#baseUncheckAllButton').click(function () {
            Scrutari.Utils.uncheck("input[name='base']");
            Engine.Ui.filterChange(engine);
        });
    }
    $('#parametersDisplayButton').click(function () {
        var state = Scrutari.Utils.toggle($(this), "scrutariState");
        if (state === 'on') {
            Engine.Ui.show('#parametersArea');
        } else {
            Engine.Ui.hide('#parametersArea');
        }
    });
    $('#modeHelpButton').click(function () {
        $modeHelpModal = $("#modeHelpModal");
        $modalBody = $modeHelpModal.find(".modal-body");
        if ($modalBody.children().length === 0) {
            $modalBody.html(engine.loc("_ help_mode.html"));
        }
        $modeHelpModal.modal('show');
    });
    $('#ponderationHelpButton').click(function () {
        $ponderationHelpModal = $("#ponderationHelpModal");
        $modalBody = $ponderationHelpModal.find(".modal-body");
        if ($modalBody.children().length === 0) {
            $modalBody.html(engine.loc("_ help_ponderation.html"));
        }
        $ponderationHelpModal.modal('show');
    });
    $('#periodeHelpButton').click(function () {
        $periodeHelpModal = $("#periodeHelpModal");
        $modalBody = $periodeHelpModal.find(".modal-body");
        if ($modalBody.children().length === 0) {
            $modalBody.html(engine.loc("_ help_periode.html"));
        }
        $periodeHelpModal.modal('show');
    });
    $("input[name='q-mode']").click(function () {
        if (this.value === 'operation') {
            Scrutari.Utils.disable("input[name='wildchar']");
        } else {
            Scrutari.Utils.enable("input[name='wildchar']");
        }
    });
};
Engine.Init.scrutariMeta = function (engine, scrutariMeta) {
    engine.setScrutariMeta(scrutariMeta);
    Engine.Init.forms(engine);
    Engine.Init.buttons(engine);
    Engine.Ui.setCount("#globalFicheCount", scrutariMeta.getGlobalFicheCount(), engine.getScrutariConfig().getLangUi());
    Engine.Ui.show("#globalFicheCount");
    Engine.Ui.show("#parametersDisplayButton");
    var langArray = scrutariMeta.getLangArray(Scrutari.Utils.sortLangByFicheCountDesc);
    if ((langArray.length > 1) && (Scrutari.exists("#langPanel"))) {
        Scrutari.Utils.divideIntoColumns(langArray, ".scrutari-column-Lang", engine.getTemplate("lang"));
            Engine.Ui.show("#langPanel");
            $("input[name='lang']").click(function () {
                var button = $('#langEnableButton');
                if (Scrutari.Utils.toggle.getState(button, 'scrutariState') === 'off') {
                    button.click();
                    $(this).focus();
                } else {
                    Engine.Ui.filterChange(engine);
                }
            });
    }
    if ((scrutariMeta.withCategory()) && (Scrutari.exists("#categoryPanel"))) {
        var categoryArray = scrutariMeta.getCategoryArray(Scrutari.Utils.sortCategoryByRankDesc);
        Scrutari.Utils.divideIntoColumns(categoryArray, ".scrutari-column-Category", engine.getTemplate("category"));
        Engine.Ui.show("#categoryPanel");
        $("input[name='category']").click(function () {
            var button = $('#categoryEnableButton');
            if (Scrutari.Utils.toggle.getState(button, 'scrutariState') === 'off') {
                button.click();
                $(this).focus();
            } else {
                Engine.Ui.filterChange(engine);
            }
        });
    }
    if (engine.isWithCorpus()) {
        if (Scrutari.exists("#corpusPanel")) {
            var corpusSortFunction = null;
            if (engine.getCorpusSort() === "fiche-count") {
                corpusSortFunction = Scrutari.Utils.sortCorpusByFicheCountDesc;
            }
            var corpusArray =  scrutariMeta.getCorpusArray(corpusSortFunction);
            if (corpusArray.length > 1) {
                Scrutari.Utils.divideIntoColumns(corpusArray, ".scrutari-column-Corpus", engine.getTemplate("corpus"));
                Engine.Ui.show("#corpusPanel");
                $("input[name='corpus']").click(function () {
                    var button = $('#corpusEnableButton');
                    if (Scrutari.Utils.toggle.getState(button, 'scrutariState') === 'off') {
                        button.click();
                        $(this).focus();
                    } else {
                        Engine.Ui.filterChange(engine);
                    }
                });
            }
        }
    } else {
        if (Scrutari.exists("#basePanel")) {
            var baseSortFunction = null;
            if (engine.getBaseSort() === "fiche-count") {
                baseSortFunction = Scrutari.Utils.sortBaseByFicheCountDesc;
            }
            var baseArray =  scrutariMeta.getBaseArray(baseSortFunction);
            if (baseArray.length > 1) {
                Scrutari.Utils.divideIntoColumns(baseArray, ".scrutari-column-Base", engine.getTemplate("base"));
                Engine.Ui.show("#basePanel");
                $("input[name='base']").click(function () {
                    var button = $('#baseEnableButton');
                    if (Scrutari.Utils.toggle.getState(button, 'scrutariState') === 'off') {
                        button.click();
                        $(this).focus();
                    } else {
                        Engine.Ui.filterChange(engine);
                    }
                });
            }
        }
    }
    var initialQuery = engine.getInitialQuery();
    var initialQId = engine.getInitialQId();
    if (initialQuery.length > 0) {
        $mainSearchForm = $("#mainsearchForm");
        if (Scrutari.exists($mainSearchForm)) {
            $mainSearchForm.find("input[name='q']").val(initialQuery);
            $mainSearchForm.submit();
        }
    } else if ((initialQId) && (initialQId.length > 0)) {
        var requestParameters = Engine.Ui.checkRequestParameters(engine);
        requestParameters["qid"] = initialQId;
        var _mainsearchScrutariResultCallback = function (scrutariResult) {
            Engine.Query.scrutariResult(engine, scrutariResult, "mainsearch");
            $("#loadingModal").modal('hide');
            $parametersDisplayButton = $('#parametersDisplayButton');
            if ($parametersDisplayButton.data("scrutariState") === "on") {
                $parametersDisplayButton.click();
            }
        };
        var _mainScrutariErrorCallback = function (error) {
            Engine.Query.scrutariError(engine, error, "mainsearch");
            $("#loadingModal").modal('hide');
        };
        $("#loadingModal").modal('show');
        Scrutari.Result.newSearch(_mainsearchScrutariResultCallback, engine.getScrutariConfig(), requestParameters, _mainScrutariErrorCallback);
    }
};
Engine.Query = {};
Engine.Query.scrutariResult = function (engine, scrutariResult, searchOrigin) {
    var bootstrapV2 = (engine.getBootstrapVersion().lastIndexOf("2", 0) === 0);
    var _getPaginationHtml = function (bottomPagination, categoryName) {
        var paginationHtml = "";
        if (bootstrapV2) {
            paginationHtml += "<div class='pagination'><ul class='scrutari-result-Pagination";
            if (category) {
                paginationHtml += " scrutari-Pagination_" + categoryName;
            }
            paginationHtml +=  "'></ul></div>";
        } else {
            paginationHtml += "<ul class='pagination scrutari-result-Pagination";
            if (category) {
                paginationHtml += " scrutari-Pagination_" + categoryName;
            }
            if (bottomPagination) {
                paginationHtml += " scrutari-BottomPagination";
            }
            paginationHtml +=  "'></ul>";
        }
        return paginationHtml;
    };
    Engine.Ui.show(".scrutari-HiddenAtStart");
    var ficheCount = scrutariResult.getFicheCount();
    Engine.Ui.setCount("#resultFicheCount", ficheCount, engine.getScrutariConfig().getLangUi());
    var $ficheDisplayBlock = $("#ficheDisplayBlock");
    $ficheDisplayBlock.empty();
    if (searchOrigin === "mainsearch") {
        engine.setCurrentScrutariResult(scrutariResult);
        Engine.Query.addToHistory(engine, scrutariResult);
    } else if (searchOrigin === "subsearch") {
        var subsearchText =  "+ " + scrutariResult.getFormatedSearchSequence(engine.getScrutariLoc()) + " = " + scrutariResult.getFicheCount();
        $(".scrutari-history-Active").find(".scrutari-CurrentSubsearchSequence").text(subsearchText);
    }
    if (ficheCount === 0) {
        Engine.Ui.hide(".scrutari-HiddenIfEmpty");
        $ficheDisplayBlock.html("<em>" + engine.loc("_ result_none")+ "</em>");
        return;
    }
    Engine.Ui.show(".scrutari-HiddenIfEmpty");
    var qId = scrutariResult.getQId();
    var permalink = engine.toPermalink(qId);
    if (permalink) {
        $("#permalinkLink").attr("href", permalink);
    }
    $("#odsLink").attr("href", engine.getScrutariConfig().getOdsUrl(qId));
    $("#atomLink").attr("href", engine.getScrutariConfig().getAtomUrl(qId));
    if (scrutariResult.getFicheGroupType() === 'category') {
        var categoryCount = scrutariResult.getCategoryCount();
        var tabHtml = '<ul class="nav nav-tabs" role="tablist">';
        var contentHtml = "<div class='tab-content'>";
        for(var i = 0; i < categoryCount; i++) {
            var category = scrutariResult.getCategory(i);
            tabHtml += "<li";
            contentHtml += "<div class='tab-pane";
            if (i === 0) {
                tabHtml += " class='active'";
                contentHtml += " active";
            }
            tabHtml += "><a class='scrutari-Onglet' role='tab' data-toggle='tab' href='#categoryTabContent_"
                + category.name
                + "'>"
                + Scrutari.escape(category.title)
                + " ("
                + scrutariResult.getCategoryFicheCount(i)
                + ")"
                + "</a></li>";
            contentHtml += "' id='categoryTabContent_" + category.name + "'>";
            contentHtml += _getPaginationHtml(false, category.name);
            contentHtml += "<div id='categoryFiches_" + category.name + "'></div>";
            contentHtml += _getPaginationHtml(true, category.name);
            contentHtml += "</div>";
        }
        tabHtml += "</ul>";
        contentHtml += "</div>";
        $ficheDisplayBlock.html(tabHtml + contentHtml);
        for(var i = 0; i < categoryCount; i++) {
            var category = scrutariResult.getCategory(i);
            Engine.Query.categoryChangePlage(engine, category.name, scrutariResult, engine.getScrutariConfig().getPlageLength(), 1);
        }
    } else {
        $ficheDisplayBlock.html(_getPaginationHtml(false, null) + "<div id='noneFiches'></div>" + _getPaginationHtml(true, null));
        Engine.Query.noneChangePlage(engine, scrutariResult, engine.getScrutariConfig().getPlageLength(), 1);
    }
};
Engine.Query.scrutariError = function (engine, error, searchOrigin) {
    if (error.parameter !== "q") {
        Scrutari.logError(error);
        return;
    }
    var title =  engine.loc(error.key);
    if (title !== error.key) {
        var alertMessage = title;
        if (error.hasOwnProperty("array"))  {
            alertMessage += engine.loc('_ scrutari-colon');
            for(var i = 0; i < error.array.length; i++) {
                alertMessage += "\n";
                var obj = error.array[i];
                alertMessage += "- ";
                alertMessage += engine.loc(obj.key);
                if (obj.hasOwnProperty("value")) {
                    alertMessage += engine.loc('_ scrutari-colon');
                    alertMessage += " ";
                    alertMessage += obj.value;
                }
            }
        }
        alert(alertMessage);
    } else {
        Scrutari.logError(error);
    }
};
Engine.Query.noneChangePlage = function (engine, scrutariResult, plageLength, plageNumber) {
    var html = "";
    if (!scrutariResult.isNonePlageLoaded(plageLength, plageNumber)) {
        if (engine.isWaiting()) {
            return;
        }
        $("#noneFiches").html("<span class='scrutari-icon-Loader'></span> " + engine.loc('_ loading_plage'));
        var _plageCallback = function () {
            engine.setWaiting(false);
            Engine.Query.noneChangePlage(engine, scrutariResult, plageLength, plageNumber);
        };
        engine.setWaiting(true);
        scrutariResult.loadNonePlage(_plageCallback, engine.getScrutariConfig(), plageLength, plageNumber);
        return;
    }
    var plageFicheArray = scrutariResult.selectNoneFicheArray(plageLength, plageNumber);
    var ficheTemplate =  engine.getTemplate("fiche", scrutariResult);
    for(var i = 0; i < plageFicheArray.length; i++) {
        html += ficheTemplate(plageFicheArray[i]);
    }
    $("#noneFiches").html(html);
    var paginationIdPrefix = "pagination_";
    Scrutari.Utils.checkPagination(scrutariResult.getFicheCount(), plageLength, plageNumber, ".scrutari-Pagination", engine.getTemplate("pagination", paginationIdPrefix));
    $(".scrutari-Pagination a").click(function () {
        var idx = this.href.indexOf("#" + paginationIdPrefix);
        var newPlageNumber = parseInt(this.href.substring(idx + 1 + paginationIdPrefix.length));
        Engine.Query.noneChangePlage(engine, scrutariResult, plageLength, newPlageNumber);
        return false;
    });
    $(".scrutari-Pagination.scrutari-BottomPagination a").click(function () {
        Engine.Ui.scrollToResult();
    });
    engine.changePlageCallback(plageFicheArray, {engine: engine, scrutariResult: scrutariResult,  plageLength:plageLength, plageNumber:plageNumber});
};
Engine.Query.categoryChangePlage = function (engine, categoryName, scrutariResult, plageLength, plageNumber) {
    var html = "";
    if (!scrutariResult.isCategoryPlageLoaded(categoryName, plageLength, plageNumber)) {
        $("#categoryFiches_" + categoryName).html("<span class='scrutari-icon-Loader'></span> " + engine.loc('_ loading_plage'));
        var _plageCallback = function () {
            engine.setWaiting(false);
            Engine.Query.categoryChangePlage(engine, categoryName, scrutariResult, plageLength, plageNumber);
        };
        if (engine.isWaiting()) {
            return;
        }
        engine.setWaiting(true);
        scrutariResult.loadCategoryPlage(_plageCallback, engine.getScrutariConfig(), categoryName, plageLength, plageNumber);
        return;
    }
    var plageFicheArray = scrutariResult.selectCategoryFicheArray(categoryName, plageLength, plageNumber);
    var ficheTemplate =  engine.getTemplate("fiche", scrutariResult);
    for(var i = 0; i < plageFicheArray.length; i++) {
        html += ficheTemplate(plageFicheArray[i]);
    }
    $("#categoryFiches_" + categoryName).html(html);
    var paginationIdPrefix = "pagination_" + categoryName + "_";
    Scrutari.Utils.checkPagination(scrutariResult.getCategoryFicheCountbyName(categoryName), plageLength, plageNumber, ".scrutari-Pagination_" + categoryName, engine.getTemplate("pagination", paginationIdPrefix));
    $(".scrutari-Pagination_" + categoryName + " a").click(function () {
        var idx = this.href.indexOf("#" + paginationIdPrefix);
        var newPlageNumber = parseInt(this.href.substring(idx + 1 + paginationIdPrefix.length));
        Engine.Query.categoryChangePlage(engine, categoryName, scrutariResult, plageLength, newPlageNumber);
        return false;
    });
    $(".scrutari-Pagination_" + categoryName + ".scrutari-BottomPagination a").click(function () {
        Engine.Ui.scrollToResult();
    });
    engine.changePlageCallback(plageFicheArray, {engine: engine, categoryName: categoryName, scrutariResult: scrutariResult,  plageLength:plageLength, plageNumber:plageNumber});
};
Engine.Query.addToHistory = function (engine, scrutariResult) {
    var $resultHistoryList = $("#resultHistoryList");
    if (!Scrutari.exists($resultHistoryList)) {
        Engine.Query.updateSubsearch(engine, scrutariResult);
        return;
    } else {
        Engine.Query.activateResult(engine, scrutariResult);
    }
    var historyId = "searchHistory_" + engine.newHistoryNumber();
    var withFiche = (scrutariResult.getFicheCount() > 0);
    var html = "";
    html += "<button type='button' class='close hidden' title='" + engine.loc('_ scrutari-remove') + "'><span aria-hidden='true'>&times;</span><span class='sr-only'>" + engine.loc('_ scrutari-remove') + "</span></button>";
    if (withFiche) {
        html += "<a href='#" + historyId + "'>";
    }
    html += scrutariResult.getFormatedSearchSequence(engine.getScrutariLoc());
    html += " (" + scrutariResult.getFicheCount() + ")";
    if (withFiche) {
        html += "</a>";
    }
    html += "<br/><span class='scrutari-CurrentSubsearchSequence'></span>";
    var $new = $("<div/>").html(html).attr("id", historyId).addClass("scrutari-history-Block").addClass("scrutari-history-Active");
    var _close = function() {
        $("#" + historyId).remove();
    }
    var _load = function () {
        var $historyId = $("#" + historyId);
        var historyScrutariResult = $historyId.data("scrutariResult");
        engine.setCurrentScrutariResult(historyScrutariResult);
        Engine.Query.activateResult(engine, historyScrutariResult);
        $historyId.addClass("scrutari-history-Active");
        Engine.Ui.hide($historyId.find("button"));
        Engine.Query.scrutariResult(engine, historyScrutariResult);
        return false;
    }
    $new.children("button").click(_close);
    $new.children("a").click(_load);
    $new.data("scrutariResult", scrutariResult);
    $resultHistoryList.prepend($new);
};
Engine.Query.activateResult = function (engine, scrutariResult) {
    var $elements = $(".scrutari-history-Active");
    $elements.removeClass("scrutari-history-Active");
    $elements.find(".scrutari-CurrentSubsearchSequence").empty();
    Engine.Ui.show($elements.find("button"));
    Engine.Query.updateSubsearch(engine, scrutariResult);
};
Engine.Query.updateSubsearch = function (engine, scrutariResult) {
    $(".scrutari-CurrentSearchSequence").text(scrutariResult.getFormatedSearchSequence(engine.getScrutariLoc()) + " (" + scrutariResult.getFicheCount() + ")");
};
Engine.Ui = {};
Engine.Ui.show = function (jqArgument) {
    var $elements = Scrutari.convert(jqArgument);
    return $elements.removeClass("hidden");
};
Engine.Ui.hide = function (jqArgument) {
    var $elements = Scrutari.convert(jqArgument);
    return $elements.addClass("hidden");
};
Engine.Ui.scrollToResult = function () {
    $(window).scrollTop($("#resultArea").offset().top);
};
Engine.Ui.setCount = function (parentJqArgument, count, locale) {
    var $parent = Scrutari.convert(parentJqArgument);
    var $elements = $parent.find(".scrutari-stats-Count");
    var html = '';
    if (Number.prototype.toLocaleString) {
        html += count.toLocaleString(locale);
    } else {
        html += count;
    }
    $elements.html(html);
    return $elements;
};
Engine.Ui.filterChange = function (engine) {
    var scrutariFilter = Engine.Ui.checkFilter(engine);
    var scrutariMeta = engine.getScrutariMeta();
    var filterFicheCount = scrutariFilter.getFilterFicheCount(scrutariMeta);
    var globalFicheCount = scrutariMeta.getGlobalFicheCount();
    var $element = Engine.Ui.setCount("#filterFicheCount", filterFicheCount, engine.getScrutariConfig().getLangUi());
    if (filterFicheCount === globalFicheCount) {
        $element.removeClass("scrutari-stats-Filter").removeClass("scrutari-stats-None");
        Engine.Ui.hide("#filterFicheCount");
    } else if (filterFicheCount === 0) {
        $element.removeClass("scrutari-stats-Filter").addClass("scrutari-stats-None");
        Engine.Ui.show("#filterFicheCount");
    } else {
        $element.addClass("scrutari-stats-Filter").removeClass("scrutari-stats-None");
        Engine.Ui.show("#filterFicheCount");
    }
};
Engine.Ui.checkFilter = function (engine) {
    var _testEnable = function (selector) {
        return ($(selector).data("scrutariState") === "on");
    }
    var scrutariFilter = new Scrutari.Filter();
    if (engine.isWithCorpus()) {
        scrutariFilter.setWithCorpus(_testEnable("#corpusEnableButton"));
    } else {
        scrutariFilter.setWithBase(_testEnable("#baseEnableButton"));
    }
    scrutariFilter.setWithLang(_testEnable("#langEnableButton"));
    scrutariFilter.setWithCategory(_testEnable("#categoryEnableButton"));
    var $baseElements = $("input[name='base']:checked");
    for(var i = 0; i < $baseElements.length; i++) {
        scrutariFilter.addBase(parseInt($baseElements[i].value));
    }
    var $langElements = $("input[name='lang']:checked");
    for(var i = 0; i < $langElements.length; i++) {
        scrutariFilter.addLang($langElements[i].value);
    }
    var $categoryElements = $("input[name='category']:checked");
    for(var i = 0; i < $categoryElements.length; i++) {
        scrutariFilter.addCategory($categoryElements[i].value);
    }
    var $corpusElements = $("input[name='corpus']:checked");
    for(var i = 0; i < $corpusElements.length; i++) {
        scrutariFilter.addCorpus(parseInt($corpusElements[i].value));
    }
    return scrutariFilter;
};
Engine.Ui.checkRequestParameters = function (engine) {
    var scrutariFilter = Engine.Ui.checkFilter(engine);
    var requestParameters = new Object();
    scrutariFilter.checkRequestParameters(engine.getScrutariMeta(), requestParameters);
    requestParameters["q-mode"] = $("input[name='q-mode']:checked").val();
    var ponderation = $("input[name='ponderation']:checked").val();
    if (ponderation === 'date') {
        requestParameters.ponderation = '15,80,5';
    }
    var periode = $.trim($("input[name='periode']").val());
    if (periode) {
        requestParameters["flt-date"] = periode;
    }
    if (Scrutari.exists("input[name='wildchar']:checked")) {
        requestParameters.wildchar = "end";
    } else {
        requestParameters.wildchar = "none";
    }
    return requestParameters;
};
Engine.FrameMode = {};
Engine.FrameMode.is = function (engine) {
    return (engine.getMode() === "frame");
};
Engine.FrameMode.setCurrentFicheBlock = function (code, scroll) {
    $(".scrutari-fiche-CurrentBlock").removeClass("scrutari-fiche-CurrentBlock");
    var $ficheBlock = $("#ficheBlock_" + code);
    $ficheBlock.addClass("scrutari-fiche-CurrentBlock");
    if (scroll) {
        var top = $ficheBlock.offset().top;
        var $window =  $(window);
        var windowTop = $window.scrollTop();
        var windowBottom = windowTop + $window.height();
        if ((top < windowTop) || (top > (windowBottom - 20))) {
            if ($window.height() > 300) {
                top = top - 150;
            }
            $window.scrollTop(top);
        }
    }
};
Engine.FrameMode.clear = function () {
    window.parent.frames["Navigation"].clear();
};
Engine.FrameMode.changePlageCallback = function (plageFicheArray, changePlageArguments) {
    var _linkClick = function () {
        var idx = this.id.indexOf('_');
        var code = this.id.substring(idx + 1);
        Engine.FrameMode.setCurrentFicheBlock(code, false);
        window.parent.frames["Navigation"].setCurrentFiche(code, plageFicheArray);
    };
    Engine.FrameMode.clear();
    if (changePlageArguments.categoryName) {
        $("#categoryFiches_" + changePlageArguments.categoryName + " a.scrutari-fiche-Link").click(_linkClick);
    } else {
        $("a.scrutari-fiche-Link").click(_linkClick);
    }
};
Engine.html = {
list:["loadingModal","titleArea","mainsearchForm_start","modeHelpModal","ponderationHelpModal","periodeHelpModal","parametersArea_start","optionsPanel","corpusPanel","basePanel","langPanel","categoryPanel","parametersArea_end","mainsearchForm_end","statsArea","resultArea"],
loadingModal:"<div class=\"modal fade\" id=\"loadingModal\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\"> <div class=\"modal-dialog\"> <div class=\"modal-content\"> <div class=\"modal-header\"> <p class=\"modal-title scrutari-modal-Title\"><span class=\"scrutari-icon-Loader\"></span> _ loading_search</p> </div> </div> </div> </div>",
titleArea:"<!-- title --> <div id=\"titleArea\" class=\"scrutari-area-Title\"> <div class=\"row\"> <div class=\"col-xs-12\"> <p class=\"scrutari-title-Main\" id=\"mainTitle\"></p> </div> </div> </div> <!-- /title -->",
mainsearchForm_start:"<!-- mainform --> <form id=\"mainsearchForm\"> <div class=\"row\"> <div class=\"col-md-8 col-lg-7\"> <input type=\"text\" class=\"scrutari-input-Text\" size=\"60\" name=\"q\" value=\"\"> <input type=\"submit\" value=\"_ button_submit\"> </div> <div class=\"col-md-4 col-lg-offset-1\"> <button type=\"button\" id=\"parametersDisplayButton\" class=\"scrutari-button-Parameters hidden btn btn-primary\" data-toggle=\"button\" data-scrutari-state=\"off\"> <span class=\"glyphicon glyphicon-cog\"></span> <span>_ button_parameters</span> </button> </div> </div>",
modeHelpModal:"<div class=\"modal fade\" id=\"modeHelpModal\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\"> <div class=\"modal-dialog\"> <div class=\"modal-content\"> <div class=\"modal-header\"> <p class=\"modal-title scrutari-modal-Title\">_ mode_help</p> <div class=\"modal-body\"></div> <div class=\"modal-footer\"> <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">_ button_close</button> </div> </div> </div> </div> </div>",
ponderationHelpModal:"<div class=\"modal fade\" id=\"ponderationHelpModal\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\"> <div class=\"modal-dialog\"> <div class=\"modal-content\"> <div class=\"modal-header\"> <p class=\"modal-title scrutari-modal-Title\">_ ponderation_help</p> <div class=\"modal-body\"></div> <div class=\"modal-footer\"> <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">_ button_close</button> </div> </div> </div> </div> </div>",
periodeHelpModal:"<div class=\"modal fade\" id=\"periodeHelpModal\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\"> <div class=\"modal-dialog\"> <div class=\"modal-content\"> <div class=\"modal-header\"> <p class=\"modal-title scrutari-modal-Title\">_ periode_help</p> <div class=\"modal-body\"></div> <div class=\"modal-footer\"> <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">_ button_close</button> </div> </div> </div> </div> </div>",
parametersArea_start:"<div id=\"parametersArea\" class=\"scrutari-area-Parameters hidden\">",
optionsPanel:"<div class=\"panel panel-default scrutari-panel-Div\" id=\"optionsPanel\"> <div class=\"panel-heading scrutari-panel-Heading\"> <p class=\"panel-title scrutari-panel-Title\" style=\"cursor: pointer;\" data-toggle=\"collapse\" data-target=\"#optionsPanelBody\"> <span class=\"glyphicon glyphicon-wrench\"></span> _ title_options </p> </div> <div class=\"panel-body collapse\" id=\"optionsPanelBody\"> <div class=\"row\"> <div class=\"col-sm-3\"> <p>_ mode_title<br/><button id=\"modeHelpButton\" type=\"button\" class=\"btn btn-primary btn-xs\" title=\"_ mode_help\"><span class=\"glyphicon glyphicon-question-sign\"></span></button></p> <p><label class=\"scrutari-Label\"><input type=\"radio\" name=\"q-mode\" value=\"intersection\" checked> _ mode_intersection</label></p> <p><label class=\"scrutari-Label\"><input type=\"radio\" name=\"q-mode\" value=\"union\"> _ mode_union</label></p> <p><label class=\"scrutari-Label\"><input type=\"radio\" name=\"q-mode\" value=\"operation\"> _ mode_operation</label></p> <p><label class=\"scrutari-Label\"><input type=\"checkbox\" name=\"wildchar\" value=\"end\" checked> _ wildchar_end</label></p> </div> <div class=\"col-sm-3\"> <p>_ ponderation_title<br/><button id=\"ponderationHelpButton\" type=\"button\" class=\"btn btn-primary btn-xs\" title=\"_ ponderation_help\"><span class=\"glyphicon glyphicon-question-sign\"></span></button></p> <p><label class=\"scrutari-Label\"><input type=\"radio\" name=\"ponderation\" value=\"pertinence\" checked> _ ponderation_pertinence</label></p> <p><label class=\"scrutari-Label\"><input type=\"radio\" name=\"ponderation\" value=\"date\"> _ ponderation_date</label></p> </div> <div class=\"col-sm-3\"> <p>_ periode_title<br/><button id=\"periodeHelpButton\" type=\"button\" class=\"btn btn-primary btn-xs\" title=\"_ periode_help\"><span class=\"glyphicon glyphicon-question-sign\"></span></button></p> <p><input type=\"text\" class=\"scrutari-input-Text\" name=\"periode\" value=\"\" size=\"20\"></p> </div> </div> </div> </div>",
corpusPanel:"<!-- corpus-panel --> <div class=\"panel panel-default scrutari-panel-Div hidden\" id=\"corpusPanel\"> <div class=\"panel-heading scrutari-panel-Heading\"> <p class=\"panel-title scrutari-panel-Title\" style=\"cursor: pointer;\" data-toggle=\"collapse\" data-target=\"#corpusPanelBody\"> <span class=\"glyphicon glyphicon-filter\"></span> _ title_filter_corpus <span id=\"corpusFilterLabel\" class=\"small scrutari-Disabled\" data-scrutari-alternate=\"_ filter_on\">_ filter_off</span> </p> </div> <div class=\"panel-body scrutari-panel-WithTools collapse\" id=\"corpusPanelBody\"> <div class=\"row\"> <div class=\"col-sm-12\"> <div class=\"btn-toolbar\"> <div class=\"btn-group btn-group-sm\"> <button type=\"button\" id=\"corpusEnableButton\" class=\"btn btn-primary\" data-toggle=\"button\" data-scrutari-state=\"off\"> <span data-scrutari-alternate=\"_ button_filter_off\">_ button_filter_on</span> </button> </div> <div class=\"btn-group btn-group-sm\"> <button type=\"button\" id=\"corpusCheckAllButton\" class=\"btn btn-primary\" disabled>_ button_check_all</button> <button type=\"button\" id=\"corpusUncheckAllButton\" class=\"btn btn-primary\" disabled>_ button_uncheck_all</button> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-sm-3 scrutari-column-Corpus scrutari-Disabled\"></div> <div class=\"col-sm-3 scrutari-column-Corpus scrutari-Disabled\"></div> <div class=\"col-sm-3 scrutari-column-Corpus scrutari-Disabled\"></div> <div class=\"col-sm-3 scrutari-column-Corpus scrutari-Disabled\"></div> </div> </div> </div> <!-- /corpus-panel -->",
basePanel:"<!-- base-panel --> <div class=\"panel panel-default scrutari-panel-Div hidden\" id=\"basePanel\"> <div class=\"panel-heading scrutari-panel-Heading\"> <p class=\"panel-title scrutari-panel-Title\" style=\"cursor: pointer;\" data-toggle=\"collapse\" data-target=\"#basePanelBody\"> <span class=\"glyphicon glyphicon-filter\"></span> _ title_filter_base <span id=\"baseFilterLabel\" class=\"small scrutari-Disabled\" data-scrutari-alternate=\"_ filter_on\">_ filter_off</span> </p> </div> <div class=\"panel-body scrutari-panel-WithTools collapse\" id=\"basePanelBody\"> <div class=\"row\"> <div class=\"col-sm-12\"> <div class=\"btn-toolbar\"> <div class=\"btn-group btn-group-sm\"> <button type=\"button\" id=\"baseEnableButton\" class=\"btn btn-primary\" data-toggle=\"button\" data-scrutari-state=\"off\"> <span data-scrutari-alternate=\"_ button_filter_off\">_ button_filter_on</span> </button> </div> <div class=\"btn-group btn-group-sm\"> <button type=\"button\" id=\"baseCheckAllButton\" class=\"btn btn-primary\" disabled>_ button_check_all</button> <button type=\"button\" id=\"baseUncheckAllButton\" class=\"btn btn-primary\" disabled>_ button_uncheck_all</button> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-sm-3 scrutari-column-Base scrutari-Disabled\"></div> <div class=\"col-sm-3 scrutari-column-Base scrutari-Disabled\"></div> <div class=\"col-sm-3 scrutari-column-Base scrutari-Disabled\"></div> <div class=\"col-sm-3 scrutari-column-Base scrutari-Disabled\"></div> </div> </div> </div> <!-- /base-panel -->",
langPanel:"<!-- lang-panel --> <div class=\"panel panel-default scrutari-panel-Div hidden\" id=\"langPanel\"> <div class=\"panel-heading scrutari-panel-Heading\"> <p class=\"panel-title scrutari-panel-Title\" style=\"cursor: pointer;\" data-toggle=\"collapse\" data-target=\"#langPanelBody\"> <span class=\"glyphicon glyphicon-filter\"></span> _ title_filter_lang <span id=\"langFilterLabel\" class=\"small scrutari-Disabled\" data-scrutari-alternate=\"_ filter_on\">_ filter_off</span> </p> </div> <div class=\"panel-body scrutari-panel-WithTools collapse\" id=\"langPanelBody\"> <div class=\"row\"> <div class=\"col-sm-12\"> <div class=\"btn-toolbar\"> <div class=\"btn-group btn-group-sm\"> <button type=\"button\" id=\"langEnableButton\" class=\"btn btn-primary\" data-toggle=\"button\" data-scrutari-state=\"off\"> <span data-scrutari-alternate=\"_ button_filter_off\">_ button_filter_on</span> </button> </div> <div class=\"btn-group btn-group-sm\"> <button type=\"button\" id=\"langCheckAllButton\" class=\"btn btn-primary\" disabled>_ button_check_all</button> <button type=\"button\" id=\"langUncheckAllButton\" class=\"btn btn-primary\" disabled>_ button_uncheck_all</button> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-sm-3 scrutari-column-Lang scrutari-Disabled\"></div> <div class=\"col-sm-3 scrutari-column-Lang scrutari-Disabled\"></div> <div class=\"col-sm-3 scrutari-column-Lang scrutari-Disabled\"></div> <div class=\"col-sm-3 scrutari-column-Lang scrutari-Disabled\"></div> </div> </div> </div> <!-- /lang-panel -->",
categoryPanel:"<!-- category-panel --> <div class=\"panel panel-default scrutari-panel-Div hidden\" id=\"categoryPanel\"> <div class=\"panel-heading scrutari-panel-Heading\"> <p class=\"panel-title scrutari-panel-Title\" style=\"cursor: pointer;\" data-toggle=\"collapse\" data-target=\"#categoryPanelBody\"> <span class=\"glyphicon glyphicon-filter\"></span> _ title_filter_category <span id=\"categoryFilterLabel\" class=\"small scrutari-Disabled\" data-scrutari-alternate=\"_ filter_on\">_ filter_off</span> </p> </div> <div class=\"panel-body scrutari-panel-WithTools collapse\" id=\"categoryPanelBody\"> <div class=\"row\"> <div class=\"col-sm-12\"> <div class=\"btn-toolbar\"> <div class=\"btn-group btn-group-sm\"> <button type=\"button\" id=\"categoryEnableButton\" class=\"btn btn-primary\" data-toggle=\"button\" data-scrutari-state=\"off\"> <span data-scrutari-alternate=\"_ button_filter_off\">_ button_filter_on</span> </button> </div> <div class=\"btn-group btn-group-sm\"> <button type=\"button\" id=\"categoryCheckAllButton\" class=\"btn btn-primary\" disabled>_ button_check_all</button> <button type=\"button\" id=\"categoryUncheckAllButton\" class=\"btn btn-primary\" disabled>_ button_uncheck_all</button> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-sm-3 scrutari-column-Category scrutari-Disabled\"></div> <div class=\"col-sm-3 scrutari-column-Category scrutari-Disabled\"></div> <div class=\"col-sm-3 scrutari-column-Category scrutari-Disabled\"></div> <div class=\"col-sm-3 scrutari-column-Category scrutari-Disabled\"></div> </div> </div> </div> <!-- /category-panel -->",
parametersArea_end:"</div>",
mainsearchForm_end:"</form> <!-- /mainform -->",
statsArea:"<!-- stats --> <div id=\"statsArea\" class=\"scrutari-area-Stats\"> <div class=\"row\"> <div class=\"col-xs-12\"> <span class=\"hidden\" id=\"globalFicheCount\"> <abbr class=\"scrutari-stats-Abbr\" title=\"_ fichecount_global_title\">_ fichecount_global_short</abbr> &nbsp; <span class=\"scrutari-stats-Count\"></span> </span> <span class=\"hidden\" id=\"filterFicheCount\"> &nbsp; | &nbsp; <abbr class=\"scrutari-stats-Abbr\" title=\"_ fichecount_filter_title\">_ fichecount_filter_short</abbr> &nbsp; <span class=\"scrutari-stats-Count\"></span> </span> <span class=\"scrutari-HiddenAtStart hidden\" id=\"resultFicheCount\"> &nbsp; | &nbsp; <abbr class=\"scrutari-stats-Abbr\" title=\"_ fichecount_result_title\">_ fichecount_result_short</abbr> &nbsp; <span class=\"scrutari-stats-Count\"></span> </span> </div> </div> </div> <!-- /stats -->",
resultArea:"<div id=\"resultArea\" class=\"scrutari-area-Result\"> <div class=\"row\"> <div class=\"col-md-8 col-lg-7\" id=\"ficheDisplayBlock\"> </div> <div class=\"col-md-4 col-lg-offset-1\"> <div class=\"scrutari-HiddenIfEmpty hidden\"> <!-- share --> <p class=\"scrutari-links-Title\">_ share</p> <p><a href=\"#\" id=\"permalinkLink\" target=\"_blank\">_ link_permalink</a></p> <!-- /share --> <!-- export --> <p class=\"scrutari-links-Title\">_ links</p> <p class=\"scrutari-export-Ods\"><a href=\"#\" id=\"odsLink\" target=\"_blank\">_ link_ods</a></p> <p class=\"scrutari-export-Atom\"><a href=\"#\" id=\"atomLink\" target=\"_blank\">_ link_atom</a></p> <!-- /export --> <!-- subsearch --> <form id=\"subsearchForm\"> <p class=\"scrutari-links-Title\">_ title_subquery_1<span class=\"scrutari-CurrentSearchSequence\"></span>_ title_subquery_2</p> <input type=\"text\" class=\"scrutari-input-Text\" size=\"20\" name=\"q\"> <input type=\"submit\" value=\"_ button_submit\"> </form> <!-- /subsearch --> </div> <!-- history --> <div class=\"scrutari-HiddenAtStart hidden\"> <p class=\"scrutari-links-Title\">_ title_history</p> <div id=\"resultHistoryList\"> </div> </div> <!-- /history --> </div> </div> </div>"
};
