/* global Scrutari,Engine */

Engine.Query = {};

Engine.Query.scrutariResult = function (engine, scrutariResult, searchOrigin) {
    var bootstrapV2 = (engine.getBootstrapVersion().lastIndexOf("2", 0) === 0);
    var _getPaginationHtml = function (bottomPagination, categoryName) {
        var paginationHtml = "";
        if (bootstrapV2) {
            paginationHtml += "<div class='pagination'><ul class='scrutari-result-Pagination";
            if (category) {
                paginationHtml += " scrutari-Pagination_" + categoryName;
            }
            paginationHtml +=  "'></ul></div>";
        } else {
            paginationHtml += "<ul class='pagination scrutari-result-Pagination";
            if (category) {
                paginationHtml += " scrutari-Pagination_" + categoryName;
            }
            if (bottomPagination) {
                paginationHtml += " scrutari-BottomPagination";
            }
            paginationHtml +=  "'></ul>";
        }
        return paginationHtml;
    };
    Engine.Ui.show(".scrutari-HiddenAtStart");
    var ficheCount = scrutariResult.getFicheCount();
    Engine.Ui.setCount("#resultFicheCount", ficheCount, engine.getScrutariConfig().getLangUi());
    var $ficheDisplayBlock = $("#ficheDisplayBlock");
    $ficheDisplayBlock.empty();
    if (searchOrigin === "mainsearch") {
        engine.setCurrentScrutariResult(scrutariResult);
        Engine.Query.addToHistory(engine, scrutariResult);
    } else if (searchOrigin === "subsearch") {
        var subsearchText =  "+ " + scrutariResult.getFormatedSearchSequence(engine.getScrutariLoc()) + " = " + scrutariResult.getFicheCount();
        $(".scrutari-history-Active").find(".scrutari-CurrentSubsearchSequence").text(subsearchText);
    }
    if (ficheCount === 0) {
        Engine.Ui.hide(".scrutari-HiddenIfEmpty");
        $ficheDisplayBlock.html("<em>" + engine.loc("_ result_none")+ "</em>");
        return;
    }
    Engine.Ui.show(".scrutari-HiddenIfEmpty");
    var qId = scrutariResult.getQId();
    var permalink = engine.toPermalink(qId);
    if (permalink) {
        $("#permalinkLink").attr("href", permalink);
    }
    $("#odsLink").attr("href", engine.getScrutariConfig().getOdsUrl(qId));
    $("#atomLink").attr("href", engine.getScrutariConfig().getAtomUrl(qId));
    if (scrutariResult.getFicheGroupType() === 'category') {
        var categoryCount = scrutariResult.getCategoryCount();
        var tabHtml = '<ul class="nav nav-tabs" role="tablist">';
        var contentHtml = "<div class='tab-content'>";
        for(var i = 0; i < categoryCount; i++) {
            var category = scrutariResult.getCategory(i);
            tabHtml += "<li";
            contentHtml += "<div class='tab-pane";
            if (i === 0) {
                tabHtml += " class='active'";
                contentHtml += " active";
            }
            tabHtml += "><a class='scrutari-Onglet' role='tab' data-toggle='tab' href='#categoryTabContent_"
                + category.name
                + "'>"
                + Scrutari.escape(category.title)
                + " ("
                + scrutariResult.getCategoryFicheCount(i)
                + ")"
                + "</a></li>";
            contentHtml += "' id='categoryTabContent_" + category.name + "'>";
            contentHtml += _getPaginationHtml(false, category.name);
            contentHtml += "<div id='categoryFiches_" + category.name + "'></div>";
            contentHtml += _getPaginationHtml(true, category.name);
            contentHtml += "</div>";
        }
        tabHtml += "</ul>";
        contentHtml += "</div>";
        $ficheDisplayBlock.html(tabHtml + contentHtml);
        for(var i = 0; i < categoryCount; i++) {
            var category = scrutariResult.getCategory(i);
            Engine.Query.categoryChangePlage(engine, category.name, scrutariResult, engine.getScrutariConfig().getPlageLength(), 1);
        }
    } else {
        $ficheDisplayBlock.html(_getPaginationHtml(false, null) + "<div id='noneFiches'></div>" + _getPaginationHtml(true, null));
        Engine.Query.noneChangePlage(engine, scrutariResult, engine.getScrutariConfig().getPlageLength(), 1);
    }
};

Engine.Query.scrutariError = function (engine, error, searchOrigin) {
    if (error.parameter !== "q") {
        Scrutari.logError(error);
        return;
    }
    var title =  engine.loc(error.key);
    if (title !== error.key) {
        var alertMessage = title;
        if (error.hasOwnProperty("array"))  {
            alertMessage += engine.loc('_ scrutari-colon');
            for(var i = 0; i < error.array.length; i++) {
                alertMessage += "\n";
                var obj = error.array[i];
                alertMessage += "- ";
                alertMessage += engine.loc(obj.key);
                if (obj.hasOwnProperty("value")) {
                    alertMessage += engine.loc('_ scrutari-colon');
                    alertMessage += " ";
                    alertMessage += obj.value;
                }
            }
        }
        alert(alertMessage);
    } else {
        Scrutari.logError(error);
    }
};

Engine.Query.noneChangePlage = function (engine, scrutariResult, plageLength, plageNumber) {
    var html = "";
    if (!scrutariResult.isNonePlageLoaded(plageLength, plageNumber)) {
        if (engine.isWaiting()) {
            return;
        }
        $("#noneFiches").html("<span class='scrutari-icon-Loader'></span> " + engine.loc('_ loading_plage'));
        var _plageCallback = function () {
            engine.setWaiting(false);
            Engine.Query.noneChangePlage(engine, scrutariResult, plageLength, plageNumber);
        };
        engine.setWaiting(true);
        scrutariResult.loadNonePlage(_plageCallback, engine.getScrutariConfig(), plageLength, plageNumber);
        return;
    }
    var plageFicheArray = scrutariResult.selectNoneFicheArray(plageLength, plageNumber);
    var ficheTemplate =  engine.getTemplate("fiche", scrutariResult);
    for(var i = 0; i < plageFicheArray.length; i++) {
        html += ficheTemplate(plageFicheArray[i]);
    }
    $("#noneFiches").html(html);
    var paginationIdPrefix = "pagination_";
    Scrutari.Utils.checkPagination(scrutariResult.getFicheCount(), plageLength, plageNumber, ".scrutari-Pagination", engine.getTemplate("pagination", paginationIdPrefix));
    $(".scrutari-Pagination a").click(function () {
        var idx = this.href.indexOf("#" + paginationIdPrefix);
        var newPlageNumber = parseInt(this.href.substring(idx + 1 + paginationIdPrefix.length));
        Engine.Query.noneChangePlage(engine, scrutariResult, plageLength, newPlageNumber);
        return false;
    });
    $(".scrutari-Pagination.scrutari-BottomPagination a").click(function () {
        Engine.Ui.scrollToResult();
    });
    engine.changePlageCallback(plageFicheArray, {engine: engine, scrutariResult: scrutariResult,  plageLength:plageLength, plageNumber:plageNumber});
};
    
Engine.Query.categoryChangePlage = function (engine, categoryName, scrutariResult, plageLength, plageNumber) {
    var html = "";
    if (!scrutariResult.isCategoryPlageLoaded(categoryName, plageLength, plageNumber)) {
        $("#categoryFiches_" + categoryName).html("<span class='scrutari-icon-Loader'></span> " + engine.loc('_ loading_plage'));
        var _plageCallback = function () {
            engine.setWaiting(false);
            Engine.Query.categoryChangePlage(engine, categoryName, scrutariResult, plageLength, plageNumber);
        };
        if (engine.isWaiting()) {
            return;
        }
        engine.setWaiting(true);
        scrutariResult.loadCategoryPlage(_plageCallback, engine.getScrutariConfig(), categoryName, plageLength, plageNumber);
        return;
    }
    var plageFicheArray = scrutariResult.selectCategoryFicheArray(categoryName, plageLength, plageNumber);
    var ficheTemplate =  engine.getTemplate("fiche", scrutariResult);
    for(var i = 0; i < plageFicheArray.length; i++) {
        html += ficheTemplate(plageFicheArray[i]);
    }
    $("#categoryFiches_" + categoryName).html(html);
    var paginationIdPrefix = "pagination_" + categoryName + "_";
    Scrutari.Utils.checkPagination(scrutariResult.getCategoryFicheCountbyName(categoryName), plageLength, plageNumber, ".scrutari-Pagination_" + categoryName, engine.getTemplate("pagination", paginationIdPrefix));
    $(".scrutari-Pagination_" + categoryName + " a").click(function () {
        var idx = this.href.indexOf("#" + paginationIdPrefix);
        var newPlageNumber = parseInt(this.href.substring(idx + 1 + paginationIdPrefix.length));
        Engine.Query.categoryChangePlage(engine, categoryName, scrutariResult, plageLength, newPlageNumber);
        return false;
    });
    $(".scrutari-Pagination_" + categoryName + ".scrutari-BottomPagination a").click(function () {
        Engine.Ui.scrollToResult();
    });
    engine.changePlageCallback(plageFicheArray, {engine: engine, categoryName: categoryName, scrutariResult: scrutariResult,  plageLength:plageLength, plageNumber:plageNumber});
};

Engine.Query.addToHistory = function (engine, scrutariResult) {
    var $resultHistoryList = $("#resultHistoryList");
    if (!Scrutari.exists($resultHistoryList)) {
        Engine.Query.updateSubsearch(engine, scrutariResult);
        return;
    } else {
        Engine.Query.activateResult(engine, scrutariResult);
    }
    var historyId = "searchHistory_" + engine.newHistoryNumber();
    var withFiche = (scrutariResult.getFicheCount() > 0);
    var html = "";
    html += "<button type='button' class='close hidden' title='" + engine.loc('_ scrutari-remove') + "'><span aria-hidden='true'>&times;</span><span class='sr-only'>" + engine.loc('_ scrutari-remove') + "</span></button>";
    if (withFiche) {
        html += "<a href='#" + historyId + "'>";
    }
    html += scrutariResult.getFormatedSearchSequence(engine.getScrutariLoc());
    html += " (" + scrutariResult.getFicheCount() + ")";
    if (withFiche) {
        html += "</a>";
    }
    html += "<br/><span class='scrutari-CurrentSubsearchSequence'></span>";
    var $new = $("<div/>").html(html).attr("id", historyId).addClass("scrutari-history-Block").addClass("scrutari-history-Active");
    var _close = function() {
        $("#" + historyId).remove();
    }
    var _load = function () {
        var $historyId = $("#" + historyId);
        var historyScrutariResult = $historyId.data("scrutariResult");
        engine.setCurrentScrutariResult(historyScrutariResult);
        Engine.Query.activateResult(engine, historyScrutariResult);
        $historyId.addClass("scrutari-history-Active");
        Engine.Ui.hide($historyId.find("button"));
        Engine.Query.scrutariResult(engine, historyScrutariResult);
        return false;
    }
    $new.children("button").click(_close);
    $new.children("a").click(_load);
    $new.data("scrutariResult", scrutariResult);
    $resultHistoryList.prepend($new);
};
    
Engine.Query.activateResult = function (engine, scrutariResult) {
    var $elements = $(".scrutari-history-Active");
    $elements.removeClass("scrutari-history-Active");
    $elements.find(".scrutari-CurrentSubsearchSequence").empty();
    Engine.Ui.show($elements.find("button"));
    Engine.Query.updateSubsearch(engine, scrutariResult);
};

Engine.Query.updateSubsearch = function (engine, scrutariResult) {
    $(".scrutari-CurrentSearchSequence").text(scrutariResult.getFormatedSearchSequence(engine.getScrutariLoc()) + " (" + scrutariResult.getFicheCount() + ")");
};