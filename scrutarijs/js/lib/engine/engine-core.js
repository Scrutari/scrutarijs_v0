/* global Scrutari */

/***************************************************************
 * ScutariJs
 * Copyright (c) 2014-2017 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */

Engine = function (scrutariConfig, locMap) {
    this.scrutariConfig = scrutariConfig;
    this.scrutariLoc = new Scrutari.Loc(locMap);
    this.withCorpus = false;
    this.scrutariMeta = null;
    this.historyNumber = 0;
    this.waiting = false;
    this.baseSort = "fiche-count";
    this.corpusSort = "fiche-count";
    this.mode = "";
    this.target = "_blank";
    this.initialQuery = "";
    this.initialQId = "";
    this.currentScrutariResult = null;
    this.changePlageCallbackFunction = null;
    this.hiddenList = new Array();
    this.bootstrapVersion = "3";
    this.permalinkPattern = null;
    this.templateFactoryArray = new Array();
};

Engine.prototype.getScrutariConfig = function () {
    return this.scrutariConfig;
};

Engine.prototype.loc = function (locKey) {
    return this.scrutariLoc.loc(locKey);
};

Engine.prototype.getScrutariLoc = function () {
    return this.scrutariLoc;
};

Engine.prototype.isWithCorpus = function () {
    return this.withCorpus;
};

Engine.prototype.setWithCorpus = function (withCorpus) {
    this.withCorpus = withCorpus;
};

Engine.prototype.isWithPermalink = function () {
    return (this.permalinkPattern != null);
};

Engine.prototype.setPermalinkPattern = function (permalinkPattern) {
    this.permalinkPattern = permalinkPattern;
};

Engine.prototype.toPermalink = function (qId) {
    if (this.permalinkPattern) {
        return this.scrutariConfig.getPermalinkUrl(qId, this.permalinkPattern);
    }
    return null;
};

Engine.prototype.getPermalinkPattern = function () {
    return permalinkPattern;
};

Engine.prototype.getScrutariMeta = function () {
    return this.scrutariMeta;
};

Engine.prototype.setScrutariMeta = function (scrutariMeta) {
    this.scrutariMeta = scrutariMeta;
};

Engine.prototype.getScrutariLoc = function () {
    return this.scrutariLoc;
};

Engine.prototype.newHistoryNumber = function () {
    this.historyNumber++;
    return this.historyNumber;
};

Engine.prototype.isWaiting = function () {
    return this.waiting;
};

Engine.prototype.setWaiting = function (bool) {
    this.waiting = bool;
};

Engine.prototype.getBaseSort = function () {
    return this.baseSort;
};

Engine.prototype.setBaseSort = function (baseSort) {
    this.baseSort = baseSort;
};

Engine.prototype.getCorpusSort = function () {
    return this.corpusSort;
};

Engine.prototype.setCorpusSort = function (corpusSort) {
    this.corpusSort = corpusSort;
};

Engine.prototype.getMode = function () {
    return this.mode;
};

Engine.prototype.setMode = function (mode) {
    this.mode = mode;
};

Engine.prototype.getTarget = function () {
    return this.target;
};

Engine.prototype.setTarget = function (target) {
    this.target = target;
};

Engine.prototype.getInitialQuery = function () {
    return this.initialQuery;
};

Engine.prototype.setInitialQuery = function (initialQuery) {
    this.initialQuery = initialQuery;
};

Engine.prototype.getInitialQId = function () {
    return this.initialQId;
};

Engine.prototype.setInitialQId = function (initialQId) {
    this.initialQId = initialQId;
};

Engine.prototype.getBootstrapVersion = function () {
    return this.bootstrapVersion;
};

Engine.prototype.setBootstrapVersion = function (bootstrapVersion) {
    this.bootstrapVersion = bootstrapVersion;
};

Engine.prototype.setTarget = function (target) {
    this.target = target;
};

Engine.prototype.hasCurrentScrutariResult = function () {
    return (this.currentScrutariResult != null);
};

Engine.prototype.getCurrentScrutariResult = function () {
    return this.currentScrutariResult;
};

Engine.prototype.setCurrentScrutariResult = function (scrutariResult) {
    this.currentScrutariResult = scrutariResult;
};

Engine.prototype.setChangePlageCallbackFunction = function (changePlageCallbackFunction) {
    this.changePlageCallbackFunction = changePlageCallbackFunction;
};

Engine.prototype.changePlageCallback = function (plageFicheArray, changePlageArguments) {
    if (this.changePlageCallbackFunction) {
        this.changePlageCallbackFunction(plageFicheArray, changePlageArguments);
    }
};

Engine.prototype.setHiddenList = function (hiddenArg) {
    if ($.type(hiddenArg) === "string") {
        this.hiddenList = hiddenArg.split(",");
    } else {
        this.hiddenList = hiddenArg;
    }
    for(var i = 0, length = this.hiddenList.length; i < length; i++) {
        this.hiddenList[i] = $.trim(this.hiddenList[i]);
    }
};

Engine.prototype.getHiddenList = function () {
    return this.hiddenList;
};

Engine.prototype.isHidden = function (elementName) {
    return $.inArray(elementName, this.hiddenList) > -1;
};

/**
* templateFactory est une fonction qui doit prendre trois arguments :
* - engine
* - templateName
* - templateParameter
*/
Engine.prototype.addTemplateFactory = function (templateFactory) {
    this.templateFactoryArray.push(templateFactory);
};

/**
* templateParameter doit être une chaine pour "pagination", scrutariResult pour "fiche"
*/
Engine.prototype.getTemplate = function (templateName, templateParameter) {
    var factoryLength = this.templateFactoryArray.length;
    if (factoryLength > 0) {
        for(var i = (factoryLength -1); i >= 0; i--) {
            var templateFactory = this.templateFactoryArray[i];
            var template = templateFactory(this, templateName, templateParameter);
            if (template) {
                return template;
            }
        }
    }
    switch(templateName) {
        case "base":
            return Engine.baseTemplate;
        case "category":
            return Engine.categoryTemplate;
        case "corpus":
            return Engine.corpusTemplate;
        case "lang":
            var scrutariMeta = this.scrutariMeta;
            return function (lang) {
                var code = lang.lang;
                var html = "";
                html += "<p>";
                html += "<label class='scrutari-Label'><input type='checkbox' name='lang' value='" + code + "'> ";
                var label = scrutariMeta.getLangLabel(code);
                if (label !== code) {
                    html += label;
                    html += " ";
                }
                html += "[" + code + "]";
                html += " (" + lang.fiche +")";
                html += "</label>";
                html += "</p>";
                return html;
            };
        case "pagination":
            return Scrutari.Utils.buildPaginationTemplate(templateParameter);
        case "fiche":
            return Scrutari.Utils.buildFicheTemplate(this.getScrutariMeta(), this.getScrutariLoc(), templateParameter, this.getTarget());
        default:
            return function () {
                return "Unknwon template: " + templateName;
            };
    }
};

/**
 * Objet base tel que définit dans type=base et type=engine
 */ 
Engine.baseTemplate = function (base) {
    var code = base.codebase;
    var html = "";
    html += "<p>";
    html += "<label class='scrutari-Label'><input type='checkbox' name='base' value='" + code + "'> ";
    if (base.baseicon) {
        html += "<img src='" + base.baseicon + "' alt='' class='scrutari-panel-Icon'> ";
    }
    html += Scrutari.escape(base.intitules.short);
    html += " (" + base.stats.fiche +")";
    html += "</label>";
    html += "</p>";
    return html;
};

/**
 * Objet category tel que définit dans type=category et type=engine
 * 
 * @returns {String} Code HTML correspondant
 */ 
Engine.categoryTemplate = function (category) {
    var name = category.name;
    var html = "";
    html += "<p>";
    html += "<label class='scrutari-Label'><input type='checkbox' name='category' value='" + name + "'> ";
    html +=  Scrutari.escape(category.title);
    html += " (" + category.stats.fiche +")";
    html += "</label>";
    html += "</p>";
    return html;
};

/**
 * Objet corpus tel que définit dans type=corpus et type=engine
 * 
 * @returns {String} Code HTML correspondant
 */ 
Engine.corpusTemplate = function (corpus) {
    var code = corpus.codecorpus;
    var html = "";
    html += "<p>";
    html += "<label class='scrutari-Label'><input type='checkbox' name='corpus' value='" + code + "'> ";
    html += Scrutari.escape(corpus.intitules.corpus);
    html += " (" + corpus.stats.fiche +")";
    html += "</label>";
    html += "</p>";
    return html;
};