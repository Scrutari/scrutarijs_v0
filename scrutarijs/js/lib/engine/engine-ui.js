/* global Scrutari,Engine */

Engine.Ui = {};

Engine.Ui.show = function (jqArgument) {
    var $elements = Scrutari.convert(jqArgument);
    return $elements.removeClass("hidden");
};
    
Engine.Ui.hide = function (jqArgument) {
    var $elements = Scrutari.convert(jqArgument);
    return $elements.addClass("hidden");
};

Engine.Ui.scrollToResult = function () {
    $(window).scrollTop($("#resultArea").offset().top);
};

Engine.Ui.setCount = function (parentJqArgument, count, locale) {
    var $parent = Scrutari.convert(parentJqArgument);
    var $elements = $parent.find(".scrutari-stats-Count");
    var html = '';
    if (Number.prototype.toLocaleString) {
        html += count.toLocaleString(locale);
    } else {
        html += count;
    }
    $elements.html(html);
    return $elements;
};

Engine.Ui.filterChange = function (engine) {
    var scrutariFilter = Engine.Ui.checkFilter(engine);
    var scrutariMeta = engine.getScrutariMeta();
    var filterFicheCount = scrutariFilter.getFilterFicheCount(scrutariMeta);
    var globalFicheCount = scrutariMeta.getGlobalFicheCount();
    var $element = Engine.Ui.setCount("#filterFicheCount", filterFicheCount, engine.getScrutariConfig().getLangUi());
    if (filterFicheCount === globalFicheCount) {
        $element.removeClass("scrutari-stats-Filter").removeClass("scrutari-stats-None");
        Engine.Ui.hide("#filterFicheCount");
    } else if (filterFicheCount === 0) {
        $element.removeClass("scrutari-stats-Filter").addClass("scrutari-stats-None");
        Engine.Ui.show("#filterFicheCount");
    } else {
        $element.addClass("scrutari-stats-Filter").removeClass("scrutari-stats-None");
        Engine.Ui.show("#filterFicheCount");
    }
};

Engine.Ui.checkFilter = function (engine) {
    var _testEnable = function (selector) {
        return ($(selector).data("scrutariState") === "on");
    }
    var scrutariFilter = new Scrutari.Filter();
    if (engine.isWithCorpus()) {
        scrutariFilter.setWithCorpus(_testEnable("#corpusEnableButton"));
    } else {
        scrutariFilter.setWithBase(_testEnable("#baseEnableButton"));
    }
    scrutariFilter.setWithLang(_testEnable("#langEnableButton"));
    scrutariFilter.setWithCategory(_testEnable("#categoryEnableButton"));
    var $baseElements = $("input[name='base']:checked");
    for(var i = 0; i < $baseElements.length; i++) {
        scrutariFilter.addBase(parseInt($baseElements[i].value));
    }
    var $langElements = $("input[name='lang']:checked");
    for(var i = 0; i < $langElements.length; i++) {
        scrutariFilter.addLang($langElements[i].value);
    }
    var $categoryElements = $("input[name='category']:checked");
    for(var i = 0; i < $categoryElements.length; i++) {
        scrutariFilter.addCategory($categoryElements[i].value);
    }
    var $corpusElements = $("input[name='corpus']:checked");
    for(var i = 0; i < $corpusElements.length; i++) {
        scrutariFilter.addCorpus(parseInt($corpusElements[i].value));
    }
    return scrutariFilter;
};

Engine.Ui.checkRequestParameters = function (engine) {
    var scrutariFilter = Engine.Ui.checkFilter(engine);
    var requestParameters = new Object();
    scrutariFilter.checkRequestParameters(engine.getScrutariMeta(), requestParameters);
    requestParameters["q-mode"] = $("input[name='q-mode']:checked").val();
    var ponderation = $("input[name='ponderation']:checked").val();
    if (ponderation === 'date') {
        requestParameters.ponderation = '15,80,5';
    }
    var periode = $.trim($("input[name='periode']").val());
    if (periode) {
        requestParameters["flt-date"] = periode;
    }
    if (Scrutari.exists("input[name='wildchar']:checked")) {
        requestParameters.wildchar = "end";
    } else {
        requestParameters.wildchar = "none";
    }
    return requestParameters;
};
