/* global Scrutari,Engine */

Engine.Init = {};

Engine.Init.html = function (engine, jqArgument) {
    if  ((!Engine.html) || (!Engine.html.list) || (!Engine.html.list.length)) {
        Scrutari.log("Engine.html.list is undefined");
        return;
    }
    var _hide = function (key, text) {
        var key_start = "<!-- " + key + " -->";
        var key_end = "<!-- /" + key + " -->";
        var start_index = text.indexOf(key_start);
        var end_index = text.indexOf(key_end);
        if ((start_index > -1) && (end_index > -1) && (start_index < end_index)) {
            text = text.substring(0, start_index) + text.substring(end_index + key_end.length);
        }
        return text;
    }
    var html = "";
    for(var i= 0, len = Engine.html.list.length; i < len; i++) {
        var key = Engine.html.list[i];
        if (Engine.html.hasOwnProperty(key)) {
            html += Engine.html[key];
        }
    }
    var hiddenList = engine.getHiddenList();
    for(var i =0, length = hiddenList.length; i < length ; i++) {
        html = _hide(hiddenList[i], html);
    }
    if (engine.isWithCorpus()) {
         html = _hide("base-panel", html);
    } else {
         html = _hide("corpus-panel", html);
    }
    if (!engine.isWithPermalink()) {
        html = _hide("share", html);
    }
    html = html.replace(/_ [-_\.a-z0-9]+/g, function(match) {
        return engine.loc(match);
    });
    $(jqArgument).html(html);
};


Engine.Init.mainTitle = function (engine) {
    var html = "";
    if ($.isFunction(engine)) {
        html = engine.call(this);
    } else if (typeof engine === "string") {
        html = engine;
    } else {
        html += engine.loc('_ title_main');
        html += " – [";
        html += engine.getScrutariConfig().getName();
        html += "]";
    }
    $("#mainTitle").html(html);
};

Engine.Init.custom = function (engine) {
    
};

Engine.Init.forms = function (engine) {
    $("#mainsearchForm").submit(function () {
        var q = $(this).find("input[name='q']").val();
        q = $.trim(q);
        if (q.length > 0) {
            var requestParameters = Engine.Ui.checkRequestParameters(engine);
            requestParameters["log"] = "all";
            requestParameters["q"] = q;
            var _mainsearchScrutariResultCallback = function (scrutariResult) {
                Engine.Query.scrutariResult(engine, scrutariResult, "mainsearch");
                $("#loadingModal").modal('hide');
                $parametersDisplayButton = $('#parametersDisplayButton');
                if ($parametersDisplayButton.data("scrutariState") === "on") {
                    $parametersDisplayButton.click();
                }
            };
            var _mainScrutariErrorCallback = function (error) {
                Engine.Query.scrutariError(engine, error, "mainsearch");
                $("#loadingModal").modal('hide');
            };
            $("#loadingModal").modal('show');
            Scrutari.Result.newSearch(_mainsearchScrutariResultCallback, engine.getScrutariConfig(), requestParameters, _mainScrutariErrorCallback);
        }
        return false;
    });
    $("#subsearchForm").submit(function () {
        var q = $(this).find("input[name='q']").val();
        q = $.trim(q);
        if ((q.length > 0) && (engine.hasCurrentScrutariResult())) {
            var requestParameters = Engine.Ui.checkRequestParameters(engine);
            requestParameters["q"] = q;
            requestParameters["flt-qid"] = engine.getCurrentScrutariResult().getQId();
            var _subsearchScrutariResultCallback = function (scrutariResult) {
                Engine.Query.scrutariResult(engine, scrutariResult, "subsearch");
                $("#loadingModal").modal('hide');
            };
            var _subsearchScrutariErrorCallback = function (error) {
                Engine.Query.scrutariError(engine, error, "subsearch");
                $("#loadingModal").modal('hide');
            };
            $("#loadingModal").modal('show');
            Scrutari.Result.newSearch(_subsearchScrutariResultCallback, engine.getScrutariConfig(), requestParameters, _subsearchScrutariErrorCallback);
        }
        return false;
    });
};

Engine.Init.buttons = function (engine) {
    $('#langEnableButton').click(function () {
        var $this = $(this);
        var state = Scrutari.Utils.toggle($this, "scrutariState");
        Scrutari.Utils.toggle.disabled("#langCheckAllButton, #langUncheckAllButton", state);
        Scrutari.Utils.toggle.classes(".scrutari-column-Lang", state, "", "scrutari-Disabled");
        Scrutari.Utils.toggle.text($this.children("span"), "scrutariAlternate");
        var $langFilterLabel = Scrutari.Utils.toggle.text("#langFilterLabel", "scrutariAlternate");
        Scrutari.Utils.toggle.classes($langFilterLabel, state, "scrutari-panel-Active", "scrutari-Disabled");
        Engine.Ui.filterChange(engine);
    });
    $('#langCheckAllButton').click(function () {
        Scrutari.Utils.check("input[name='lang']");
        Engine.Ui.filterChange(engine);
    });
    $('#langUncheckAllButton').click(function () {
        Scrutari.Utils.uncheck("input[name='lang']");
        Engine.Ui.filterChange(engine);
    });
    $('#categoryEnableButton').click(function () {
        var $this = $(this);
        var state = Scrutari.Utils.toggle($this, "scrutariState");
        Scrutari.Utils.toggle.disabled("#categoryCheckAllButton, #categoryUncheckAllButton", state);
        Scrutari.Utils.toggle.classes(".scrutari-column-Category", state, "", "scrutari-Disabled");
        Scrutari.Utils.toggle.text($this.children("span"), "scrutariAlternate");
        var $categoryFilterLabel = Scrutari.Utils.toggle.text("#categoryFilterLabel", "scrutariAlternate");
        Scrutari.Utils.toggle.classes($categoryFilterLabel, state, "scrutari-panel-Active", "scrutari-Disabled");
        Engine.Ui.filterChange(engine);
    });
    $('#categoryCheckAllButton').click(function () {
        Scrutari.Utils.check("input[name='category']");
        Engine.Ui.filterChange(engine);
    });
    $('#categoryUncheckAllButton').click(function () {
        Scrutari.Utils.uncheck("input[name='category']");
        Engine.Ui.filterChange(engine);
    });
    if (engine.isWithCorpus()) {
        $('#corpusEnableButton').click(function () {
            var $this = $(this);
            var state = Scrutari.Utils.toggle($this, "scrutariState");
            Scrutari.Utils.toggle.disabled("#corpusCheckAllButton, #corpusUncheckAllButton", state);
            Scrutari.Utils.toggle.classes(".scrutari-column-Corpus", state, "", "scrutari-Disabled");
            Scrutari.Utils.toggle.text($this.children("span"), "scrutariAlternate");
            var $corpusFilterLabel = Scrutari.Utils.toggle.text("#corpusFilterLabel", "scrutariAlternate");
            Scrutari.Utils.toggle.classes($corpusFilterLabel, state, "scrutari-panel-Active", "scrutari-Disabled");
            Engine.Ui.filterChange(engine);
        });
        $('#corpusCheckAllButton').click(function () {
            Scrutari.Utils.check("input[name='corpus']");
            Engine.Ui.filterChange(engine);
        });
        $('#corpusUncheckAllButton').click(function () {
            Scrutari.Utils.uncheck("input[name='corpus']");
            Engine.Ui.filterChange(engine);
        });
    } else {
        $('#baseEnableButton').click(function () {
            var $this = $(this);
            var state = Scrutari.Utils.toggle($this, "scrutariState");
            Scrutari.Utils.toggle.disabled("#baseCheckAllButton, #baseUncheckAllButton", state);
            Scrutari.Utils.toggle.classes(".scrutari-column-Base", state, "", "scrutari-Disabled");
            Scrutari.Utils.toggle.text($this.children("span"), "scrutariAlternate");
            var $baseFilterLabel = Scrutari.Utils.toggle.text("#baseFilterLabel", "scrutariAlternate");
            Scrutari.Utils.toggle.classes($baseFilterLabel, state, "scrutari-panel-Active", "scrutari-Disabled");
            Engine.Ui.filterChange(engine);
        });
        $('#baseCheckAllButton').click(function () {
            Scrutari.Utils.check("input[name='base']");
            Engine.Ui.filterChange(engine);
        });
        $('#baseUncheckAllButton').click(function () {
            Scrutari.Utils.uncheck("input[name='base']");
            Engine.Ui.filterChange(engine);
        });
    }
    $('#parametersDisplayButton').click(function () {
        var state = Scrutari.Utils.toggle($(this), "scrutariState");
        if (state === 'on') {
            Engine.Ui.show('#parametersArea');
        } else {
            Engine.Ui.hide('#parametersArea');
        }
    });
    $('#modeHelpButton').click(function () {
        $modeHelpModal = $("#modeHelpModal");
        $modalBody = $modeHelpModal.find(".modal-body");
        if ($modalBody.children().length === 0) {
            $modalBody.html(engine.loc("_ help_mode.html"));
        }
        $modeHelpModal.modal('show');
    });
    $('#ponderationHelpButton').click(function () {
        $ponderationHelpModal = $("#ponderationHelpModal");
        $modalBody = $ponderationHelpModal.find(".modal-body");
        if ($modalBody.children().length === 0) {
            $modalBody.html(engine.loc("_ help_ponderation.html"));
        }
        $ponderationHelpModal.modal('show');
    });
    $('#periodeHelpButton').click(function () {
        $periodeHelpModal = $("#periodeHelpModal");
        $modalBody = $periodeHelpModal.find(".modal-body");
        if ($modalBody.children().length === 0) {
            $modalBody.html(engine.loc("_ help_periode.html"));
        }
        $periodeHelpModal.modal('show');
    });
    $("input[name='q-mode']").click(function () {
        if (this.value === 'operation') {
            Scrutari.Utils.disable("input[name='wildchar']");
        } else {
            Scrutari.Utils.enable("input[name='wildchar']");
        }
    });
};

Engine.Init.scrutariMeta = function (engine, scrutariMeta) {
    engine.setScrutariMeta(scrutariMeta);
    Engine.Init.forms(engine);
    Engine.Init.buttons(engine);
    Engine.Ui.setCount("#globalFicheCount", scrutariMeta.getGlobalFicheCount(), engine.getScrutariConfig().getLangUi());
    Engine.Ui.show("#globalFicheCount");
    Engine.Ui.show("#parametersDisplayButton");
    var langArray = scrutariMeta.getLangArray(Scrutari.Utils.sortLangByFicheCountDesc);
    if ((langArray.length > 1) && (Scrutari.exists("#langPanel"))) {
        Scrutari.Utils.divideIntoColumns(langArray, ".scrutari-column-Lang", engine.getTemplate("lang"));
            Engine.Ui.show("#langPanel");
            $("input[name='lang']").click(function () {
                var button = $('#langEnableButton');
                if (Scrutari.Utils.toggle.getState(button, 'scrutariState') === 'off') {
                    button.click();
                    $(this).focus();
                } else {
                    Engine.Ui.filterChange(engine);
                }
            });
    }
    if ((scrutariMeta.withCategory()) && (Scrutari.exists("#categoryPanel"))) {
        var categoryArray = scrutariMeta.getCategoryArray(Scrutari.Utils.sortCategoryByRankDesc);
        Scrutari.Utils.divideIntoColumns(categoryArray, ".scrutari-column-Category", engine.getTemplate("category"));
        Engine.Ui.show("#categoryPanel");
        $("input[name='category']").click(function () {
            var button = $('#categoryEnableButton');
            if (Scrutari.Utils.toggle.getState(button, 'scrutariState') === 'off') {
                button.click();
                $(this).focus();
            } else {
                Engine.Ui.filterChange(engine);
            }
        });
    }
    if (engine.isWithCorpus()) {
        if (Scrutari.exists("#corpusPanel")) {
            var corpusSortFunction = null;
            if (engine.getCorpusSort() === "fiche-count") {
                corpusSortFunction = Scrutari.Utils.sortCorpusByFicheCountDesc;
            }
            var corpusArray =  scrutariMeta.getCorpusArray(corpusSortFunction);
            if (corpusArray.length > 1) {
                Scrutari.Utils.divideIntoColumns(corpusArray, ".scrutari-column-Corpus", engine.getTemplate("corpus"));
                Engine.Ui.show("#corpusPanel");
                $("input[name='corpus']").click(function () {
                    var button = $('#corpusEnableButton');
                    if (Scrutari.Utils.toggle.getState(button, 'scrutariState') === 'off') {
                        button.click();
                        $(this).focus();
                    } else {
                        Engine.Ui.filterChange(engine);
                    }
                });
            }
        }
    } else {
        if (Scrutari.exists("#basePanel")) {
            var baseSortFunction = null;
            if (engine.getBaseSort() === "fiche-count") {
                baseSortFunction = Scrutari.Utils.sortBaseByFicheCountDesc;
            }
            var baseArray =  scrutariMeta.getBaseArray(baseSortFunction);
            if (baseArray.length > 1) {
                Scrutari.Utils.divideIntoColumns(baseArray, ".scrutari-column-Base", engine.getTemplate("base"));
                Engine.Ui.show("#basePanel");
                $("input[name='base']").click(function () {
                    var button = $('#baseEnableButton');
                    if (Scrutari.Utils.toggle.getState(button, 'scrutariState') === 'off') {
                        button.click();
                        $(this).focus();
                    } else {
                        Engine.Ui.filterChange(engine);
                    }
                });
            }
        }
    }
    var initialQuery = engine.getInitialQuery();
    var initialQId = engine.getInitialQId();
    if (initialQuery.length > 0) {
        $mainSearchForm = $("#mainsearchForm");
        if (Scrutari.exists($mainSearchForm)) {
            $mainSearchForm.find("input[name='q']").val(initialQuery);
            $mainSearchForm.submit();
        }
    } else if ((initialQId) && (initialQId.length > 0)) {
        var requestParameters = Engine.Ui.checkRequestParameters(engine);
        requestParameters["qid"] = initialQId;
        var _mainsearchScrutariResultCallback = function (scrutariResult) {
            Engine.Query.scrutariResult(engine, scrutariResult, "mainsearch");
            $("#loadingModal").modal('hide');
            $parametersDisplayButton = $('#parametersDisplayButton');
            if ($parametersDisplayButton.data("scrutariState") === "on") {
                $parametersDisplayButton.click();
            }
        };
        var _mainScrutariErrorCallback = function (error) {
            Engine.Query.scrutariError(engine, error, "mainsearch");
            $("#loadingModal").modal('hide');
        };
        $("#loadingModal").modal('show');
        Scrutari.Result.newSearch(_mainsearchScrutariResultCallback, engine.getScrutariConfig(), requestParameters, _mainScrutariErrorCallback);
    }
};