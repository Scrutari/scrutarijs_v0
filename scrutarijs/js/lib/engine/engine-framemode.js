/* global Scrutari,Engine */

Engine.FrameMode = {};
    
Engine.FrameMode.is = function (engine) {
    return (engine.getMode() === "frame");
};

Engine.FrameMode.setCurrentFicheBlock = function (code, scroll) {
    $(".scrutari-fiche-CurrentBlock").removeClass("scrutari-fiche-CurrentBlock");
    var $ficheBlock = $("#ficheBlock_" + code);
    $ficheBlock.addClass("scrutari-fiche-CurrentBlock");
    if (scroll) {
        var top = $ficheBlock.offset().top;
        var $window =  $(window);
        var windowTop = $window.scrollTop();
        var windowBottom = windowTop + $window.height();
        if ((top < windowTop) || (top > (windowBottom - 20))) {
            if ($window.height() > 300) {
                top = top - 150;
            }
            $window.scrollTop(top);
        }
    }
};

Engine.FrameMode.clear = function () {
    window.parent.frames["Navigation"].clear();
};

Engine.FrameMode.changePlageCallback = function (plageFicheArray, changePlageArguments) {
    var _linkClick = function () {
        var idx = this.id.indexOf('_');
        var code = this.id.substring(idx + 1);
        Engine.FrameMode.setCurrentFicheBlock(code, false);
        window.parent.frames["Navigation"].setCurrentFiche(code, plageFicheArray);
    };
    Engine.FrameMode.clear();
    if (changePlageArguments.categoryName) {
        $("#categoryFiches_" + changePlageArguments.categoryName + " a.scrutari-fiche-Link").click(_linkClick);
    } else {
        $("a.scrutari-fiche-Link").click(_linkClick);
    }
};
