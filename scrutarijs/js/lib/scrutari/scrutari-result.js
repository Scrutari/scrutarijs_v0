/* global Scrutari */

/***************************************************************
 * ScutariJs
 * Copyright (c) 2014-2016 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */

/**
 * Objet encapsulant un objet ficheSearchResult obtenu via l'API Json (requête type=q-fiche) et proposant
 * des accesseurs et des méthodes de traitement.
 * Voir http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_qfiche
 * 
 * @constructor
 * @param {Object} ficheSearchResult Objet ficheSearchResult de l'API SCrutari
 * @param {Function} [groupSortFunction] Fonction optionnelle de tri des groupes
 */
Scrutari.Result = function (ficheSearchResult, groupSortFunction) {
    this.ficheSearchResult = ficheSearchResult;
    this.ficheGroupArray = ficheSearchResult.ficheGroupArray;
    if ((groupSortFunction) && (this.ficheGroupArray.length > 1)) {
        this.ficheGroupArray = this.ficheGroupArray.sort(groupSortFunction);
    }
    this.motcleMap = new Object();
    if (ficheSearchResult.hasOwnProperty("motcleArray")) {
        var length = ficheSearchResult.motcleArray.length;
        for(var i = 0; i < length; i++) {
            var motcle = ficheSearchResult.motcleArray[i];
            this.motcleMap["code_" + motcle.codemotcle] = motcle;
        }
    }
};

/**
 * Fonction de retour utilisée par Scrutari.Result.newSearch.load prenant comme argument
 * une instance de Scrutari.Result
 * 
 * @callback Scrutari.Result~newSearchCallback
 * @param {Scrutari.Result} scrutariResult
 */

/**
 * Construit une instance de ScrutariResult en effectuant unee nouvelle recherche
 * définie par les paramètres en argument. Cette instance est transmise à la fonction callback
 * 
 * @param {Scrutari.Result~newSearchCallback} callback Fonction de retour du nouveau resultat Scrutari.Result
 * @param {Scrutari.Config} scrutariConfig Configuration du moteur Scrutari
 * @param {Object} [requestParameters] Paramètres de la recherche qui seront transmis au moteur Scrutari
 * @param {scrutariErrorCallback} [scrutariErrorCallback] fonction de traitement d'erreur envoyée par le moteur Scrutari
 * @returns {undefined}
 */
Scrutari.Result.newSearch = function (callback, scrutariConfig, requestParameters, scrutariErrorCallback) {
    var _ficheSearchResultCallback = function (ficheSearchResult) {
        callback(new Scrutari.Result(ficheSearchResult, scrutariConfig.getGroupSortFunction()));
    };
    Scrutari.Ajax.loadFicheSearchResult(_ficheSearchResultCallback, scrutariConfig, requestParameters, scrutariErrorCallback);
};

/**
 * Retourne l'identifiant de la recherche au sein du moteur Scrutari.
 * 
 * @returns {String} Identifiant de la recherche
 */
Scrutari.Result.prototype.getQId = function () {
    return this.ficheSearchResult.qId;
};

Scrutari.Result.prototype.getFormatedSearchSequence = function (scrutariLoc) {
    var q = this.ficheSearchResult.q;
    q = q.replace(/\&\&/g, scrutariLoc.loc('_ scrutari-and'));
    q = q.replace(/\|\|/g, scrutariLoc.loc('_ scrutari-or'));
    return q;
};

/**
 * Retourne le nombre de fiches comprises dans le résultat de la recherche
 * 
 * @returns {Number} Nombre de fiches dans le résultat
 */
Scrutari.Result.prototype.getFicheCount = function () {
    return this.ficheSearchResult.ficheCount;
};

/**
 * Retourne « none » ou « category »
 */
Scrutari.Result.prototype.getFicheGroupType = function () {
    return this.ficheSearchResult.ficheGroupType;
};


Scrutari.Result.prototype.getNoneFicheArray = function () {
    if (this.ficheGroupArray.length === 0) {
        return new Array();
    }
    return this.ficheGroupArray[0].ficheArray;
};

/**
 * Retourne la liste des fiches de la plage correspondante, à utiliser
 */
Scrutari.Result.prototype.selectNoneFicheArray = function (plageLength, plageNumber) {
    var selectionArray = new Array();
    if (this.ficheGroupArray.length === 0) {
        return selectionArray;
    }
    var ficheArray = this.ficheGroupArray[0].ficheArray;
    var startIndex = plageLength * (plageNumber - 1);
    var length = ficheArray.length;
    if (startIndex >= length) {
        return selectionArray;
    }
    var min = Math.min(ficheArray.length, startIndex + plageLength);
    for(var i = startIndex; i < min; i++) {
        selectionArray.push(ficheArray[i]);
    }
    return selectionArray;
};

Scrutari.Result.prototype.isNonePlageLoaded = function (plageLength, plageNumber) {
    if (this.ficheGroupArray.length === 0) {
        return true;
    }
    var ficheCount = this.ficheSearchResult.ficheCount;
    var ficheArray = this.ficheGroupArray[0].ficheArray;
    var length = ficheArray.length;
    if (length === ficheCount) {
        return true;
    }
    var endIndex = (plageLength * plageNumber) - 1;
    if (endIndex < length) {
        return true;
    }
    return false;
};

Scrutari.Result.prototype.loadNonePlage = function (callback, scrutariConfig, plageLength, plageNumber) {
    if (this.ficheGroupArray.length === 0) {
        return true;
    }
    var group = this.ficheGroupArray[0];
    if (!group) {
        return;
    }
    var ficheCount = this.ficheSearchResult.ficheCount;
    var ficheArray = group.ficheArray;
    var length = ficheArray.length;
    if (length === ficheCount) {
        return;
    }
    var _existingFicheSearchResultCallback = function (ficheSearchResult) {
        var newCount = ficheSearchResult.ficheGroupArray.length;
        if (newCount > 0) {
            group.ficheArray = group.ficheArray.concat(ficheSearchResult.ficheGroupArray[0].ficheArray);
        }
        callback();
    };
    var requestParameters = {
        qid: this.ficheSearchResult.qId,
        start: length +1,
        limit: (plageLength * (plageNumber + 2)) - length
    };
    Scrutari.Ajax.loadExistingFicheSearchResult(_existingFicheSearchResultCallback, scrutariConfig, requestParameters); 
};

Scrutari.Result.prototype.isCategoryPlageLoaded = function (categoryName, plageLength, plageNumber) {
    var group = this.getFicheGroupByCategoryName(categoryName);
    if (!group) {
        return true;
    }
    var categoryFicheCount = group.ficheCount;
    var ficheArray = group.ficheArray;
    var length = ficheArray.length;
    if (length === categoryFicheCount) {
        return true;
    }
    var endIndex = (plageLength * plageNumber) - 1;
    if (endIndex < length) {
        return true;
    }
    return false;
};

Scrutari.Result.prototype.loadCategoryPlage = function (callback, scrutariConfig, categoryName, plageLength, plageNumber) {
    var group = this.getFicheGroupByCategoryName(categoryName);
    if (!group) {
        return;
    }
    var categoryFicheCount = group.ficheCount;
    var ficheArray = group.ficheArray;
    var length = ficheArray.length;
    if (length === categoryFicheCount) {
        return;
    }
    var _existingFicheSearchResultCallback = function (ficheSearchResult) {
        var newCount = ficheSearchResult.ficheGroupArray.length;
        for(var i = 0; i < newCount; i++) {
            var newGroup = ficheSearchResult.ficheGroupArray[i];
            if (newGroup.category.name === group.category.name) {
                group.ficheArray = group.ficheArray.concat(newGroup.ficheArray);
            }
        }
        callback();
    };
    var requestParameters = {
        qid: this.ficheSearchResult.qId,
        start: length +1,
        limit: (plageLength * (plageNumber + 2)) - length,
        starttype: "in:" + categoryName
    };
    Scrutari.Ajax.loadExistingFicheSearchResult(_existingFicheSearchResultCallback, scrutariConfig, requestParameters); 
};


Scrutari.Result.prototype.selectCategoryFicheArray = function (categoryName, plageLength, plageNumber) {
    var selectionArray = new Array();
    var ficheArray = this.getCategoryFicheArrayByName(categoryName);
    var startIndex = plageLength * (plageNumber - 1);
    var length = ficheArray.length;
    if (startIndex >= length) {
        return selectionArray;
    }
    var min = Math.min(ficheArray.length, startIndex + plageLength);
    for(var i = startIndex; i < min; i++) {
        selectionArray.push(ficheArray[i]);
    }
    return selectionArray;
};

Scrutari.Result.prototype.getCategoryCount = function () {
    return this.ficheGroupArray.length;
};

Scrutari.Result.prototype.getCategory = function (index) {
    return this.ficheGroupArray[index].category;
};

Scrutari.Result.prototype.getFicheGroupByCategoryName = function (categoryName) {
    var groupCount = this.ficheGroupArray.length;
    for(var i = 0; i < groupCount; i++) {
        var group = this.ficheGroupArray[i];
        if ((group.category) && (group.category.name === categoryName)) {
            return group;
        }
    }
    return null;
};

Scrutari.Result.prototype.getCategoryFicheCount = function (index) {
    return this.ficheGroupArray[index].ficheCount;
};

Scrutari.Result.prototype.getCategoryFicheCountbyName = function (categoryName) {
    var groupCount = this.ficheGroupArray.length;
    for(var i = 0; i < groupCount; i++) {
        var group = this.ficheGroupArray[i];
        if ((group.category) && (group.category.name === categoryName)) {
            return group.ficheCount;
        }
    }
    return 0;
};

Scrutari.Result.prototype.getCategoryFicheArray = function (index) {
    return this.ficheGroupArray[index].ficheArray;
};

Scrutari.Result.prototype.getCategoryFicheArrayByName = function (categoryName) {
    var categoryCount = this.getCategoryCount();
    for(var i = 0; i < categoryCount; i++) {
        var category = this.getCategory(i);
        if (category.name === categoryName) {
            return this.getCategoryFicheArray(i);
        }
    }
    return new Array();
};

Scrutari.Result.prototype.getMotcle = function (code) {
    var key = "code_" + code;
    if (this.motcleMap.hasOwnProperty(key)) {
        return this.motcleMap[key];
    } else {
        return null;
    }
};