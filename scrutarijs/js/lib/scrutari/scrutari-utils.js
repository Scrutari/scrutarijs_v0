/* global Scrutari */

/***************************************************************
 * ScutariJs
 * Copyright (c) 2014-2017 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */

/**
 * Contient des fonctions utilitaires « statiques ». Aucune de ces fonctions ne doit altérer
 * l'état de leurs arguments ou de variables globales.
 */
Scrutari.Utils = {};

/**
 * Répartit les objets dans objectArray dans l'ensemble des élements HTML définis
 * par jqArgument.
 * objectTemplate est la fonction à utiliser pour produire le code html de l'objet.
 * Elle doit prendre comme argument un objet de objectArray
 */
Scrutari.Utils.divideIntoColumns = function (objectArray, jqArgument, objectTemplate) {
    var objectCount = objectArray.length;
    if (objectCount === 0) {
        return;
    }
    var $elements = Scrutari.convert(jqArgument);
    var elementCount = $elements.length;
    if (elementCount === 0) {
        Scrutari.log("HtmlElement selection with jqArgument is empty ");
        return;
    }
    var objectCount = objectArray.length;
    if (objectCount <= elementCount) {
        for(var i = 0; i < objectCount; i++) {
            $($elements[i]).append(objectTemplate(objectArray[i]));
        }
        return;
    }
    var modulo = objectCount % elementCount;
    var columnLength = (objectCount - modulo) / elementCount;
    var start = 0;
    var stop = 0;
    for(var i = 0; i< elementCount; i++) {
        var $element = $($elements[i]);
        stop += columnLength;
        if (i < modulo) {
            stop++;
        }
        for(var j = start; j < stop; j++) {
            $element.append(objectTemplate(objectArray[j]));
        }
        start = stop;
    }
};

/**
 * paginationTemplate prend comme un argument un objet avec les propriétés suivantes : 
 * {
 *    number: le numéro de la plage,
 *    title: son titre,
 *    state: son état
 * }
 */
Scrutari.Utils.checkPagination = function (ficheCount, plageLength, currentPlageNumber, jqArgument, paginationTemplate) {
    var $elements = Scrutari.convert(jqArgument);
    $elements.empty();
    var plageCount;
    if (ficheCount <= plageLength) {
        plageCount = 1;
        return;
    } else {
        var modulo = ficheCount % plageLength;
        plageCount = (ficheCount - modulo) / plageLength;
        if (modulo > 0) {
            plageCount ++;
        }
    }
    if (currentPlageNumber > plageCount) {
        currentPlageNumber = plageCount;
    }
    var plageNumberStart = 1;
    var plageNumberEnd = 9;
    if (currentPlageNumber > 6) {
       
        plageNumberStart = currentPlageNumber - 3;
        plageNumberEnd = currentPlageNumber + 3;
    }
    if (plageNumberEnd > plageCount) {
        plageNumberEnd = plageCount;
    }
    if (plageNumberStart > 1) {
        $elements.append(paginationTemplate({
                number: 1,
                title: "1",
                state: 'enabled'
        }));
        $elements.append(paginationTemplate({
                number: 0,
                title: "…",
                state: 'disabled'
        }));
    }
    for(var i = plageNumberStart; i <= plageNumberEnd; i++) {
        var state = 'enabled';
        if (i == currentPlageNumber) {
            state = 'active';
        }
        var html = paginationTemplate({
                number: i,
                title: i.toString(),
                state: state
        });
        $elements.append(html);
    }
    if (plageNumberEnd < plageCount) {
        $elements.append(paginationTemplate({
                number: 0,
                title: "…",
                state: 'disabled'
        }));
    }
};

/**
 * Désactive les éléments de selectors
 */
Scrutari.Utils.disable = function (jqArgument) {
    var $elements = Scrutari.convert(jqArgument);
    $elements.prop('disabled', true);
    return $elements;
};

/**
 * Active les éléments de selectors
 */
Scrutari.Utils.enable = function (jqArgument) {
    var $elements = Scrutari.convert(jqArgument);
    $elements.prop('disabled', false);
    return $elements;
};

/**
 * Coche les éléments de selectors
 */
Scrutari.Utils.uncheck = function (jqArgument) {
    var $elements = Scrutari.convert(jqArgument);
    $elements.prop('checked', false);
    return $elements;
};

/**
 * Décoche les éléments de selectors
 */
Scrutari.Utils.check = function (jqArgument) {
    var $elements = Scrutari.convert(jqArgument);
    $elements.prop('checked', true);
    return $elements;
};

/**
 * Bascule de la valeur « on » à « off » ou « off » à « on » pour la clé stateDataKey
 */
Scrutari.Utils.toggle = function (jqElement, stateDataKey) {
    var state = jqElement.data(stateDataKey);
    if (state === 'off') {
        state = 'on';
    } else {
        state = 'off';
    }
    jqElement.data(stateDataKey, state);
    return state;
};

/**
 * Renvoie la valeur d'état pour la clé stateDataKey
 */
Scrutari.Utils.toggle.getState = function (jqArgument, stateDataKey) {
    var $elements = Scrutari.convert(jqArgument);
    return $elements.data(stateDataKey);
};

/**
 * Active ou désactive les éléments au sens HTML (attribut disabled)
 * en fonction de la valeur de state (« off » ou « on »)
 */
Scrutari.Utils.toggle.disabled = function (jqArgument, state) {
    var $elements = Scrutari.convert(jqArgument);
    if (state === 'off') {
        $elements.prop('disabled', true);
    } else {
        $elements.prop('disabled', false);
    }
    return $elements;
};

/**
 * Change le texte actuel par celui de la clé alterDataKey et conserve
 * l'ancienne valeur du texte dans le clé alterDataKey
 */
Scrutari.Utils.toggle.text = function (jqArgument, alterDataKey) {
    var $elements = Scrutari.convert(jqArgument);
    var length = $elements.length;
    for(var i = 0; i < length; i++) {
        var jqEl = $($elements[i]);
        var currentText =jqEl.text();
        var alterText = jqEl.data(alterDataKey);
        jqEl.text(alterText);
        jqEl.data(alterDataKey, currentText);
    }
    return $elements;
};

/**
 * Bascule de la classe onClass à offClass si state est égal à « off »
 * ou sinon de la classe offClass à la classe onClass
 */
Scrutari.Utils.toggle.classes = function (jqArgument, state, onClass, offClass) {
    var $elements = Scrutari.convert(jqArgument);
    if (state === 'off') {
        $elements.addClass(offClass).removeClass(onClass);
    } else {
        $elements.removeClass(offClass).addClass(onClass);
    }
    return $elements;
};

/**
 * Comparateur des bases en classant par le plus grand nombre de fiches
 */
Scrutari.Utils.sortBaseByFicheCountDesc = function (base1, base2) {
    var count1 = base1.stats.fiche;
    var count2 = base2.stats.fiche;
    if (count1 > count2) {
        return -1;
    } else if (count1 < count2) {
        return 1;
    } else {
        var code1 = base1.codebase;
        var code2 = base2.codebase;
        if (code1 < code2) {
            return -1;
        } else if (code1 > code2) {
            return 1;
        } else {
            return 0;
        }
    }
};

/**
 * Comparateur des langues en classant par le plus grand nombre de fiches
 */
Scrutari.Utils.sortLangByFicheCountDesc = function (lang1, lang2) {
    var count1 = lang1.fiche;
    var count2 = lang2.fiche;
    if (count1 > count2) {
        return -1;
    } else if (count1 < count2) {
        return 1;
    } else {
        var code1 = lang1.lang;
        var code2 = lang2.lang;
        if (code1 < code2) {
            return -1;
        } else if (code1 > code2) {
            return 1;
        } else {
            return 0;
        }
    }
};

/**
 * Comparateur des langues en classant par le plus grand nombre de fiches
 */
Scrutari.Utils.sortCategoryByRankDesc = function (category1, category2) {
    var count1 = category1.rank;
    var count2 = category2.rank;
    if (count1 > count2) {
        return -1;
    } else if (count1 < count2) {
        return 1;
    } else {
        var code1 = category1.name;
        var code2 = category2.name;
        if (code1 < code2) {
            return -1;
        } else if (code1 > code2) {
            return 1;
        } else {
            return 0;
        }
    }
};

/**
 * Comparateur des corpus en classant par le plus grand nombre de fiches
 */
Scrutari.Utils.sortCorpusByFicheCountDesc = function (corpus1, corpus2) {
    var count1 = corpus1.stats.fiche;
    var count2 = corpus2.stats.fiche;
    if (count1 > count2) {
        return -1;
    } else if (count1 < count2) {
        return 1;
    } else {
        var code1 = corpus1.codecorpus;
        var code2 = corpus2.codecorpus;
        if (code1 < code2) {
            return -1;
        } else if (code1 > code2) {
            return 1;
        } else {
            return 0;
        }
    }
};

/**
 *
 *@returns {Number} -1,0 ou 1
 */
Scrutari.Utils.sortGroupByFicheCount = function (group1, group2) {
    var count1 = group1.ficheCount;
    var count2 = group2.ficheCount;
    if (count1 > count2) {
        return -1;
    } else if (count1 < count2) {
        return 1;
    } else {
        var rank1 = group1.category.rank;
        var rank2 = group1.category.rank;
        if (rank1 < rank2) {
            return -1;
        } else if (rank1 > rank2) {
            return 1;
        } else {
            return 0;
        }
    }
};

 /**
  * Tableau d'un élément « marqué » (mtitre, mlibelle)
  * 
  * @returns {String} Code HTML correspondant
  */
Scrutari.Utils.mark = function (markArray, classAttribute) {
    if (!classAttribute) {
        classAttribute = "scrutari-Mark";
    }
    var html = "";
    var length = markArray.length;
    for (var i = 0; i < length; i++) {
        var obj = markArray[i];
        if (typeof obj === 'string') {
            html += obj;
        } else if (obj.s) {
            html += "<span class='" + classAttribute + "'>";
            html += Scrutari.escape(obj.s);
            html += "</span>";
        }
    }
    return html;
};

/**
 * Construit une fonction de gabarit pour un objet pagination
 */
Scrutari.Utils.buildPaginationTemplate = function (paginationIdPrefix) {
    var _paginationTemplate = function (obj) {
        var html = "";
        if (obj.state === 'active') {
            html += "<li class='active'>";
            html += "<span>";
            html += Scrutari.escape(obj.title);
            html += "</span>";
            html += "</li>";
        } else if (obj.state === 'disabled') {
            html += "<li class='disabled'>";
            html += "<span>";
            html += Scrutari.escape(obj.title);
            html += "</span>";
            html += "</li>";
        } else {
            html += "<li>";
            html += "<a href='#" + paginationIdPrefix + obj.number + "'>";
            html += Scrutari.escape(obj.title);
            html += "</a>";
            html += "</li>";
        }
        return html;   
    };
    return _paginationTemplate;
};

/**
 * Construit une fonction de gabarit pour un objet fiche
 * 
 */ 
 Scrutari.Utils.buildFicheTemplate = function (scrutariMeta, scrutariLoc, scrutariResult, target) {
     var _attrToString = function (mattrMap, attributeArray) {
         var html = "";
         var length = attributeArray.length;
         for(var i = 0; i < length; i++) {
            var attribute = attributeArray[i];
            if (mattrMap.hasOwnProperty(attribute.name)) {
                 var valueArray = mattrMap[attribute.name];
                 var valLength = valueArray.length;
                 html += "<p class='scrutari-fiche-Attribute'>";
                 html += '<span class="scrutari-label-Attribute">';
                 html += attribute.title;
                 html += scrutariLoc.loc("_ scrutari-colon");
                 html += '</span>';
                 if (attribute.type === "block") {
                     html += '</p><p class="scrutari-fiche-AttrBlocks">…<br/>';
                     for(var j = 0; j< valLength; j++) {
                        if (j > 0) {
                            html += "<br/>…<br/>";
                        }
                        html +=Scrutari.Utils.mark(valueArray[j]);
                     }
                     html += '<br/>…</p>';
                 } else {
                     html += ' ';
                     for(var j = 0; j< valLength; j++) {
                        if (j > 0) {
                            html += ", ";
                        }
                        html +=Scrutari.Utils.mark(valueArray[j]);
                     }
                     html += "</p>";
                 }
            }
         }
         return html;
     };
     var _ficheTemplate = function (fiche) {
        var html = "";
        html += "<div class='scrutari-fiche-Block' id='ficheBlock_" + fiche.codefiche + "'>";
        if (fiche.hasOwnProperty("icon")) {
            html += "<div class='scrutari-fiche-Icon'><img alt='' src='" + fiche.icon + "'/></div>";
        }
        html += "<p class='scrutari-fiche-Titre'>";
        html += "<a href='" + fiche.href + "'";
        html += " class='scrutari-fiche-Link'";
        html += " id='ficheLink_" + fiche.codefiche + "'";
        if (target.length > 0) {
            html += " target='" + target + "'";
        }
        html += ">";
        if (fiche.hasOwnProperty("mtitre")) {
            html +=Scrutari.Utils.mark(fiche.mtitre);
        } else {
            html += fiche.href;
        }
        html += "</a>";
        html += "</p>";
        if (fiche.hasOwnProperty("msoustitre")) {
            html += "<p class='scrutari-fiche-Soustitre'>";
            html +=Scrutari.Utils.mark(fiche.msoustitre);
            html += "</p>";
        }
        if (fiche.hasOwnProperty("annee")) {
            html += "<p class='scrutari-fiche-Annee'>";
            html += fiche.annee;
            html += "</p>";
        }
        if (fiche.hasOwnProperty("mattrMap")) {
            html += _attrToString(fiche.mattrMap, scrutariMeta.getAttributeArray("primary"));
        }
        if (fiche.hasOwnProperty("mcomplementArray")) {
            var complementLength = fiche.mcomplementArray.length;
            for(var i = 0; i < complementLength; i++) {
                var complement = fiche.mcomplementArray[i];
                html += "<p class='scrutari-fiche-Complement'>";
                html += '<span class="scrutari-label-Complement">';
                html += scrutariMeta.getComplementIntitule(fiche.codecorpus, complement.num);
                html += scrutariLoc.loc("_ scrutari-colon");
                html += '</span> ';
                html +=Scrutari.Utils.mark(complement.mcomp);
                html += "</p>";
            }
        }
        if (fiche.hasOwnProperty("mattrMap")) {
            html += _attrToString(fiche.mattrMap, scrutariMeta.getAttributeArray("secondary"));
        }
        if (fiche.hasOwnProperty('codemotcleArray')) {
            var length = fiche.codemotcleArray.length;
            html += '<p class="scrutari-fiche-Motcle">';
            html += '<span class="scrutari-label-Motcle">';
            if (length === 1) {
                html += scrutariLoc.loc("_ scrutari-motscles_un");
            } else {
                html += scrutariLoc.loc("_ scrutari-motscles_plusieurs");
            }
            html += '</span> ';
            for(var i = 0; i < length; i++) {
                if (i > 0) {
                    html += ", ";
                }
                var code = fiche.codemotcleArray[i];
                var motcle = scrutariResult.getMotcle(code);
                var libelleLength = motcle.mlibelleArray.length;
                for(var j = 0; j < libelleLength; j++) {
                    if (j > 0) {
                        html += "/";
                    }
                    html +=Scrutari.Utils.mark(motcle.mlibelleArray[j].mlib);
                }
            }
            html += "</p>";
        }
        html += '</div>';
        return html;
    };
    return _ficheTemplate;
 };


