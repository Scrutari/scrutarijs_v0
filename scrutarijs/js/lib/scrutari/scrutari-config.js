/* global Scrutari */

/***************************************************************
 * ScutariJs
 * Copyright (c) 2014-2016 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */

/**
 * Construction d'un objet Scrutari.Config.
 * Une objet Scrutari.Config conserve des informations de configuration,
 * en particulier l'URL d'accès au moteur Scrutari.
 * 
 * @constructor
 * @param {String} name Nom de la configuration
 * @param {String} engineUrl Adresse du moteur Scrutari
 * @param {String} langUi Langue de l'interface
 * @param {String} origin Paramètre origin transmis au moteur Scrutari
 */
Scrutari.Config = function (name, engineUrl, langUi, origin) {
    this.name = name;
    this.engineUrl = engineUrl;
    this.langUi = langUi;
    this.origin = origin;
    this.ficheFields = "";
    this.plageLength = 50;
    this.groupSortFunction = Scrutari.Utils.sortGroupByFicheCount;
};

/**
 * Retourne le nom associé à la configuration
 * 
 * @returns {String} Nom de la configuration
 */
Scrutari.Config.prototype.getName = function () {
    return this.name;
};

/**
 * Retourne l'URL d'appel Json du moteur Scrutari
 * 
 * @returns {String} URL d'appel du Json
 */
Scrutari.Config.prototype.getJsonUrl = function () {
    return this.engineUrl + "json";
};

/**
 * Retourne la langue à utiliser pour l'interface.
 * Ce doit être un code ISO
 * 
 * @returns {String} la langue de l'interface
 */
Scrutari.Config.prototype.getLangUi = function () {
    return this.langUi;
};

/**
 * Retourne le nom du paramètre origin qui sera transmis au moteur Scrutari.
 * Ce nom servira à identifier le client dans les journaux de recherche.
 * 
 * @returns {String} Valeur du paramètre origin transmise au moteur
 */
Scrutari.Config.prototype.getOrigin = function () {
    return this.origin;
};

/**
 * Retourne la longueur d'une plage de résultats, autrement dit le nombre de
 * résultats affichés par onglet (par défaut, 50)
 * 
 * @returns {Number} Valeur de la longueur de la plage
 */
Scrutari.Config.prototype.getPlageLength = function () {
    return this.plageLength;
};

/**
 * Modifie la longueur d'une plage de résultats.
 * 
 * @param {Number} plageLength Nouvelle valeur de la longueur d'une plage
 * @returns {undefined}
 */
Scrutari.Config.prototype.setPlageLength = function (plageLength) {
    this.plageLength = plageLength;
};

/**
 * Retourne l'URL de récupération du fichier Ods correspondant à la recherche
 * possédant l'identifiant qId. Cette URL est construite à partir de l'URL du moteur
 * 
 * @param {String} qId Idenfiant de la recherche    
 * @returns {String} URL des données de la recherche au format ODS
 */
Scrutari.Config.prototype.getOdsUrl = function (qId) {
    return this.engineUrl + "export/" +  "result_" + qId + "_" + this.langUi + ".ods";
};

/**
 * Retourne l'URL du flux de syndication Atom qui permet de récupérer les résultats
 * d'une recherche donnée ainsi que d'être prévenu des nouvelles entrées répondant
 * aux critères de la recherche.
 * 
 * @param {String} qId Idenfiant de la recherche 
 * @returns {String} URL du flux de syndication Atom
 */
Scrutari.Config.prototype.getAtomUrl = function (qId) {
    var date = new Date();
    var dateString = date.getFullYear() + "-";
    var mois = date.getMonth() + 1;
    if (mois < 10) {
        dateString += "0";
    }
    dateString += mois;
    dateString += "-";
    var jour = date.getDate();
    if (jour < 10) {
        dateString += "0";
    }
    dateString += jour;
    return this.engineUrl + "feed/" + "fiches_" + this.langUi + ".atom?qid=" + qId + "&all=" + dateString;
};

/**
 * Construit un permalien de fonction de permalinkPattern
 * $QUI et $LANG sont remplacés par les bonnes valeurs
 * 
 * @param {String} qId Idenfiant de la recherche
 * @param {String} permalinkPattern Gabarit de l'URL       
 * @returns {String} URL du permalien
 */
Scrutari.Config.prototype.getPermalinkUrl = function (qId, permalinkPattern) {
    var permalink = permalinkPattern.replace("$LANG", this.langUi);
    permalink = permalink.replace("$QID", qId);
    return permalink;
};

/**
 * Retourne la valeur du paramètre fichefields qui sera transmis au moteur au moment
 * d'une recherche. Ce paramètre liste les champs qui doivent être renvoyés dans le
 * JSON résultant. Chaine vide par défaut.
 * 
 * @returns {String} Valeur du paramètre fichefields transmise au moteur
 */
Scrutari.Config.prototype.getFicheFields = function () {
    return this.ficheFields;
};

/**
 * Modifie la valeur du paramètre ficheFields qui sera transmise au moteur Scrutari.
 * 
 * @param {String} ficheFields Nouvelle valeur du paramètre ficheFields
 * @returns {undefined}
 */
Scrutari.Config.prototype.setFicheFields = function (ficheFields) {
    this.ficheFields = ficheFields;
};

Scrutari.Config.prototype.setGroupSortFunction = function (groupSortFunction) {
    this.groupSortFunction = groupSortFunction;
};

Scrutari.Config.prototype.getGroupSortFunction = function () {
    return this.groupSortFunction;
};

