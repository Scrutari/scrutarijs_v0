/* global Scrutari */

/***************************************************************
 * ScutariJs
 * Copyright (c) 2014-2016 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */

/**
 * Objet définissant le filtre à utiliser pour la recherche
 * 
 * @constructor
 */
Scrutari.Filter = function () {
    this.withBase = false;
    this.withCategory = false;
    this.withBase = false;
    this.withCorpus = false;
    this.baseArray = new Array();
    this.langArray = new Array();
    this.categoryArray = new Array();
    this.corpusArray = new Array();
};

Scrutari.Filter.prototype.clearArrays = function () {
    while(this.baseArray.length > 0) {
        this.baseArray.pop();
    }
    while(this.langArray.length > 0) {
        this.langArray.pop();
    }
    while(this.categoryArray.length > 0) {
        this.categoryArray.pop();
    }
    while(this.corpusArray.length > 0) {
        this.corpusArray.pop();
    }
};

Scrutari.Filter.prototype.setWithBase = function (bool) {
    this.withBase = bool;
};

Scrutari.Filter.prototype.setWithLang = function (bool) {
    this.withLang = bool;
};

Scrutari.Filter.prototype.setWithCategory = function (bool) {
    this.withCategory = bool;
};

Scrutari.Filter.prototype.setWithCorpus = function (bool) {
    this.withCorpus = bool;
};

Scrutari.Filter.prototype.addBase = function (base) {
    this.baseArray.push(base);
};

Scrutari.Filter.prototype.addLang = function (lang) {
    this.langArray.push(lang);
};

Scrutari.Filter.prototype.addCategory = function (category) {
    this.categoryArray.push(category);
};

Scrutari.Filter.prototype.addCorpus = function (corpus) {
    this.corpusArray.push(corpus);
};

Scrutari.Filter.prototype.checkRequestParameters = function (scrutariMeta, requestParameters) {
    if (this.hasLang()) {
        requestParameters.langlist = this.langArray.join(",");
    }
    if (this.hasBase()) {
        requestParameters.baselist = this.baseArray.join(",");
    }
    if (this.hasCategory()) {
        requestParameters.categorylist = this.categoryArray.join(",");
    }
    if (this.hasCorpus()) {
        requestParameters.corpuslist = this.corpusArray.join(",");
    }
};

Scrutari.Filter.prototype.getFilterFicheCount = function (scrutariMeta) {
    if (!this.hasFilter()) {
        return scrutariMeta.getGlobalFicheCount();
    }
    var filterFicheCount = 0;
    if (this.hasCorpusFilter()) {
        var corpusMap = this.checkCorpusMap(scrutariMeta);
        var completeValue = corpusMap.completeValue;
        for(var key in corpusMap) {
            if (key === 'completeValue') {
                continue;
            }
            if (corpusMap[key] === completeValue) {
                var codecorpus = parseInt(key.substring(5));
                if (this.hasLang()) {
                    filterFicheCount += scrutariMeta.getCorpusLangFicheCount(codecorpus, this.langArray);
                } else {
                    filterFicheCount += scrutariMeta.getCorpusFicheCount(codecorpus);
                }
            }
        }
    } else {
        filterFicheCount += scrutariMeta.getGlobalLangFicheCount(this.langArray);
    }
    return filterFicheCount;
};

Scrutari.Filter.prototype.hasFilter = function () {
    if (this.hasLang()) {
        return true;
    }
    if (this.hasBase()) {
        return true;
    }
    if (this.hasCategory()) {
        return true;
    }
    if (this.hasCorpus()) {
        return true;
    }
    return false;
};

Scrutari.Filter.prototype.hasCorpusFilter = function () {
    if (this.hasBase()) {
        return true;
    }
    if (this.hasCategory()) {
        return true;
    }
    if (this.hasCorpus()) {
        return true;
    }
    return false;
};

Scrutari.Filter.prototype.checkCorpusMap = function (scrutariMeta) {
    var corpusMap = new Object();
    var finalCount = 0;
    if (this.hasCategory())  {
        finalCount++;
        var arrayForCategories = scrutariMeta.getCorpusArrayForCategories(this.categoryArray);
        for(var i = 0; i < arrayForCategories.length; i++) {
            var key = "code_" + arrayForCategories[i];
            if (corpusMap.hasOwnProperty(key)) {
                corpusMap[key] = corpusMap[key] + 1;
            } else {
                corpusMap[key] = 1;
            }
        }
    }
    if (this.hasBase())  {
        finalCount++;
        var arrayForBases = scrutariMeta.getCorpusArrayForBases(this.baseArray);
        for(var i = 0; i < arrayForBases.length; i++) {
            var key = "code_" + arrayForBases[i];
            if (corpusMap.hasOwnProperty(key)) {
                corpusMap[key] = corpusMap[key] + 1;
            } else {
                corpusMap[key] = 1;
            }
        }
    }
    if (this.hasCorpus())  {
        finalCount++;
        for(var i = 0; i < this.corpusArray.length; i++) {
            var key = "code_" + this.corpusArray[i];
            if (corpusMap.hasOwnProperty(key)) {
                corpusMap[key] = corpusMap[key] + 1;
            } else {
                corpusMap[key] = 1;
            }
        }
    }
    corpusMap.completeValue = finalCount;
    return corpusMap;
};

Scrutari.Filter.prototype.hasLang = function () {
    return ((this.withLang) && (this.langArray.length > 0));
};

Scrutari.Filter.prototype.hasBase = function () {
    return ((this.withBase) && (this.baseArray.length > 0));
};

Scrutari.Filter.prototype.hasCategory = function () {
    return ((this.withCategory) && (this.categoryArray.length > 0));
};

Scrutari.Filter.prototype.hasCorpus = function () {
    return ((this.withCorpus) && (this.corpusArray.length > 0));
};