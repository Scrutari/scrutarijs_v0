/* global Scrutari */

/***************************************************************
 * ScutariJs
 * Copyright (c) 2014 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */

/**
 * Fonctions statiques d'appel Ajax
 */
Scrutari.Ajax = {};

/*
 * Requête Json type=base
 * baseArrayCallback doit accepter comme argument un tableau baseArray tel que définit dans
 * http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_base
 * requestParameters est facultatif
 */
Scrutari.Ajax.loadBaseArray = function (baseArrayCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "base";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "baseArray", baseArrayCallback);
        }
    });
};

/*
 * Requête Json type=corpus
 * corpusArrayCallback doit accepter comme argument un tableau corpusArray tel que définit dans
 * http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_corpus
 * requestParameters est facultatif
 */
Scrutari.Ajax.loadCorpusArray = function (corpusArrayCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "corpus";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "corpusArray", corpusArrayCallback);
        }
    });
};

/*
 * Requête Json type=thesaurus
 * thesaurusArrayCallback doit accepter comme argument un tableau thesaurusArray tel que définit dans
 * http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_thesaurus
 * requestParameters est facultatif
 */
Scrutari.Ajax.loadThesaurusArray = function (thesaurusArrayCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "thesaurus";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "thesaurusArray", thesaurusArrayCallback);
        }
    });
};

/*
 * Requête Json type=motcle
 * motcleArrayCallback doit accepter comme argument un tableau motcleArray tel que définit dans
 * http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_motcle
 * requestParameters est facultatif
 */
Scrutari.Ajax.loadMotcleArray = function (motcleArrayCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "motcle";
    Scrutari.Ajax.check(requestParameters, "motclefields", "motcleid,libelles");
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "motcleArray", motcleArrayCallback);
        }
    });
};

/*
 * Requête Json type=fiche
 * ficheArrayCallback doit accepter comme argument un tableau ficheArray tel que définit dans
 * http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_fiche
 * requestParameters est facultatif
 */
Scrutari.Ajax.loadFicheArray = function (ficheArrayCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "fiche";
    Scrutari.Ajax.check(requestParameters, "fichefields", "titre");
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "ficheArray", ficheArrayCallback);
        }
    });
};

/*
 * Requête Json type=category
 * categoryArrayCallback doit accepter comme argument un tableau categoryArray tel que définit dans
 * http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_category
 * requestParameters est facultatif
 */
Scrutari.Ajax.loadCategoryArray = function (categoryArrayCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "category";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "categoryArray", categoryArrayCallback);
        }
    });
};

/*
 * Requête Json type=engine
 * engineInfoCallback doit accepter comme argument un objet engineInfo tel que définit dans
 * http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_engine
 * requestParameters est facultatif
 */
Scrutari.Ajax.loadEngineInfo = function (engineInfoCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "engine";
    Scrutari.Ajax.check(requestParameters, "data", "all");
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 1);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "engineInfo", engineInfoCallback);
        }
    });
};

/*
 * requestParameters est obligatoire et doit posséder une propriété q ou des propriétés q_*
 * tel que défini dans http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_qfiche
 * scrutariErrorCallback est facultatif
 */
Scrutari.Ajax.loadFicheSearchResult = function (ficheSearchResultCallback, scrutariConfig, requestParameters, scrutariErrorCallback) {
    var defaultFicheFields = scrutariConfig.getFicheFields();
    if (!defaultFicheFields) {
        defaultFicheFields = "codecorpus,mtitre,msoustitre,mattrs_all,mcomplements,annee,href,icon";
    }
    requestParameters.type = "q-fiche";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "fichefields", defaultFicheFields);
    Scrutari.Ajax.check(requestParameters, "motclefields", "mlibelles");
    Scrutari.Ajax.check(requestParameters, "q-mode", "intersection");
    Scrutari.Ajax.check(requestParameters, "origin", scrutariConfig.getOrigin());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    Scrutari.Ajax.check(requestParameters, "start", 1);
    Scrutari.Ajax.check(requestParameters, "limit", scrutariConfig.getPlageLength() * 2);
    Scrutari.Ajax.check(requestParameters, "starttype", "in_all");
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "ficheSearchResult", ficheSearchResultCallback, scrutariErrorCallback);
        }
    });
};

/*
 * requestParameters est obligatoire et doit contenir une propriété qid
 */
Scrutari.Ajax.loadExistingFicheSearchResult = function (existingFicheSearchResultCallback, scrutariConfig, requestParameters) {
    var defaultFicheFields = scrutariConfig.getFicheFields();
    if (!defaultFicheFields) {
        defaultFicheFields = "codecorpus,mtitre,msoustitre,mattrs_all,mcomplements,annee,href,icon";
    }
    requestParameters.type = "q-fiche";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "fichefields", defaultFicheFields);
    Scrutari.Ajax.check(requestParameters, "motclefields", "");
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    Scrutari.Ajax.check(requestParameters, "start", 1);
    Scrutari.Ajax.check(requestParameters, "limit", scrutariConfig.getPlageLength() * 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "ficheSearchResult", existingFicheSearchResultCallback);
        }
    });
};

/**
 * Fonction de retour après chargement d'une requête type=geojson. Le paramètre
 * de la requête est constituée des données complètes retournées par le serveur
 * (suivant le format JeoSon)
 * 
 * @callback Scrutari.Ajax~loadGeoJsonCallback
 * @param {Object} data
 */


/**
 * Charge des données au format GeoJson tel que défini dans http://www.scrutari.net/dokuwiki/serveurscrutari:json:type_geojson
 *  
 * @param {Scrutari.Ajax~loadGeoJsonCallback} geojsonCallback
 * @param {Scrutari.Config} scrutariConfig Configuration du moteur Scrutari
 * @param {Object} [requestParameters] Paramètres de la requête
 * @param {scrutariErrorCallback} [scrutariErrorCallback]
 * @returns {undefined}
 */
Scrutari.Ajax.loadGeoJson = function (geojsonCallback, scrutariConfig, requestParameters, scrutariErrorCallback) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "geojson";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "fichefields", "titre");
    Scrutari.Ajax.check(requestParameters, "motclefields", "");
    Scrutari.Ajax.check(requestParameters, "origin", scrutariConfig.getOrigin());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 1);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            if (data.hasOwnProperty("error")) {
                if (scrutariErrorCallback) {
                    scrutariErrorCallback(data.error);
                } else {
                    Scrutari.logError(data.error);
                }
            } else {
                Scrutari.Ajax.logWarnings(data);
                geojsonCallback(data);
            }
        }
    });
};


/*
 * Fonction invoquée lors du succès d'une requête Ajax, traite les messages d'erreur
 * (si scrutariErrorCallback n'est pas défini, envoie dans la console))
 * et les messages d'avertissement avant de faire appel à la fonction objectCallback en envoyant
 * le bon objet.
 */
Scrutari.Ajax.success = function(ajaxResult, objectName, objectCallback, scrutariErrorCallback) {
    if (ajaxResult.hasOwnProperty("error")) {
        if (scrutariErrorCallback) {
            scrutariErrorCallback(ajaxResult.error);
        } else {
            Scrutari.logError(ajaxResult.error);
        }
    } else {
        Scrutari.Ajax.logWarnings(ajaxResult);
        if (!ajaxResult.hasOwnProperty(objectName)) {
            $.error(objectName + " object is missing in json response");
        } else {
            objectCallback(ajaxResult[objectName]);
        }
    }
};

/*
 * Test la présence d'avertissements et les écrit dans le journal
 */
Scrutari.Ajax.logWarnings = function (ajaxResult) {
    if (ajaxResult.hasOwnProperty("warnings")) {
        warningsMessage = "Scrutari Request Warnings [";
        for(var i = 0; i < ajaxResult.warnings.length; i++) {
            if (i > 0) {
                warningsMessage += ";";
            }
            var warning = ajaxResult.warnings[i];
            warningsMessage += "key = ";
            warningsMessage += warning.key;
            warningsMessage += " | parameter = ";
            warningsMessage += warning.parameter;
            if (warning.hasOwnProperty("value")) {
                warningsMessage += " | value = ";
                warningsMessage += warning.value;
            }
        }
        warningsMessage += "]";
        Scrutari.log(warningsMessage);
    }
};

/*
 * Test la présence de name et définit sa valeur à defaultValue s'il n'est pas présent.
 */
Scrutari.Ajax.check = function (obj, name, defaultValue) {
    if (!obj.hasOwnProperty(name)) {
        obj[name] = defaultValue;
    }
};