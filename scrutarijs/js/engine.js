/*** r1612 ***/
Engine = function (scrutariConfig, locMap) {
    this.scrutariConfig = scrutariConfig;
    this.scrutariLoc = new Scrutari.Loc(locMap);
    this.withCorpus = false;
    this.scrutariMeta = null;
    this.historyNumber = 0;
    this.waiting = false;
    this.baseSort = "fiche-count";
    this.corpusSort = "fiche-count";
    this.mode = "";
    this.target = "_blank";
    this.initialQuery = "";
    this.initialQId = "";
    this.currentScrutariResult = null;
    this.changePlageCallbackFunction = null;
    this.hiddenList = new Array();
    this.bootstrapVersion = "3";
    this.permalinkPattern = null;
    this.templateFactoryArray = new Array();
};
Engine.prototype.getScrutariConfig = function () {
    return this.scrutariConfig;
};
Engine.prototype.loc = function (locKey) {
    return this.scrutariLoc.loc(locKey);
};
Engine.prototype.getScrutariLoc = function () {
    return this.scrutariLoc;
};
Engine.prototype.isWithCorpus = function () {
    return this.withCorpus;
};
Engine.prototype.setWithCorpus = function (withCorpus) {
    this.withCorpus = withCorpus;
};
Engine.prototype.isWithPermalink = function () {
    return (this.permalinkPattern != null);
};
Engine.prototype.setPermalinkPattern = function (permalinkPattern) {
    this.permalinkPattern = permalinkPattern;
};
Engine.prototype.toPermalink = function (qId) {
    if (this.permalinkPattern) {
        return this.scrutariConfig.getPermalinkUrl(qId, this.permalinkPattern);
    }
    return null;
};
Engine.prototype.getPermalinkPattern = function () {
    return permalinkPattern;
};
Engine.prototype.getScrutariMeta = function () {
    return this.scrutariMeta;
};
Engine.prototype.setScrutariMeta = function (scrutariMeta) {
    this.scrutariMeta = scrutariMeta;
};
Engine.prototype.getScrutariLoc = function () {
    return this.scrutariLoc;
};
Engine.prototype.newHistoryNumber = function () {
    this.historyNumber++;
    return this.historyNumber;
};
Engine.prototype.isWaiting = function () {
    return this.waiting;
};
Engine.prototype.setWaiting = function (bool) {
    this.waiting = bool;
};
Engine.prototype.getBaseSort = function () {
    return this.baseSort;
};
Engine.prototype.setBaseSort = function (baseSort) {
    this.baseSort = baseSort;
};
Engine.prototype.getCorpusSort = function () {
    return this.corpusSort;
};
Engine.prototype.setCorpusSort = function (corpusSort) {
    this.corpusSort = corpusSort;
};
Engine.prototype.getMode = function () {
    return this.mode;
};
Engine.prototype.setMode = function (mode) {
    this.mode = mode;
};
Engine.prototype.getTarget = function () {
    return this.target;
};
Engine.prototype.setTarget = function (target) {
    this.target = target;
};
Engine.prototype.getInitialQuery = function () {
    return this.initialQuery;
};
Engine.prototype.setInitialQuery = function (initialQuery) {
    this.initialQuery = initialQuery;
};
Engine.prototype.getInitialQId = function () {
    return this.initialQId;
};
Engine.prototype.setInitialQId = function (initialQId) {
    this.initialQId = initialQId;
};
Engine.prototype.getBootstrapVersion = function () {
    return this.bootstrapVersion;
};
Engine.prototype.setBootstrapVersion = function (bootstrapVersion) {
    this.bootstrapVersion = bootstrapVersion;
};
Engine.prototype.setTarget = function (target) {
    this.target = target;
};
Engine.prototype.hasCurrentScrutariResult = function () {
    return (this.currentScrutariResult != null);
};
Engine.prototype.getCurrentScrutariResult = function () {
    return this.currentScrutariResult;
};
Engine.prototype.setCurrentScrutariResult = function (scrutariResult) {
    this.currentScrutariResult = scrutariResult;
};
Engine.prototype.setChangePlageCallbackFunction = function (changePlageCallbackFunction) {
    this.changePlageCallbackFunction = changePlageCallbackFunction;
};
Engine.prototype.changePlageCallback = function (plageFicheArray, changePlageArguments) {
    if (this.changePlageCallbackFunction) {
        this.changePlageCallbackFunction(plageFicheArray, changePlageArguments);
    }
};
Engine.prototype.setHiddenList = function (hiddenArg) {
    if ($.type(hiddenArg) === "string") {
        this.hiddenList = hiddenArg.split(",");
    } else {
        this.hiddenList = hiddenArg;
    }
    for(var i = 0, length = this.hiddenList.length; i < length; i++) {
        this.hiddenList[i] = $.trim(this.hiddenList[i]);
    }
};
Engine.prototype.getHiddenList = function () {
    return this.hiddenList;
};
Engine.prototype.isHidden = function (elementName) {
    return $.inArray(elementName, this.hiddenList) > -1;
};
Engine.prototype.addTemplateFactory = function (templateFactory) {
    this.templateFactoryArray.push(templateFactory);
};
Engine.prototype.getTemplate = function (templateName, templateParameter) {
    var factoryLength = this.templateFactoryArray.length;
    if (factoryLength > 0) {
        for(var i = (factoryLength -1); i >= 0; i--) {
            var templateFactory = this.templateFactoryArray[i];
            var template = templateFactory(this, templateName, templateParameter);
            if (template) {
                return template;
            }
        }
    }
    switch(templateName) {
        case "base":
            return Engine.baseTemplate;
        case "category":
            return Engine.categoryTemplate;
        case "corpus":
            return Engine.corpusTemplate;
        case "lang":
            var scrutariMeta = this.scrutariMeta;
            return function (lang) {
                var code = lang.lang;
                var html = "";
                html += "<p>";
                html += "<label class='scrutari-Label'><input type='checkbox' name='lang' value='" + code + "'> ";
                var label = scrutariMeta.getLangLabel(code);
                if (label !== code) {
                    html += label;
                    html += " ";
                }
                html += "[" + code + "]";
                html += " (" + lang.fiche +")";
                html += "</label>";
                html += "</p>";
                return html;
            };
        case "pagination":
            return Scrutari.Utils.buildPaginationTemplate(templateParameter);
        case "fiche":
            return Scrutari.Utils.buildFicheTemplate(this.getScrutariMeta(), this.getScrutariLoc(), templateParameter, this.getTarget());
        default:
            return function () {
                return "Unknwon template: " + templateName;
            };
    }
};
Engine.baseTemplate = function (base) {
    var code = base.codebase;
    var html = "";
    html += "<p>";
    html += "<label class='scrutari-Label'><input type='checkbox' name='base' value='" + code + "'> ";
    if (base.baseicon) {
        html += "<img src='" + base.baseicon + "' alt='' class='scrutari-panel-Icon'> ";
    }
    html += Scrutari.escape(base.intitules.short);
    html += " (" + base.stats.fiche +")";
    html += "</label>";
    html += "</p>";
    return html;
};
Engine.categoryTemplate = function (category) {
    var name = category.name;
    var html = "";
    html += "<p>";
    html += "<label class='scrutari-Label'><input type='checkbox' name='category' value='" + name + "'> ";
    html +=  Scrutari.escape(category.title);
    html += " (" + category.stats.fiche +")";
    html += "</label>";
    html += "</p>";
    return html;
};
Engine.corpusTemplate = function (corpus) {
    var code = corpus.codecorpus;
    var html = "";
    html += "<p>";
    html += "<label class='scrutari-Label'><input type='checkbox' name='corpus' value='" + code + "'> ";
    html += Scrutari.escape(corpus.intitules.corpus);
    html += " (" + corpus.stats.fiche +")";
    html += "</label>";
    html += "</p>";
    return html;
};
Engine.Init = {};
Engine.Init.html = function (engine, jqArgument) {
    if  ((!Engine.html) || (!Engine.html.list) || (!Engine.html.list.length)) {
        Scrutari.log("Engine.html.list is undefined");
        return;
    }
    var _hide = function (key, text) {
        var key_start = "<!-- " + key + " -->";
        var key_end = "<!-- /" + key + " -->";
        var start_index = text.indexOf(key_start);
        var end_index = text.indexOf(key_end);
        if ((start_index > -1) && (end_index > -1) && (start_index < end_index)) {
            text = text.substring(0, start_index) + text.substring(end_index + key_end.length);
        }
        return text;
    }
    var html = "";
    for(var i= 0, len = Engine.html.list.length; i < len; i++) {
        var key = Engine.html.list[i];
        if (Engine.html.hasOwnProperty(key)) {
            html += Engine.html[key];
        }
    }
    var hiddenList = engine.getHiddenList();
    for(var i =0, length = hiddenList.length; i < length ; i++) {
        html = _hide(hiddenList[i], html);
    }
    if (engine.isWithCorpus()) {
         html = _hide("base-panel", html);
    } else {
         html = _hide("corpus-panel", html);
    }
    if (!engine.isWithPermalink()) {
        html = _hide("share", html);
    }
    html = html.replace(/_ [-_\.a-z0-9]+/g, function(match) {
        return engine.loc(match);
    });
    $(jqArgument).html(html);
};
Engine.Init.mainTitle = function (engine) {
    var html = "";
    if ($.isFunction(engine)) {
        html = engine.call(this);
    } else if (typeof engine === "string") {
        html = engine;
    } else {
        html += engine.loc('_ title_main');
        html += " – [";
        html += engine.getScrutariConfig().getName();
        html += "]";
    }
    $("#mainTitle").html(html);
};
Engine.Init.custom = function (engine) {
};
Engine.Init.forms = function (engine) {
    $("#mainsearchForm").submit(function () {
        var q = $(this).find("input[name='q']").val();
        q = $.trim(q);
        if (q.length > 0) {
            var requestParameters = Engine.Ui.checkRequestParameters(engine);
            requestParameters["log"] = "all";
            requestParameters["q"] = q;
            var _mainsearchScrutariResultCallback = function (scrutariResult) {
                Engine.Query.scrutariResult(engine, scrutariResult, "mainsearch");
                $("#loadingModal").modal('hide');
                $parametersDisplayButton = $('#parametersDisplayButton');
                if ($parametersDisplayButton.data("scrutariState") === "on") {
                    $parametersDisplayButton.click();
                }
            };
            var _mainScrutariErrorCallback = function (error) {
                Engine.Query.scrutariError(engine, error, "mainsearch");
                $("#loadingModal").modal('hide');
            };
            $("#loadingModal").modal('show');
            Scrutari.Result.newSearch(_mainsearchScrutariResultCallback, engine.getScrutariConfig(), requestParameters, _mainScrutariErrorCallback);
        }
        return false;
    });
    $("#subsearchForm").submit(function () {
        var q = $(this).find("input[name='q']").val();
        q = $.trim(q);
        if ((q.length > 0) && (engine.hasCurrentScrutariResult())) {
            var requestParameters = Engine.Ui.checkRequestParameters(engine);
            requestParameters["q"] = q;
            requestParameters["flt-qid"] = engine.getCurrentScrutariResult().getQId();
            var _subsearchScrutariResultCallback = function (scrutariResult) {
                Engine.Query.scrutariResult(engine, scrutariResult, "subsearch");
                $("#loadingModal").modal('hide');
            };
            var _subsearchScrutariErrorCallback = function (error) {
                Engine.Query.scrutariError(engine, error, "subsearch");
                $("#loadingModal").modal('hide');
            };
            $("#loadingModal").modal('show');
            Scrutari.Result.newSearch(_subsearchScrutariResultCallback, engine.getScrutariConfig(), requestParameters, _subsearchScrutariErrorCallback);
        }
        return false;
    });
};
Engine.Init.buttons = function (engine) {
    $('#langEnableButton').click(function () {
        var $this = $(this);
        var state = Scrutari.Utils.toggle($this, "scrutariState");
        Scrutari.Utils.toggle.disabled("#langCheckAllButton, #langUncheckAllButton", state);
        Scrutari.Utils.toggle.classes(".scrutari-column-Lang", state, "", "scrutari-Disabled");
        Scrutari.Utils.toggle.text($this.children("span"), "scrutariAlternate");
        var $langFilterLabel = Scrutari.Utils.toggle.text("#langFilterLabel", "scrutariAlternate");
        Scrutari.Utils.toggle.classes($langFilterLabel, state, "scrutari-panel-Active", "scrutari-Disabled");
        Engine.Ui.filterChange(engine);
    });
    $('#langCheckAllButton').click(function () {
        Scrutari.Utils.check("input[name='lang']");
        Engine.Ui.filterChange(engine);
    });
    $('#langUncheckAllButton').click(function () {
        Scrutari.Utils.uncheck("input[name='lang']");
        Engine.Ui.filterChange(engine);
    });
    $('#categoryEnableButton').click(function () {
        var $this = $(this);
        var state = Scrutari.Utils.toggle($this, "scrutariState");
        Scrutari.Utils.toggle.disabled("#categoryCheckAllButton, #categoryUncheckAllButton", state);
        Scrutari.Utils.toggle.classes(".scrutari-column-Category", state, "", "scrutari-Disabled");
        Scrutari.Utils.toggle.text($this.children("span"), "scrutariAlternate");
        var $categoryFilterLabel = Scrutari.Utils.toggle.text("#categoryFilterLabel", "scrutariAlternate");
        Scrutari.Utils.toggle.classes($categoryFilterLabel, state, "scrutari-panel-Active", "scrutari-Disabled");
        Engine.Ui.filterChange(engine);
    });
    $('#categoryCheckAllButton').click(function () {
        Scrutari.Utils.check("input[name='category']");
        Engine.Ui.filterChange(engine);
    });
    $('#categoryUncheckAllButton').click(function () {
        Scrutari.Utils.uncheck("input[name='category']");
        Engine.Ui.filterChange(engine);
    });
    if (engine.isWithCorpus()) {
        $('#corpusEnableButton').click(function () {
            var $this = $(this);
            var state = Scrutari.Utils.toggle($this, "scrutariState");
            Scrutari.Utils.toggle.disabled("#corpusCheckAllButton, #corpusUncheckAllButton", state);
            Scrutari.Utils.toggle.classes(".scrutari-column-Corpus", state, "", "scrutari-Disabled");
            Scrutari.Utils.toggle.text($this.children("span"), "scrutariAlternate");
            var $corpusFilterLabel = Scrutari.Utils.toggle.text("#corpusFilterLabel", "scrutariAlternate");
            Scrutari.Utils.toggle.classes($corpusFilterLabel, state, "scrutari-panel-Active", "scrutari-Disabled");
            Engine.Ui.filterChange(engine);
        });
        $('#corpusCheckAllButton').click(function () {
            Scrutari.Utils.check("input[name='corpus']");
            Engine.Ui.filterChange(engine);
        });
        $('#corpusUncheckAllButton').click(function () {
            Scrutari.Utils.uncheck("input[name='corpus']");
            Engine.Ui.filterChange(engine);
        });
    } else {
        $('#baseEnableButton').click(function () {
            var $this = $(this);
            var state = Scrutari.Utils.toggle($this, "scrutariState");
            Scrutari.Utils.toggle.disabled("#baseCheckAllButton, #baseUncheckAllButton", state);
            Scrutari.Utils.toggle.classes(".scrutari-column-Base", state, "", "scrutari-Disabled");
            Scrutari.Utils.toggle.text($this.children("span"), "scrutariAlternate");
            var $baseFilterLabel = Scrutari.Utils.toggle.text("#baseFilterLabel", "scrutariAlternate");
            Scrutari.Utils.toggle.classes($baseFilterLabel, state, "scrutari-panel-Active", "scrutari-Disabled");
            Engine.Ui.filterChange(engine);
        });
        $('#baseCheckAllButton').click(function () {
            Scrutari.Utils.check("input[name='base']");
            Engine.Ui.filterChange(engine);
        });
        $('#baseUncheckAllButton').click(function () {
            Scrutari.Utils.uncheck("input[name='base']");
            Engine.Ui.filterChange(engine);
        });
    }
    $('#parametersDisplayButton').click(function () {
        var state = Scrutari.Utils.toggle($(this), "scrutariState");
        if (state === 'on') {
            Engine.Ui.show('#parametersArea');
        } else {
            Engine.Ui.hide('#parametersArea');
        }
    });
    $('#modeHelpButton').click(function () {
        $modeHelpModal = $("#modeHelpModal");
        $modalBody = $modeHelpModal.find(".modal-body");
        if ($modalBody.children().length === 0) {
            $modalBody.html(engine.loc("_ help_mode.html"));
        }
        $modeHelpModal.modal('show');
    });
    $('#ponderationHelpButton').click(function () {
        $ponderationHelpModal = $("#ponderationHelpModal");
        $modalBody = $ponderationHelpModal.find(".modal-body");
        if ($modalBody.children().length === 0) {
            $modalBody.html(engine.loc("_ help_ponderation.html"));
        }
        $ponderationHelpModal.modal('show');
    });
    $('#periodeHelpButton').click(function () {
        $periodeHelpModal = $("#periodeHelpModal");
        $modalBody = $periodeHelpModal.find(".modal-body");
        if ($modalBody.children().length === 0) {
            $modalBody.html(engine.loc("_ help_periode.html"));
        }
        $periodeHelpModal.modal('show');
    });
    $("input[name='q-mode']").click(function () {
        if (this.value === 'operation') {
            Scrutari.Utils.disable("input[name='wildchar']");
        } else {
            Scrutari.Utils.enable("input[name='wildchar']");
        }
    });
};
Engine.Init.scrutariMeta = function (engine, scrutariMeta) {
    engine.setScrutariMeta(scrutariMeta);
    Engine.Init.forms(engine);
    Engine.Init.buttons(engine);
    Engine.Ui.setCount("#globalFicheCount", scrutariMeta.getGlobalFicheCount(), engine.getScrutariConfig().getLangUi());
    Engine.Ui.show("#globalFicheCount");
    Engine.Ui.show("#parametersDisplayButton");
    var langArray = scrutariMeta.getLangArray(Scrutari.Utils.sortLangByFicheCountDesc);
    if ((langArray.length > 1) && (Scrutari.exists("#langPanel"))) {
        Scrutari.Utils.divideIntoColumns(langArray, ".scrutari-column-Lang", engine.getTemplate("lang"));
            Engine.Ui.show("#langPanel");
            $("input[name='lang']").click(function () {
                var button = $('#langEnableButton');
                if (Scrutari.Utils.toggle.getState(button, 'scrutariState') === 'off') {
                    button.click();
                    $(this).focus();
                } else {
                    Engine.Ui.filterChange(engine);
                }
            });
    }
    if ((scrutariMeta.withCategory()) && (Scrutari.exists("#categoryPanel"))) {
        var categoryArray = scrutariMeta.getCategoryArray(Scrutari.Utils.sortCategoryByRankDesc);
        Scrutari.Utils.divideIntoColumns(categoryArray, ".scrutari-column-Category", engine.getTemplate("category"));
        Engine.Ui.show("#categoryPanel");
        $("input[name='category']").click(function () {
            var button = $('#categoryEnableButton');
            if (Scrutari.Utils.toggle.getState(button, 'scrutariState') === 'off') {
                button.click();
                $(this).focus();
            } else {
                Engine.Ui.filterChange(engine);
            }
        });
    }
    if (engine.isWithCorpus()) {
        if (Scrutari.exists("#corpusPanel")) {
            var corpusSortFunction = null;
            if (engine.getCorpusSort() === "fiche-count") {
                corpusSortFunction = Scrutari.Utils.sortCorpusByFicheCountDesc;
            }
            var corpusArray =  scrutariMeta.getCorpusArray(corpusSortFunction);
            if (corpusArray.length > 1) {
                Scrutari.Utils.divideIntoColumns(corpusArray, ".scrutari-column-Corpus", engine.getTemplate("corpus"));
                Engine.Ui.show("#corpusPanel");
                $("input[name='corpus']").click(function () {
                    var button = $('#corpusEnableButton');
                    if (Scrutari.Utils.toggle.getState(button, 'scrutariState') === 'off') {
                        button.click();
                        $(this).focus();
                    } else {
                        Engine.Ui.filterChange(engine);
                    }
                });
            }
        }
    } else {
        if (Scrutari.exists("#basePanel")) {
            var baseSortFunction = null;
            if (engine.getBaseSort() === "fiche-count") {
                baseSortFunction = Scrutari.Utils.sortBaseByFicheCountDesc;
            }
            var baseArray =  scrutariMeta.getBaseArray(baseSortFunction);
            if (baseArray.length > 1) {
                Scrutari.Utils.divideIntoColumns(baseArray, ".scrutari-column-Base", engine.getTemplate("base"));
                Engine.Ui.show("#basePanel");
                $("input[name='base']").click(function () {
                    var button = $('#baseEnableButton');
                    if (Scrutari.Utils.toggle.getState(button, 'scrutariState') === 'off') {
                        button.click();
                        $(this).focus();
                    } else {
                        Engine.Ui.filterChange(engine);
                    }
                });
            }
        }
    }
    var initialQuery = engine.getInitialQuery();
    var initialQId = engine.getInitialQId();
    if (initialQuery.length > 0) {
        $mainSearchForm = $("#mainsearchForm");
        if (Scrutari.exists($mainSearchForm)) {
            $mainSearchForm.find("input[name='q']").val(initialQuery);
            $mainSearchForm.submit();
        }
    } else if ((initialQId) && (initialQId.length > 0)) {
        var requestParameters = Engine.Ui.checkRequestParameters(engine);
        requestParameters["qid"] = initialQId;
        var _mainsearchScrutariResultCallback = function (scrutariResult) {
            Engine.Query.scrutariResult(engine, scrutariResult, "mainsearch");
            $("#loadingModal").modal('hide');
            $parametersDisplayButton = $('#parametersDisplayButton');
            if ($parametersDisplayButton.data("scrutariState") === "on") {
                $parametersDisplayButton.click();
            }
        };
        var _mainScrutariErrorCallback = function (error) {
            Engine.Query.scrutariError(engine, error, "mainsearch");
            $("#loadingModal").modal('hide');
        };
        $("#loadingModal").modal('show');
        Scrutari.Result.newSearch(_mainsearchScrutariResultCallback, engine.getScrutariConfig(), requestParameters, _mainScrutariErrorCallback);
    }
};
Engine.Query = {};
Engine.Query.scrutariResult = function (engine, scrutariResult, searchOrigin) {
    var bootstrapV2 = (engine.getBootstrapVersion().lastIndexOf("2", 0) === 0);
    var _getPaginationHtml = function (bottomPagination, categoryName) {
        var paginationHtml = "";
        if (bootstrapV2) {
            paginationHtml += "<div class='pagination'><ul class='scrutari-result-Pagination";
            if (category) {
                paginationHtml += " scrutari-Pagination_" + categoryName;
            }
            paginationHtml +=  "'></ul></div>";
        } else {
            paginationHtml += "<ul class='pagination scrutari-result-Pagination";
            if (category) {
                paginationHtml += " scrutari-Pagination_" + categoryName;
            }
            if (bottomPagination) {
                paginationHtml += " scrutari-BottomPagination";
            }
            paginationHtml +=  "'></ul>";
        }
        return paginationHtml;
    };
    Engine.Ui.show(".scrutari-HiddenAtStart");
    var ficheCount = scrutariResult.getFicheCount();
    Engine.Ui.setCount("#resultFicheCount", ficheCount, engine.getScrutariConfig().getLangUi());
    var $ficheDisplayBlock = $("#ficheDisplayBlock");
    $ficheDisplayBlock.empty();
    if (searchOrigin === "mainsearch") {
        engine.setCurrentScrutariResult(scrutariResult);
        Engine.Query.addToHistory(engine, scrutariResult);
    } else if (searchOrigin === "subsearch") {
        var subsearchText =  "+ " + scrutariResult.getFormatedSearchSequence(engine.getScrutariLoc()) + " = " + scrutariResult.getFicheCount();
        $(".scrutari-history-Active").find(".scrutari-CurrentSubsearchSequence").text(subsearchText);
    }
    if (ficheCount === 0) {
        Engine.Ui.hide(".scrutari-HiddenIfEmpty");
        $ficheDisplayBlock.html("<em>" + engine.loc("_ result_none")+ "</em>");
        return;
    }
    Engine.Ui.show(".scrutari-HiddenIfEmpty");
    var qId = scrutariResult.getQId();
    var permalink = engine.toPermalink(qId);
    if (permalink) {
        $("#permalinkLink").attr("href", permalink);
    }
    $("#odsLink").attr("href", engine.getScrutariConfig().getOdsUrl(qId));
    $("#atomLink").attr("href", engine.getScrutariConfig().getAtomUrl(qId));
    if (scrutariResult.getFicheGroupType() === 'category') {
        var categoryCount = scrutariResult.getCategoryCount();
        var tabHtml = '<ul class="nav nav-tabs" role="tablist">';
        var contentHtml = "<div class='tab-content'>";
        for(var i = 0; i < categoryCount; i++) {
            var category = scrutariResult.getCategory(i);
            tabHtml += "<li";
            contentHtml += "<div class='tab-pane";
            if (i === 0) {
                tabHtml += " class='active'";
                contentHtml += " active";
            }
            tabHtml += "><a class='scrutari-Onglet' role='tab' data-toggle='tab' href='#categoryTabContent_"
                + category.name
                + "'>"
                + Scrutari.escape(category.title)
                + " ("
                + scrutariResult.getCategoryFicheCount(i)
                + ")"
                + "</a></li>";
            contentHtml += "' id='categoryTabContent_" + category.name + "'>";
            contentHtml += _getPaginationHtml(false, category.name);
            contentHtml += "<div id='categoryFiches_" + category.name + "'></div>";
            contentHtml += _getPaginationHtml(true, category.name);
            contentHtml += "</div>";
        }
        tabHtml += "</ul>";
        contentHtml += "</div>";
        $ficheDisplayBlock.html(tabHtml + contentHtml);
        for(var i = 0; i < categoryCount; i++) {
            var category = scrutariResult.getCategory(i);
            Engine.Query.categoryChangePlage(engine, category.name, scrutariResult, engine.getScrutariConfig().getPlageLength(), 1);
        }
    } else {
        $ficheDisplayBlock.html(_getPaginationHtml(false, null) + "<div id='noneFiches'></div>" + _getPaginationHtml(true, null));
        Engine.Query.noneChangePlage(engine, scrutariResult, engine.getScrutariConfig().getPlageLength(), 1);
    }
};
Engine.Query.scrutariError = function (engine, error, searchOrigin) {
    if (error.parameter !== "q") {
        Scrutari.logError(error);
        return;
    }
    var title =  engine.loc(error.key);
    if (title !== error.key) {
        var alertMessage = title;
        if (error.hasOwnProperty("array"))  {
            alertMessage += engine.loc('_ scrutari-colon');
            for(var i = 0; i < error.array.length; i++) {
                alertMessage += "\n";
                var obj = error.array[i];
                alertMessage += "- ";
                alertMessage += engine.loc(obj.key);
                if (obj.hasOwnProperty("value")) {
                    alertMessage += engine.loc('_ scrutari-colon');
                    alertMessage += " ";
                    alertMessage += obj.value;
                }
            }
        }
        alert(alertMessage);
    } else {
        Scrutari.logError(error);
    }
};
Engine.Query.noneChangePlage = function (engine, scrutariResult, plageLength, plageNumber) {
    var html = "";
    if (!scrutariResult.isNonePlageLoaded(plageLength, plageNumber)) {
        if (engine.isWaiting()) {
            return;
        }
        $("#noneFiches").html("<span class='scrutari-icon-Loader'></span> " + engine.loc('_ loading_plage'));
        var _plageCallback = function () {
            engine.setWaiting(false);
            Engine.Query.noneChangePlage(engine, scrutariResult, plageLength, plageNumber);
        };
        engine.setWaiting(true);
        scrutariResult.loadNonePlage(_plageCallback, engine.getScrutariConfig(), plageLength, plageNumber);
        return;
    }
    var plageFicheArray = scrutariResult.selectNoneFicheArray(plageLength, plageNumber);
    var ficheTemplate =  engine.getTemplate("fiche", scrutariResult);
    for(var i = 0; i < plageFicheArray.length; i++) {
        html += ficheTemplate(plageFicheArray[i]);
    }
    $("#noneFiches").html(html);
    var paginationIdPrefix = "pagination_";
    Scrutari.Utils.checkPagination(scrutariResult.getFicheCount(), plageLength, plageNumber, ".scrutari-Pagination", engine.getTemplate("pagination", paginationIdPrefix));
    $(".scrutari-Pagination a").click(function () {
        var idx = this.href.indexOf("#" + paginationIdPrefix);
        var newPlageNumber = parseInt(this.href.substring(idx + 1 + paginationIdPrefix.length));
        Engine.Query.noneChangePlage(engine, scrutariResult, plageLength, newPlageNumber);
        return false;
    });
    $(".scrutari-Pagination.scrutari-BottomPagination a").click(function () {
        Engine.Ui.scrollToResult();
    });
    engine.changePlageCallback(plageFicheArray, {engine: engine, scrutariResult: scrutariResult,  plageLength:plageLength, plageNumber:plageNumber});
};
Engine.Query.categoryChangePlage = function (engine, categoryName, scrutariResult, plageLength, plageNumber) {
    var html = "";
    if (!scrutariResult.isCategoryPlageLoaded(categoryName, plageLength, plageNumber)) {
        $("#categoryFiches_" + categoryName).html("<span class='scrutari-icon-Loader'></span> " + engine.loc('_ loading_plage'));
        var _plageCallback = function () {
            engine.setWaiting(false);
            Engine.Query.categoryChangePlage(engine, categoryName, scrutariResult, plageLength, plageNumber);
        };
        if (engine.isWaiting()) {
            return;
        }
        engine.setWaiting(true);
        scrutariResult.loadCategoryPlage(_plageCallback, engine.getScrutariConfig(), categoryName, plageLength, plageNumber);
        return;
    }
    var plageFicheArray = scrutariResult.selectCategoryFicheArray(categoryName, plageLength, plageNumber);
    var ficheTemplate =  engine.getTemplate("fiche", scrutariResult);
    for(var i = 0; i < plageFicheArray.length; i++) {
        html += ficheTemplate(plageFicheArray[i]);
    }
    $("#categoryFiches_" + categoryName).html(html);
    var paginationIdPrefix = "pagination_" + categoryName + "_";
    Scrutari.Utils.checkPagination(scrutariResult.getCategoryFicheCountbyName(categoryName), plageLength, plageNumber, ".scrutari-Pagination_" + categoryName, engine.getTemplate("pagination", paginationIdPrefix));
    $(".scrutari-Pagination_" + categoryName + " a").click(function () {
        var idx = this.href.indexOf("#" + paginationIdPrefix);
        var newPlageNumber = parseInt(this.href.substring(idx + 1 + paginationIdPrefix.length));
        Engine.Query.categoryChangePlage(engine, categoryName, scrutariResult, plageLength, newPlageNumber);
        return false;
    });
    $(".scrutari-Pagination_" + categoryName + ".scrutari-BottomPagination a").click(function () {
        Engine.Ui.scrollToResult();
    });
    engine.changePlageCallback(plageFicheArray, {engine: engine, categoryName: categoryName, scrutariResult: scrutariResult,  plageLength:plageLength, plageNumber:plageNumber});
};
Engine.Query.addToHistory = function (engine, scrutariResult) {
    var $resultHistoryList = $("#resultHistoryList");
    if (!Scrutari.exists($resultHistoryList)) {
        Engine.Query.updateSubsearch(engine, scrutariResult);
        return;
    } else {
        Engine.Query.activateResult(engine, scrutariResult);
    }
    var historyId = "searchHistory_" + engine.newHistoryNumber();
    var withFiche = (scrutariResult.getFicheCount() > 0);
    var html = "";
    html += "<button type='button' class='close hidden' title='" + engine.loc('_ scrutari-remove') + "'><span aria-hidden='true'>&times;</span><span class='sr-only'>" + engine.loc('_ scrutari-remove') + "</span></button>";
    if (withFiche) {
        html += "<a href='#" + historyId + "'>";
    }
    html += scrutariResult.getFormatedSearchSequence(engine.getScrutariLoc());
    html += " (" + scrutariResult.getFicheCount() + ")";
    if (withFiche) {
        html += "</a>";
    }
    html += "<br/><span class='scrutari-CurrentSubsearchSequence'></span>";
    var $new = $("<div/>").html(html).attr("id", historyId).addClass("scrutari-history-Block").addClass("scrutari-history-Active");
    var _close = function() {
        $("#" + historyId).remove();
    }
    var _load = function () {
        var $historyId = $("#" + historyId);
        var historyScrutariResult = $historyId.data("scrutariResult");
        engine.setCurrentScrutariResult(historyScrutariResult);
        Engine.Query.activateResult(engine, historyScrutariResult);
        $historyId.addClass("scrutari-history-Active");
        Engine.Ui.hide($historyId.find("button"));
        Engine.Query.scrutariResult(engine, historyScrutariResult);
        return false;
    }
    $new.children("button").click(_close);
    $new.children("a").click(_load);
    $new.data("scrutariResult", scrutariResult);
    $resultHistoryList.prepend($new);
};
Engine.Query.activateResult = function (engine, scrutariResult) {
    var $elements = $(".scrutari-history-Active");
    $elements.removeClass("scrutari-history-Active");
    $elements.find(".scrutari-CurrentSubsearchSequence").empty();
    Engine.Ui.show($elements.find("button"));
    Engine.Query.updateSubsearch(engine, scrutariResult);
};
Engine.Query.updateSubsearch = function (engine, scrutariResult) {
    $(".scrutari-CurrentSearchSequence").text(scrutariResult.getFormatedSearchSequence(engine.getScrutariLoc()) + " (" + scrutariResult.getFicheCount() + ")");
};
Engine.Ui = {};
Engine.Ui.show = function (jqArgument) {
    var $elements = Scrutari.convert(jqArgument);
    return $elements.removeClass("hidden");
};
Engine.Ui.hide = function (jqArgument) {
    var $elements = Scrutari.convert(jqArgument);
    return $elements.addClass("hidden");
};
Engine.Ui.scrollToResult = function () {
    $(window).scrollTop($("#resultArea").offset().top);
};
Engine.Ui.setCount = function (parentJqArgument, count, locale) {
    var $parent = Scrutari.convert(parentJqArgument);
    var $elements = $parent.find(".scrutari-stats-Count");
    var html = '';
    if (Number.prototype.toLocaleString) {
        html += count.toLocaleString(locale);
    } else {
        html += count;
    }
    $elements.html(html);
    return $elements;
};
Engine.Ui.filterChange = function (engine) {
    var scrutariFilter = Engine.Ui.checkFilter(engine);
    var scrutariMeta = engine.getScrutariMeta();
    var filterFicheCount = scrutariFilter.getFilterFicheCount(scrutariMeta);
    var globalFicheCount = scrutariMeta.getGlobalFicheCount();
    var $element = Engine.Ui.setCount("#filterFicheCount", filterFicheCount, engine.getScrutariConfig().getLangUi());
    if (filterFicheCount === globalFicheCount) {
        $element.removeClass("scrutari-stats-Filter").removeClass("scrutari-stats-None");
        Engine.Ui.hide("#filterFicheCount");
    } else if (filterFicheCount === 0) {
        $element.removeClass("scrutari-stats-Filter").addClass("scrutari-stats-None");
        Engine.Ui.show("#filterFicheCount");
    } else {
        $element.addClass("scrutari-stats-Filter").removeClass("scrutari-stats-None");
        Engine.Ui.show("#filterFicheCount");
    }
};
Engine.Ui.checkFilter = function (engine) {
    var _testEnable = function (selector) {
        return ($(selector).data("scrutariState") === "on");
    }
    var scrutariFilter = new Scrutari.Filter();
    if (engine.isWithCorpus()) {
        scrutariFilter.setWithCorpus(_testEnable("#corpusEnableButton"));
    } else {
        scrutariFilter.setWithBase(_testEnable("#baseEnableButton"));
    }
    scrutariFilter.setWithLang(_testEnable("#langEnableButton"));
    scrutariFilter.setWithCategory(_testEnable("#categoryEnableButton"));
    var $baseElements = $("input[name='base']:checked");
    for(var i = 0; i < $baseElements.length; i++) {
        scrutariFilter.addBase(parseInt($baseElements[i].value));
    }
    var $langElements = $("input[name='lang']:checked");
    for(var i = 0; i < $langElements.length; i++) {
        scrutariFilter.addLang($langElements[i].value);
    }
    var $categoryElements = $("input[name='category']:checked");
    for(var i = 0; i < $categoryElements.length; i++) {
        scrutariFilter.addCategory($categoryElements[i].value);
    }
    var $corpusElements = $("input[name='corpus']:checked");
    for(var i = 0; i < $corpusElements.length; i++) {
        scrutariFilter.addCorpus(parseInt($corpusElements[i].value));
    }
    return scrutariFilter;
};
Engine.Ui.checkRequestParameters = function (engine) {
    var scrutariFilter = Engine.Ui.checkFilter(engine);
    var requestParameters = new Object();
    scrutariFilter.checkRequestParameters(engine.getScrutariMeta(), requestParameters);
    requestParameters["q-mode"] = $("input[name='q-mode']:checked").val();
    var ponderation = $("input[name='ponderation']:checked").val();
    if (ponderation === 'date') {
        requestParameters.ponderation = '15,80,5';
    }
    var periode = $.trim($("input[name='periode']").val());
    if (periode) {
        requestParameters["flt-date"] = periode;
    }
    if (Scrutari.exists("input[name='wildchar']:checked")) {
        requestParameters.wildchar = "end";
    } else {
        requestParameters.wildchar = "none";
    }
    return requestParameters;
};
Engine.FrameMode = {};
Engine.FrameMode.is = function (engine) {
    return (engine.getMode() === "frame");
};
Engine.FrameMode.setCurrentFicheBlock = function (code, scroll) {
    $(".scrutari-fiche-CurrentBlock").removeClass("scrutari-fiche-CurrentBlock");
    var $ficheBlock = $("#ficheBlock_" + code);
    $ficheBlock.addClass("scrutari-fiche-CurrentBlock");
    if (scroll) {
        var top = $ficheBlock.offset().top;
        var $window =  $(window);
        var windowTop = $window.scrollTop();
        var windowBottom = windowTop + $window.height();
        if ((top < windowTop) || (top > (windowBottom - 20))) {
            if ($window.height() > 300) {
                top = top - 150;
            }
            $window.scrollTop(top);
        }
    }
};
Engine.FrameMode.clear = function () {
    window.parent.frames["Navigation"].clear();
};
Engine.FrameMode.changePlageCallback = function (plageFicheArray, changePlageArguments) {
    var _linkClick = function () {
        var idx = this.id.indexOf('_');
        var code = this.id.substring(idx + 1);
        Engine.FrameMode.setCurrentFicheBlock(code, false);
        window.parent.frames["Navigation"].setCurrentFiche(code, plageFicheArray);
    };
    Engine.FrameMode.clear();
    if (changePlageArguments.categoryName) {
        $("#categoryFiches_" + changePlageArguments.categoryName + " a.scrutari-fiche-Link").click(_linkClick);
    } else {
        $("a.scrutari-fiche-Link").click(_linkClick);
    }
};