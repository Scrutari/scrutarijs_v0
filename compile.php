<?php
/***************************************************************
 * ScutariJs
 * Copyright (c) 2014 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */
 
 /*Doit être invoqué en ligne de commande pour des questions de droit
 et doit comporter un argument qui est le texte indiquant la version
 de la compilation (typiquement le numéro de révision)
 Exemple : php compile.php r1161
 */
 
 if ($_SERVER['argc'] < 2) {
    echo "La version de la compilation n'est pas indiquée !\n";
    echo "Exemple : php compile r1161 pour indiquer qu'il s'agit de la révision r1161.\n";
    exit(0);
 }
 
 $versionHeader ="/*** ".$_SERVER['argv'][1]." ***/\n";

 require_once("l10n/locparser.php");
 

$scrutariCompil = compileDirectory("scrutarijs/js/lib/scrutari/");
$scrutariCompilFile = fopen("scrutarijs/js/scrutari.js", "w");
fwrite($scrutariCompilFile, $versionHeader);
fwrite($scrutariCompilFile, $scrutariCompil);
fclose($scrutariCompilFile);

$engineCompil = compileDirectory("scrutarijs/js/lib/engine/");
$engineCompilFile = fopen("scrutarijs/js/engine.js", "w");
fwrite($engineCompilFile, $versionHeader);
fwrite($engineCompilFile, $engineCompil);
fclose($engineCompilFile);

$compilation = $scrutariCompil."\n".$engineCompil."\n";
$compilation .= getEngineHtml("scrutarijs/js/lib/engine/html/bootstrap3/");
$compilationFile = fopen("scrutarijs/js/scrutarijs.js", "w");
fwrite($compilationFile, $versionHeader);
fwrite($compilationFile, $compilation);
fclose($compilationFile);

$GLOBALS['loc'] = array();
$l10nDir = "l10n/";
$files = scandir($l10nDir);
$count = count($files);
for($i = 0; $i < $count; $i++) {
    $langDir = $files[$i];
    if (preg_match('/^[-a-zA-Z_]+$/', $langDir)) {
        if (is_dir($l10nDir.$langDir)) {
            scanLang($langDir, $l10nDir.$langDir);
        }
    }
}

foreach($GLOBALS['loc'] as $lang => $locArray) {
    $file = fopen("scrutarijs/js/l10n/".$lang.".js", "w");
    fwrite($file, "Engine.l10n = {\n");
    $next = false;
    foreach($locArray as $key => $value) {
        if ($key === 'empty') {
            continue;
        }
        if ($next) {
            fwrite($file, ",\n");
        } else {
            $next = true;
        }
        fwrite($file, '"'.$key.'":"'.escapeQuote($value).'"');
    }
    fwrite($file, "\n};");
}

function scanLang($lang, $langPath) {
    $files = scandir($langPath);
    $count = count($files);
    $GLOBALS['loc'][$lang] = array();
    for($i = 0; $i < $count; $i++) {
        $fileName = $files[$i];
        if ($fileName == "loc.ini") {
            $locArray = parseLocArray(file($langPath."/loc.ini"));
            foreach($locArray as $key => $value) {
                $GLOBALS['loc'][$lang][$key] = $value;
            }
            break;
        }
    }
    for($i = 0; $i < $count; $i++) {
        $fileName = $files[$i];
        if (strpos($fileName, ".html") > 0) {
            $html = file_get_contents($langPath."/".$fileName);
            $html = preg_replace("/\s/", " ", $html);
            $GLOBALS['loc'][$lang]["_ ".$fileName] = $html;
        }
    }
}

function escapeQuote($value) {
    $value = str_replace('"', '\"', $value);
    return $value;
}

function compileDirectory($dirPath) {
    $list = file($dirPath."list.txt");
    $compilation = "";
    $count = count($list);
    for($i = 0; $i < $count; $i++) {
        $jsfile = trim($list[$i]);
        if (strlen($jsfile) > 0) {
            $compilation .= file_get_contents($dirPath.$jsfile)."\n";
        }
    }
    $compilation = preg_replace('!/\*.*?\*/!s','',$compilation);
    $compilation = preg_replace('/\n\s*\n/', "\n", $compilation);
    $compilation = trim($compilation);
    return $compilation;
}

 function getEngineHtml($dirPath) {
    $list = file($dirPath."list.txt");
    $count = count($list);
    $jsString = "Engine.html = {\nlist:[";
    $next = false;
    for($i = 0; $i < $count; $i++) {
        $key = trim($list[$i]);
        if (strlen($key) > 0) {
            if ($next) {
                $jsString .= ',';
            } else {
                $next = true;
            }
            $jsString .= '"'.$key.'"';
        }
    }
    $jsString .= "]";
    for($i = 0; $i < $count; $i++) {
        $key = trim($list[$i]);
        if (strlen($key) > 0) {
            $html = file_get_contents($dirPath.$key.".html");
            $html = preg_replace("/\s+/", " ", $html);
            $jsString .=  ",\n".$key.":\"";
            $jsString .= escapeQuote($html);
            $jsString .= "\"";
        }
    }
    $jsString .= "\n};\n";
    return $jsString;
 } 

 
?>