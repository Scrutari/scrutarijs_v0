<?php
/***************************************************************
 * ScutariJs
 * Copyright (c) 2014-2017 Vincent Calame - Exemole
 * Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
 */
$GLOBALS['scrutari'] = array();
$GLOBALS['scrutari']['engine'] = array();
require_once('../scrutarijs-conf.php');
$GLOBALS['scrutari']['loc'] = array();
require_once("l10n/locparser.php");

initLang();
initTarget();
initPage();


switch($GLOBALS['scrutari']['page']) {
    case 'engine':
        initEngineName();
        initEngineOptions();
        initOrigin();
        initMode();
        initHide();
        initQuery();
        initDev();
        include('pages/engine.php');
        break;
    case 'frame':
        initEngineName();
        initSupplementaryParameters();
        initWidth();
        include('pages/frame.php');
        break;
    case 'navigation':
        include('pages/navigation.php');
        break;
    case 'blank':
        include('pages/blank.php');
        break;
    default :
        exit('Unknown page: '.$GLOBALS['scrutari']['page']);
}


function initEngineName() {
    $engineName = '';
    if (isset($_REQUEST['engine'])) {
        $engine = $_REQUEST["engine"];
        if (!array_key_exists($engine, $GLOBALS['scrutari']['conf']['engines'])) {
            exit("Unknown engine: ".$engine);
        }
        $engineName = $engine;
    } else {
        foreach($GLOBALS['scrutari']['conf']['engines'] as $key => $value) {
            $engineName = $key;
            break;
        }
    }
    if (strlen($engineName) > 0) {
        $GLOBALS['scrutari']['engine']['name'] = $engineName;
    } else {
        exit("Engine is undefined");
    }
}

function initWidth() {
    $width = "780px";
    if (isset($_REQUEST['width'])) {
        $widthParam = $_REQUEST['width'];
        $val = intval($widthParam);
        if ($val > 0) {
            $width = $val."px";
        } else if ($widthParam ==  'lg') {
            $width = "1000px";
        }
    }
    $GLOBALS['scrutari']['width'] = $width;
}

function initMode() {
    $mode = '';
    if (isset($_REQUEST['mode'])) {
        $mode = $_REQUEST['mode'];
    } else {
        $mode = '';
    }
    $GLOBALS['scrutari']['mode'] = $mode;
}

function initSupplementaryParameters() {
    $params = '';
    foreach($_REQUEST as $key => $value) {
        if ($key != 'width' && $key != 'page' && $key != 'engine' && $key != 'target' && $key != 'mode') {
            $params .= '&'.$key.'='.urlencode($value);
        }
    }
    $GLOBALS['scrutari']['params'] = $params;
}

function initEngineOptions() {
    $engineName = $GLOBALS['scrutari']['engine']['name'];
    $GLOBALS['scrutari']['engine']['name'] = $engineName;
    $engineArray = $GLOBALS['scrutari']['conf']['engines'][$engineName];
    $GLOBALS['scrutari']['engine']['url'] = $engineArray["url"];
    $withCorpus = false;
    if (array_key_exists("corpus", $engineArray)) {
        if ($engineArray["corpus"] === true) {
            $withCorpus = true;
        }
    }
    $GLOBALS['scrutari']['engine']['with-corpus'] = $withCorpus;
    if (array_key_exists("css-links", $engineArray)) {
        $cssLinks = $engineArray["css-links"];
    } else {
        $cssLinks = array("scrutarijs/css/scrutarijs.css");
    }
    $GLOBALS['scrutari']['engine']['css-links'] = $cssLinks;
    if (array_key_exists("js-links", $engineArray)) {
        $jsLinks = $engineArray["js-links"];
    } else {
        $jsLinks = array();
    }
    $GLOBALS['scrutari']['engine']['js-links'] = $jsLinks;
    $baseSort = "fiche-count";
    if (array_key_exists("base-sort", $engineArray)) {
        $baseSort = $engineArray["base-sort"];
    }
    $GLOBALS['scrutari']['engine']['base-sort'] = $baseSort;
    $corpusSort = "fiche-count";
    if (array_key_exists("corpus-sort", $engineArray)) {
        $corpusSort = $engineArray["corpus-sort"];
    }
    $GLOBALS['scrutari']['engine']['corpus-sort'] = $corpusSort;
}

function initOrigin() {
    $GLOBALS['scrutari']['origin'] = $GLOBALS['scrutari']['conf']['origin-prefix'].$GLOBALS['scrutari']['engine']['name'];
}

function initLang() {
    $lang = 'fr';
    $l10n = 'fr';
    initLoc('fr');
    $langParam = '';
    if (isset($_REQUEST['lang'])) {
        $langParam = $_REQUEST['lang'];
        
    } else if (isset($_REQUEST['langui'])) {
        $langParam = $_REQUEST['langui'];
    }
    if (preg_match('/^[-a-zA-Z_]+$/', $langParam)) {
        $lang = $langParam;
    }
    if ($lang != 'fr') {
        initLoc($lang);
        if (file_exists("scrutarijs/js/l10n/".$lang.".js")) {
            $l10n = $lang;
        }
    }
    $GLOBALS['scrutari']['lang'] = $lang;
    $GLOBALS['scrutari']['l10n'] = $l10n;
}

function initTarget() {
    $target = '_blank';
    if (isset($_REQUEST['target'])) {
        $target = $_REQUEST['target'];
    }
    $GLOBALS['scrutari']['target'] = $target;
}

function initPage() {
    $page = 'engine';
    if(isset($_REQUEST['page'])) {
        $page = $_REQUEST['page'];
    }
    $GLOBALS['scrutari']['page'] = $page;
}

function initQuery() {
    $query = '';
    $qid = '';
    if(isset($_REQUEST['q'])) {
        $q = trim($_REQUEST['q']);
        if (strlen($q) > 0) {
            $query = $q;
        }
    }
    if(isset($_REQUEST['qid'])) {
        $qid = trim($_REQUEST['qid']);
    }
    $GLOBALS['scrutari']['query'] = $query;
    $GLOBALS['scrutari']['qid'] = $qid;
}

function initDev() {
    $dev = false;
    if(isset($_REQUEST['dev'])) {
        if ($_REQUEST['dev'] == "1") {
            $dev = true;
        }
    }
    $GLOBALS['scrutari']['dev'] = $dev;
}

function initHide() {
    $hide = '';
    if (isset($_REQUEST['hide'])) {
        $hide = $_REQUEST['hide'];
    } else {
        $hide = '';
    }
    $GLOBALS['scrutari']['hide'] = $hide;
}

function booleanToString($bool) {
    if ($bool) {
        echo "true";
    } else {
        echo "false";
    }
}

function initLoc($lang) {
    $filePath = 'l10n/'.$lang.'/loc.ini';
    if (file_exists($filePath)) {
        $locArray = parseLocArray(file($filePath));
        foreach($locArray as $key => $value) {
            $value = str_replace('"', '\"', $value);
            $value = json_decode('"'.$value.'"');
            $GLOBALS['scrutari']['loc'][$key] = $value;
        }
    }
}